$(document).ready(function () {

    var tz = jstz.determine();
    var timezone = tz.name();
    $.post(base_url + 'ajax/set_timezone', {timezone: timezone}, function (res) {
        // console.log(res);
    })

    $.post(base_url + 'ajax/currency_rate', function (res) {
        //console.log(res);
    })







});



function change_language(lang) {
    $.post(base_url + 'ajax/set_language', {lang: lang}, function (res) {
        window.location.reload();
    })

}


function user_currency(code) {
    if (code != "") {

        $.ajax({
            type: 'POST',
            url: base_url + 'ajax/add_user_currency',
            data: {code: code},
            dataType: 'json',
            success: function (response)
            {
                if (response.success)
                {
                    location.reload();
                } else {
                    location.reload();
                }
            }
        });
    }
}





if (modules == 'signin') {

    $(document).ready(function (){


      $("#login_otp").on('click', function () {

        $("#username_div").hide();

        $("#otp_div").show();


      });

      $("#login_username").on('click', function () {

        $("#username_div").show();

        $("#otp_div").hide();


      });
      



    });


    function change_role()
    {
        var role = $('#role').val();
        if (role == 1)
        {
            $('#role_type').html(lang_patient);
            $('#role_types').html(lang_doctor);
            $('.document-info').hide();
            $('.add-more').hide();
            $('#role').val(2);
        }
        if (role == 2)
        {
            $('#role_type').html(lang_doctor);
            $('#role_types').html(lang_patient);
            $('.document-info').show();
            $('.add-more').show();
            $('#role').val(1);
        }
        $('#register_form')[0].reset();
    }

    function resend_otp() {
        // alert('dfd');
        var mobileno = $('#mobileno').val();
        var country_code = $('#country_code').val();

        $.ajax({
            url: base_url + 'Signin/sendotp',
            data: {
                mobileno: mobileno, country_code: country_code, otpcount: '2'
            },
            //contentType: "application/json; charset=utf-8",
            dataType: "text",
            method: "post",
            beforeSend: function () {
                $('.otp_load').html('<div class="spinner-border text-light" role="status"></div>');
            },
            success: function (res) {

                $('.otp_load').html('<a class="forgot-link" onclick="resend_otp()"  href="javascript:void(0);" id="resendotp">Resend OTP</a>');
                var obj = JSON.parse(res);

                if (obj.status === 200)
                {
                    $('.OTP').show();
                    toastr.success(obj.msg);

                } else if (obj.status === 500)
                {
                    toastr.error(obj.msg);

                } else
                {
                    toastr.error(obj.msg);
                }
            }
        });

    }

      function resend_otp_login() {
        // alert('dfd');
        var mobileno = $('#mobileno').val();
        var country_code = $('#country_id').val();

        $.ajax({
            url: base_url + 'Signin/sendotp_login',
            data: {
                mobileno: mobileno, country_code: country_code, otpcount: '2'
            },
            //contentType: "application/json; charset=utf-8",
            dataType: "text",
            method: "post",
            beforeSend: function () {
                $('.otp_load').html('<div class="spinner-border text-light" role="status"></div>');
            },
            success: function (res) {

                $('.otp_load').html('<a class="forgot-link" onclick="resend_otp_login()"  href="javascript:void(0);" id="resendotp">Resend OTP</a>');
                var obj = JSON.parse(res);

                if (obj.status === 200)
                {
                    $('.OTP').show();
                    toastr.success(obj.msg);

                } else if (obj.status === 500)
                {
                    toastr.error(obj.msg);

                } else
                {
                    toastr.error(obj.msg);
                }
            }
        });

    }

    function togglePasswordVisibility(ele){
        var input = $(ele).next('input');

        if(input.attr('type') == 'password'){
            input.attr('type', 'text');
            $(ele).children('i').removeClass('far fa-eye-slash')
            $(ele).children('i').addClass('far fa-eye')
        }else{
            input.attr('type', 'password');
            $(ele).children('i').removeClass('far fa-eye')
            $(ele).children('i').addClass('far fa-eye-slash')

        }
    }
}


if (modules == 'home')
{
    if (pages == 'index')
    {


           $("#search_button").click(function(){

            var search_keywords =  $.trim($('#search_keywords').val());
            var search_location =  $.trim($('#search_location').val());
             if(search_keywords!='' && search_location !=''){

              window.location.href=base_url+'doctors-search?location='+search_location+'&keywords='+search_keywords;

              }else if(search_keywords!=''){
              window.location.href=base_url+'doctors-search?keywords='+search_keywords;
              }else if(search_location !=''){
              window.location.href=base_url+'doctors-search?location='+search_location;

              }else{
                toastr.warning("Please enter keyword or location");
              }


    });

        function search_locations()
        {

            $('.location_result').html('');
            var search_location = $.trim($('#search_location').val());
            if (search_location != '') {
                $.ajax({
                    type: "POST",
                    url: base_url + 'home/search_location',
                    data: 'search_location=' + search_location,
                    success: function (data)
                    {
                        // return false;
                        if (data.length) {
                            var obj = jQuery.parseJSON(data);
                            var html = ''

                            if (obj.location != null) {

                                $(obj.location).each(function () {
                                    html += '<div class="keyword-search"><a href="' + base_url + 'doctors-search?location=' + this.location + '">Location  - ' + this.location + '</a></div>';
                                });


                            }

                            $('.location_result').html(html);


                        } else {
                            $('.location_result').html('<b>No city found.</b>');
                        }

                    }
                });


            } else {
                $('.location_result').html('');
            }

        }

        function search_keyword()
        {
            $('.keywords_result').html('');
            var search_keywords = $.trim($('#search_keywords').val());
            if (search_keywords != '') {
                $.ajax({
                    type: "POST",
                    url: base_url + 'home/search_keywords',
                    data: 'search_keywords=' + search_keywords,
                    success: function (data)
                    {
                        // return false;
                        if (data.length) {
                            var obj = jQuery.parseJSON(data);
                            var html = ''

                            if (obj.specialist != null) {

                                $(obj.specialist).each(function () {
                                    html += '<div class="keyword-search"><a href="' + base_url + 'doctors-search?keywords=' + this.specialization + '">Speciality  - ' + this.specialization + '</a></div>';
                                });


                            }

                            if (obj.doctor != null) {
                                $(obj.doctor).each(function () {
                                    html += '<div class="keyword-search"><a href="' + base_url + 'doctors-search?keywords=' + this.first_name + '"><div class="keyword-img"><img src="' + this.profileimage + '" class="img-responsive"></div>Dr. '
                                            + this.first_name + ' ' + this.last_name +
                                            '</a><small>Specialist -' + this.speciality + '</small></div>';
                                });

                                $('.keywords_result').html(html);
                            } else {
                                var html = '<b>No Doctors found.</b>';

                            }



                            $('.keywords_result').html(html);


                        } else {
                            $('.keywords_result').html('<b>No Doctors found.</b>');
                        }

                    }
                });


            } else {
                $('.keyword_result').html('');
            }

        }


    }
    if (pages == 'index' || pages == 'doctor_preview') {
        function add_favourities(doctor_id)
        {
            $.post(base_url + 'home/add_favourities', {doctor_id: doctor_id}, function (data) {
                var obj = JSON.parse(data);

                if (obj.status === 200)
                {
                    $('#favourities_' + doctor_id).addClass("fav-btns");

                } else if (obj.status === 204)
                {
                    toastr.warning(obj.msg);

                } else if (obj.status === 201)
                {
                    $('#favourities_' + doctor_id).removeClass("fav-btns");

                } else
                {
                    $('#favourities_' + doctor_id).removeClass("fav-btns");
                }

            });
        }
    }
}
$(document).ready(function () {


    $.ajax({
        type: "GET",
        url: base_url + "ajax/get_country_code",
        data: {id: $(this).val()},
        beforeSend: function () {
            $('#country_code').find("option:eq(0)").html("Please wait..");
        },
        success: function (data) {
            /*get response as json */
            $('#country_code').find("option:eq(0)").html("Select Country Code");
            var obj = jQuery.parseJSON(data);
            $(obj).each(function ()
            {
                var option = $('<option />');
                option.attr('value', this.value).text(this.label);
                $('#country_code').append(option);
            });


            /*ends */

        }
    });
//signin
    if (modules == 'signin') {
        if (pages == 'index' || pages=='register') {

            $('.OTP').hide();
            $('#resendotp').hide();
            $("#sendotp").on('click', function () {

                var mobileno = $('#mobileno_reg').val();
                var country_code = $('#country_code').val();
                if (mobileno == "")
                {
                    toastr.error('Please enter valid mobileno!');
                } else {
                    $.ajax({
                        url: base_url + 'Signin/sendotp',
                        data: {
                            mobileno: mobileno, country_code: country_code, otpcount: '1'
                        },

                        dataType: "text",
                        method: "post",
                        beforeSend: function () {
                            $('.otp_load').html('<div class="spinner-border text-light" role="status"></div>');
                        },
                        success: function (res) {

                            $('.otp_load').html('<a class="forgot-link" onclick="resend_otp()"  href="javascript:void(0);" id="resendotp">Resend OTP</a>');

                            var obj = JSON.parse(res);

                            if (obj.status === 200)
                            {

                                $('.OTP').show();
                                $('#resendotp').show();
                                toastr.success(obj.msg);

                            } else if (obj.status === 500)
                            {
                                toastr.error(obj.msg);
                            } else
                            {
                                toastr.error(obj.msg);
                            }
                        }
                    });
                }
            });
             $('#resendotp_login').hide();
             $("#sendotp_login").on('click', function () {

                var mobileno = $('#mobileno').val();
                var country_code = $('#country_id').val();
                if (mobileno == "")
                {
                    toastr.error('Please enter valid mobileno!');
                } else {
                    $.ajax({
                        url: base_url + 'Signin/sendotp_login',
                        data: {
                            mobileno: mobileno, country_code: country_code, otpcount: '1'
                        },

                        dataType: "text",
                        method: "post",
                        beforeSend: function () {
                            $('.otp_load').html('<div class="spinner-border text-light" role="status"></div>');
                        },
                        success: function (res) {

                            $('.otp_load').html('<a class="forgot-link" onclick="resend_otp_login()"  href="javascript:void(0);" id="resendotp">Resend OTP</a>');

                            var obj = JSON.parse(res);

                            if (obj.status === 200)
                            {

                                $('.OTP').show();
                                $('#resendotp_login').show();
                                toastr.success(obj.msg);

                            } else if (obj.status === 500)
                            {
                                toastr.error(obj.msg);
                            } else
                            {
                                toastr.error(obj.msg);
                            }
                        }
                    });
                }
            });

            $(".document-info").on('click','.trash', function () {
                $(this).closest('.document-cont').remove();
                return false;
            });

            $(".add-document").on('click', function () {

                var documentcontent = '<div class="row document-cont">'+
                          '<div class="col-md-9">'+
                            '<label>Document Name</label>'+
                            '<input type="text" name="doc_name[]" class="form-control">'+
                          '</div>'+
                          '<div class="col-md-9">'+
                          '<label>Document</label>'+
                          '<input type="file" name="doc_files[]" class="form-control">'+
                          '</div>'+
                          '<div class="col-md-3">'+
                          '<label class="d-md-block d-sm-none d-none">&nbsp;</label>'+
                          '<a href="#" class="btn btn-danger trash"><i class="far fa-trash-alt"></i></a>'+
                        '</div></div>';
                
                $(".document-info").append(documentcontent);
                return false;
            });

            $("#register_form").validate({
                rules: {
                    first_name: "required",
                  
                    mobileno: {
                        required: true,
                        minlength: 7,
                        maxlength: 12,
                        digits: true,
                        remote: {
                            url: base_url + "signin/check_mobileno",
                            type: "post",
                            data: {
                                mobileno: function () {
                                    return $("#mobileno_reg").val();
                                }
                            }
                        }
                    },
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: base_url + "signin/check_email",
                            type: "post",
                            data: {
                                email: function () {
                                    return $("#email_reg").val();
                                }
                            }
                        }
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    

                },
                messages: {
                    first_name: lg_please_enter_yo,
                    last_name: lg_please_enter_yo1,
                    mobileno: {
                        required: lg_please_enter_mo,
                        maxlength: lg_please_enter_va,
                        minlength: lg_please_enter_va,
                        digits: lg_please_enter_va,
                        remote: lg_your_mobile_no_
                    },
                    email: {
                        required: lg_please_enter_em,
                        email: lg_please_enter_va1,
                        remote: lg_your_email_addr1
                    },
                    password: {
                        required: lg_please_enter_pa,
                        minlength: lg_your_password_m
                    },
                    

                },
                // submitHandler: function (form) {
                //     console.log($(form).serialize());
                // }
            });

            $(document).on('submit','#register_form',function(e){
                e.preventDefault();
                $.ajax({
                    url: base_url + 'signin/signup',
                    type: "POST",
                    data:  new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function () {
                        $('#register_btn').attr('disabled', true);
                        $('#register_btn').html('<div class="spinner-border text-light" role="status"></div>');
                    },
                    success: function (res) {
                        $('#register_btn').attr('disabled', false);
                        $('#register_btn').html(lg_signup);
                        var obj = JSON.parse(res);

                        if (obj.status === 200)
                        {
                            $('#register_form')[0].reset();
                            window.location.href = base_url + 'signin';

                        } else
                        {
                            toastr.error(obj.msg);
                        }
                    }
                });
                return false;           
            });

            $("#signin_form").validate({
                rules: {
                    email: "required",
                    password: {
                        required: true,
                        minlength: 6
                    },
                    otpno: {
                        required: true,
                        
                    },
                },
                messages: {
                    email: lg_please_enter_em,
                    password: {
                        required: lg_please_enter_pa,
                        minlength: lg_your_password_m
                    },
                    otpno: {
                        required: "Enter your OTP",
                        
                    },

                },
                submitHandler: function (form) {

                    $.ajax({
                        url: base_url + 'signin/is_valid_login',
                        data: $("#signin_form").serialize(),
                        type: "POST",
                        beforeSend: function () {
                            $('#signin_btn').attr('disabled', true);
                            $('#signin_btn').html('<div class="spinner-border text-light" role="status"></div>');
                        },
                        success: function (res) {
                            $('#signin_btn').attr('disabled', false);
                            $('#signin_btn').html(lg_signin);
                            console.log(res);
                            var obj = JSON.parse(res);

                            if (obj.status === 200)
                            {
                                window.location.href = base_url + 'dashboard';
                            } else
                            {
                                toastr.error(obj.msg);
                            }
                        }
                    });
                    return false;
                }
            });
        }
        if (pages == 'forgot_password') {
            $("#reset_password").validate({
                rules: {

                    resetemail: {
                        required: true,
                        email: true,
                        remote: {
                            url: base_url + "signin/check_resetemail",
                            type: "post",
                            data: {
                                resetemail: function () {
                                    return $("#resetemail").val();
                                }
                            }
                        }
                    }
                },
                messages: {
                    resetemail: {
                        required: lg_please_enter_em,
                        email: lg_please_enter_va1,
                        remote: lg_your_email_addr
                    }
                },
                submitHandler: function (form) {
                    $.ajax({
                        url: base_url + 'signin/reset_password',
                        data: $("#reset_password").serialize(),
                        type: "POST",
                        beforeSend: function () {
                            $('#reset_pwd').attr('disabled', true);
                            $('#reset_pwd').html('<div class="spinner-border text-light" role="status"></div>');
                        },
                        success: function (res) {
                            $('#reset_pwd').attr('disabled', false);
                            $('#reset_pwd').html(lg_reset_password);

                            var obj = JSON.parse(res);

                            if (obj.status === 200)
                            {
                                $('#reset_password')[0].reset();
                                toastr.success(obj.msg);
                                setTimeout(function () {
                                    window.location.href = base_url + 'home';
                                }, 5000);

                            } else
                            {
                                toastr.error(obj.msg);
                            }
                        }
                    });
                    return false;
                }
            });
        }

        if (pages == 'change_password')
        {
            $(document).ready(function () {

                $("#change_password").validate({
                    rules: {

                        password: {
                            required: true,
                            minlength: 6
                        },
                        confirm_password: {
                            required: true,
                            equalTo: "#password"
                        },
                    },
                    messages: {
                        password: {
                            required: lg_please_enter_pa,
                            minlength: lg_your_password_m
                        },
                        confirm_password: {
                            required: lg_please_enter_co,
                            equalTo: lg_your_password_d
                        },

                    },
                    submitHandler: function (form) {
                        $.ajax({
                            url: base_url + 'signin/update_password',
                            data: $("#change_password").serialize(),
                            type: "POST",
                            beforeSend: function () {
                                $('#update_pwd').attr('disabled', true);
                                $('#update_pwd').html('<div class="spinner-border text-light" role="status"></div>');

                            },
                            success: function (res) {
                                $('#update_pwd').attr('disabled', false);
                                $('#update_pwd').html(lg_confirm3);
                                var obj = JSON.parse(res);

                                if (obj.status === 200)
                                {
                                    $('#change_password')[0].reset();
                                    toastr.success(obj.msg);
                                    setTimeout(function () {
                                        window.location.href = base_url + 'home';
                                    }, 5000);
                                } else
                                {
                                    toastr.error(obj.msg);
                                }
                            }
                        });
                        return false;
                    }
                });

            });

        }

    }



    if ((modules == 'doctor' || modules == 'patient') && (pages == 'doctor_profile' || pages == 'patient_profile') || pages == 'doctors_search' || pages == 'doctors_searchmap' || pages == 'patients_search') {


        $.ajax({
            type: "GET",
            url: base_url + "ajax/get_country_code",
            data: {id: $(this).val()},
            beforeSend: function () {
                $('#country_code').find("option:eq(0)").html("Please wait");
            },
            success: function (data) {
                /*get response as json */
                $('#country_code').find("option:eq(0)").html("select country code");
                var obj = jQuery.parseJSON(data);
                $(obj).each(function ()
                {
                    var option = $('<option />');
                    option.attr('value', this.value).text(this.label);
                    $('#country_code').append(option);
                });

                $('#country_code').val(country_code);

                /*ends */

            }
        });


        $.ajax({
            type: "GET",
            url: base_url + "ajax/get_country",
            data: {id: $(this).val()},
            beforeSend: function () {
                $('#country').find("option:eq(0)").html(lg_please_wait);
            },
            success: function (data) {
                /*get response as json */
                $('#country').find("option:eq(0)").html(lg_select_country);
                var obj = jQuery.parseJSON(data);
                $(obj).each(function ()
                {
                    var option = $('<option />');
                    option.attr('value', this.value).text(this.label);
                    $('#country').append(option);
                });

                $('#country').val(country);

                /*ends */

            }
        });

        $.ajax({
            type: "POST",
            url: base_url + "ajax/get_state",
            data: {id: country},
            beforeSend: function () {
                $("#state option:gt(0)").remove();
                $("#city option:gt(0)").remove();
                $('#state').find("option:eq(0)").html(lg_please_wait);

            },
            success: function (data) {
                /*get response as json */
                $('#state').find("option:eq(0)").html(lg_select_state);
                var obj = jQuery.parseJSON(data);
                $(obj).each(function ()
                {
                    var option = $('<option />');
                    option.attr('value', this.value).text(this.label);
                    $('#state').append(option);
                });
                $('#state').val(state);
                /*ends */

            }
        });

        $.ajax({
            type: "POST",
            url: base_url + "ajax/get_city",
            data: {id: state},
            beforeSend: function () {

                $("#city option:gt(0)").remove();
                $('#city').find("option:eq(0)").html(lg_please_wait);

            },

            success: function (data) {
                /*get response as json */
                $('#city').find("option:eq(0)").html(lg_select_city);

                var obj = jQuery.parseJSON(data);
                $(obj).each(function ()
                {
                    var option = $('<option />');
                    option.attr('value', this.value).text(this.label);
                    $('#city').append(option);
                });
                $('#city').val(city);
                /*ends */

            }
        });



        /*Get the state list */


        $('#country').change(function () {
            $.ajax({
                type: "POST",
                url: base_url + "ajax/get_state",
                data: {id: $(this).val()},
                beforeSend: function () {
                    $("#state option:gt(0)").remove();
                    $("#city option:gt(0)").remove();
                    $('#state').find("option:eq(0)").html(lg_please_wait);

                },
                success: function (data) {
                    /*get response as json */
                    $('#state').find("option:eq(0)").html(lg_select_state);
                    var obj = jQuery.parseJSON(data);
                    $(obj).each(function ()
                    {
                        var option = $('<option />');
                        option.attr('value', this.value).text(this.label);
                        $('#state').append(option);
                    });

                    /*ends */

                }
            });
        });




        /*Get the state list */


        $('#state').change(function () {
            $.ajax({
                type: "POST",
                url: base_url + "ajax/get_city",
                data: {id: $(this).val()},
                beforeSend: function () {

                    $("#city option:gt(0)").remove();
                    $('#city').find("option:eq(0)").html(lg_please_wait);

                },

                success: function (data) {
                    /*get response as json */
                    $('#city').find("option:eq(0)").html(lg_select_city);

                    var obj = jQuery.parseJSON(data);
                    $(obj).each(function ()
                    {
                        var option = $('<option />');
                        option.attr('value', this.value).text(this.label);
                        $('#city').append(option);
                    });

                    /*ends */

                }
            });
        });

        if (pages == 'doctor_profile' || pages == 'doctors_search' || pages == 'doctors_searchmap') {



            $.ajax({
                type: "GET",
                url: base_url + "ajax/get_specialization",
                data: {id: $(this).val()},
                beforeSend: function () {
                    $('#specialization').find("option:eq(0)").html(lg_please_wait);
                },
                success: function (data) {
                    /*get response as json */
                    $('#specialization').find("option:eq(0)").html(lg_select_speciali1);
                    var obj = jQuery.parseJSON(data);
                    $(obj).each(function ()
                    {
                        var option = $('<option />');
                        option.attr('value', this.value).text(this.label);
                        $('#specialization').append(option);
                    });

                    $('#specialization').val(specialization);

                    /*ends */

                }
            });


        }


        if (pages == 'doctor_profile') {

            /*Get the country list */
            $("#doctor_profile_form").validate({
                rules: {
                    first_name: "required",
                    last_name: "required",
                    mobileno: {
                        required: true,
                        minlength: 7,
                        maxlength: 12,
                        digits: true,
                        remote: {
                            url: base_url + "profile/check_mobileno",
                            type: "post",
                            data: {
                                mobileno: function () {
                                    return $("#mobileno").val();
                                }
                            }
                        }
                    },
                    gender: "required",
                    dob: "required",
                    address1: "required",
                    address2: "required",
                    country: "required",
                    state: "required",
                    city: "required",
                    postal_code: {
                        required: true,
                        minlength: 4,
                        maxlength: 7,
                        digits: true,
                    },
                    price_type: "required",
                    amount: {
                        required: function (element) {
                            if ($("input[name='price_type']:checked").val() === "Custom Price") {
                                return true;
                            } else {
                                return false;
                            }
                        },
                        digits: true,
                        min: 1
                    },
                    services: "required",
                    specialization: "required",
                    "degree[]": "required",
                    "institute[]": "required",
                    "year_of_completion[]": "required",

                },
                messages: {
                    first_name: lg_please_enter_yo,
                    last_name: lg_please_enter_yo1,
                    mobileno: {
                        required: lg_please_enter_mo,
                        maxlength: lg_please_enter_va,
                        minlength: lg_please_enter_va,
                        digits: lg_please_enter_va,
                        remote: lg_your_mobile_no_
                    },
                    gender: lg_please_select_g,
                    dob: lg_please_enter_yo2,
                    address1: lg_please_enter_yo3,
                    address2: lg_please_enter_yo4,
                    country: lg_please_select_c,
                    state: lg_please_select_s,
                    city: lg_please_select_c1,
                    postal_code: {
                        required: lg_please_enter_po,
                        maxlength: lg_please_enter_va2,
                        minlength: lg_please_enter_va2,
                        digits: lg_please_enter_va2
                    },
                    price_type: lg_please_select_p,
                    amount: {
                        required: lg_please_enter_am,
                        digits: lg_please_enter_va3,
                        min: lg_please_enter_va3
                    },
                    services: lg_please_enter_se,
                    specialization: lg_please_select_s1,
                    "degree[]": lg_please_enter_de,
                    "institute[]": lg_please_enter_in,
                    "year_of_completion[]": lg_please_enter_ye

                },
                // submitHandler: function (form) {
                // }
            });

            $(document).on('submit','#doctor_profile_form',function(e){
                e.preventDefault();
                $.ajax({
                    url: base_url + 'profile/update_doctor_profile',
                    type: "POST",
                    data:  new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function () {
                        $('#save_btn').attr('disabled', true);
                        $('#save_btn').html('<div class="spinner-border text-light" role="status"></div>');
                    },
                    success: function (res) {
                        $('#save_btn').attr('disabled', false);
                        $('#save_btn').html(lg_save_changes);

                        var obj = JSON.parse(res);

                        if (obj.status === 200)
                        {
                            toastr.success(obj.msg);
                            setTimeout(function () {
                                window.location.href = base_url + 'dashboard';
                            }, 5000);

                        } else
                        {
                            toastr.error(obj.msg);
                        }
                    }
                });
                return false;
            });

            $(document).on('click','.doc_delete',function(){
                var id = $(this).attr('data-id');
                $.ajax({
                    url: base_url + 'profile/delete_document',
                    type: "POST",
                    data:  {'id':id},
                    success: function(res){
                        var obj = JSON.parse(res);

                        if (obj.status === 200)
                        {
                            toastr.success(obj.msg);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);

                        } else
                        {
                            toastr.error(obj.msg);
                        }
                    }
                });
            });

            $(".document-info").on('click','.trash', function () {
                $(this).closest('.document-cont').remove();
                return false;
            });

            $(".add-document").on('click', function () {

                var documentcontent = '<div class="row document-cont">'+
                          '<div class="col-md-9">'+
                            '<label>Document Name</label>'+
                            '<input type="text" name="doc_name[]" class="form-control">'+
                          '</div>'+
                          '<div class="col-md-9">'+
                          '<label>Document</label>'+
                          '<input type="file" name="doc_files[]" class="form-control">'+
                          '</div>'+
                          '<div class="col-md-3">'+
                          '<label class="d-md-block d-sm-none d-none">&nbsp;</label>'+
                          '<a href="#" class="btn btn-danger trash"><i class="far fa-trash-alt"></i></a>'+
                        '</div></div>';
                
                $(".document-info").append(documentcontent);
                return false;
            });

            $(document).on('click', '.days_check', function () {

                if ($(this).is(':checked') == true) {

                    $('.eachdays').attr('disabled', 'disabled');
                    $('.eachdayfromtime').attr('disabled', 'disabled');
                    $('.eachdaytotime').attr('disabled', 'disabled');
                    $('.eachdays').prop('checked', false);
                    $('.eachdays').removeAttr('style');
                    $('.eachdayfromtime').removeAttr('style');
                    $('.eachdaytotime').removeAttr('style');

                } else {
                    $('.eachdays').removeAttr('disabled');
                    $('.eachdayfromtime').removeAttr('disabled');
                    $('.eachdaytotime').removeAttr('disabled');
                    $('.daysfromtime_check').val('');
                    $('.daystotime_check').val('');
                    $('.daysfromtime_check').removeAttr('style');
                    $('.daystotime_check').removeAttr('style');
                }

            });

        }

        if (pages == 'patient_profile') {
            var maxDate = $('#maxDate').val();
            $('#dob').datepicker({
                startView: 2,
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true,
                endDate: maxDate
            });

            $("#patient_profile_form").validate({
                rules: {
                    first_name: "required",
                    last_name: "required",
                    mobileno: {
                        required: true,
                        minlength: 7,
                        maxlength: 12,
                        digits: true,
                        remote: {
                            url: base_url + "profile/check_mobileno",
                            type: "post",
                            data: {
                                mobileno: function () {
                                    return $("#mobileno").val();
                                }
                            }
                        }
                    },
                    gender: "required",
                    dob: "required",
                    blood_group: "required",
                    address1: "required",
                    address2: "required",
                    country: "required",
                    state: "required",
                    city: "required",
                    postal_code: {
                        required: true,
                        minlength: 4,
                        maxlength: 7,
                        digits: true,
                    }
                },
                messages: {
                    first_name: lg_please_enter_yo,
                    last_name: lg_please_enter_yo1,
                    mobileno: {
                        required: lg_please_enter_mo,
                        maxlength: lg_please_enter_va,
                        minlength: lg_please_enter_va,
                        digits: lg_please_enter_va,
                        remote: lg_your_mobile_no_
                    },
                    gender: lg_please_select_g,
                    dob: lg_please_enter_yo2,
                    blood_group: lg_please_select_b,
                    address1: lg_please_enter_yo3,
                    address2: lg_please_enter_yo4,
                    country: lg_please_select_c,
                    state: lg_please_select_s,
                    city: lg_please_select_c1,
                    postal_code: {
                        required: lg_please_enter_po,
                        maxlength: lg_please_enter_va2,
                        minlength: lg_please_enter_va2,
                        digits: lg_please_enter_va2
                    }
                },
                submitHandler: function (form) {

                    $.ajax({
                        url: base_url + 'profile/update_patient_profile',
                        data: $("#patient_profile_form").serialize(),
                        type: "POST",
                        beforeSend: function () {
                            $('#save_btn').attr('disabled', true);
                            $('#save_btn').html('<div class="spinner-border text-light" role="status"></div>');
                        },
                        success: function (res) {
                            $('#save_btn').attr('disabled', false);
                            $('#save_btn').html(lg_save_changes);

                            var obj = JSON.parse(res);

                            if (obj.status === 200)
                            {

                                toastr.success(obj.msg);
                                setTimeout(function () {
                                    window.location.href = base_url + 'dashboard';
                                }, 5000);

                            } else
                            {
                                toastr.error(obj.msg);
                            }
                        }
                    });
                    return false;
                }
            });
        }
    }

});

if (modules == 'home')
{
    if (pages == 'doctors_search')
    {


        search_doctor(0);

        function reset_doctor()
        {
            $('#orderby').val('');
            $('#keywords').val('');
            $('#appointment_type').val('');
            $('#gender').val('');
            $('#specialization').val('');
            $('#country').val('');
            $('#state').val('');
            $('#city').val('');
            // $('#search_doctor_form')[0].reset();
            search_doctor(0);
        }




        function search_doctor(load_more) {

            if (load_more == 0) {
                $('#page_no_hidden').val(1);
            }

            var specialization = $('#specialization').val();
            var order_by = $('#orderby').val();
            var page = $('#page_no_hidden').val();
            var gender = $("#gender").val();
            var appointment_type = $("#appointment_type").val();
            var city = $("#city").val();
            var state = $("#state").val();
            var country = $("#country").val();
            var keywords = $("#keywords").val();



            //$('#search-error').html('');

            $.ajax({
                url: base_url + 'home/search_doctor',
                type: 'POST',
                data: {
                    appointment_type: appointment_type,
                    gender: gender,
                    specialization: specialization,
                    order_by: order_by,
                    page: page,
                    keywords: keywords,
                    city: city,
                    citys: citys,
                    state: state,
                    country: country
                },
                beforeSend: function () {
                    // $('#doctor-list').html('<div class="spinner-border text-success text-center" role="status"></div>');
                },
                success: function (response) {
                    //$('#doctor-list').html('');
                    if (response) {

                        var obj = $.parseJSON(response);
                        if (obj.data.length >= 1) {
                            var html = '';
                            $(obj.data).each(function () {

                                var services = '';

                                if (this.services.length != 0) {
                                    var service = this.services.split(',');
                                    for (var i = 0; i < service.length; i++) {
                                        services += '<span>' + service[i] + '</span>';
                                    }
                                }

                                var clinic_images = '';
         
                                var clinic_images_file = $.parseJSON(this.clinic_images);
                                $.each(clinic_images_file, function(key, item) {
                                  var userid=item.user_id;
                              clinic_images +='<li> <a href="uploads/clinic_uploads/'+userid+'/'+item.clinic_image+'" data-fancybox="gallery"> <img src="uploads/clinic_uploads/'+userid+'/'+item.clinic_image+'" alt="Feature"> </a> </li>';
                            
                                 });


                                html += '<div class="card">' +
                                        '<div class="card-body">' +
                                        '<div class="doctor-widget">' +
                                        '<div class="doc-info-left">' +
                                        '<div class="doctor-img">' +
                                        '<a href="' + base_url + 'doctor-preview/' + this.username + '">' +
                                        '<img src="' + this.profileimage + '" class="img-fluid" alt="User Image">' +
                                        '</a>' +
                                        '</div>' +
                                        '<div class="doc-info-cont">' +
                                        '<h4 class="doc-name"><a href="' + base_url + 'doctor-preview/' + this.username + '">' + lg_dr + ' ' + this.first_name + ' ' + this.last_name + '</a></h4>' +
                                        '<h5 class="doc-department"><img src="' + this.specialization_img + '" class="img-fluid" alt="Speciality">' + this.speciality + '</h5>' +
                                        '<div class="rating">';
                                for (var j = 1; j <= 5; j++) {
                                    if (j <= this.rating_value) {
                                        html += '<i class="fas fa-star filled"></i>';
                                    } else {
                                        html += '<i class="fas fa-star"></i>';
                                    }
                                }
                                html += '<span class="d-inline-block average-rating">(' + this.rating_count + ')</span>' +
                                        '</div>' +
                                        '<div class="clinic-details">'+
                                        '<p class="doc-location"><i class="fas fa-map-marker-alt"></i> '+this.cityname+', '+this.countryname+'</p>'+
                                        ' <ul class="clinic-gallery">'+clinic_images +'</ul>'+
                                        '</div>'+
                                        '<div class="clinic-services">' + services + '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '<div class="doc-info-right">' +
                                        '<div class="clini-infos">' +
                                        '<ul>' +
                                        '<li><i class="far fa-comment"></i>' + this.rating_count + ' ' + lg_feedback + '</li>' +
                                        '<li><i class="fas fa-map-marker-alt"></i> ' + this.cityname + ', ' + this.countryname + '</li>' +
                                        '<li><i class="far fa-money-bill-alt"></i> ' + this.amount + ' </li>' +
                                        '</ul>' +
                                        '</div>' +
                                        '<div class="clinic-booking">' +
                                        '<a class="view-pro-btn" href="' + base_url + 'doctor-preview/' + this.username + '">' + lg_view_profile + '</a>' +
                                        '<a class="apt-btn" href="' + base_url + 'book-appoinments/' + this.username + '">' + lg_book_appointmen + '</a>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>';

                            });

                            if (obj.current_page_no == 1) {
                                $("#doctor-list").html(html);
                            } else {
                                $("#doctor-list").append(html);
                            }

                        } else
                        {
                            var html = '<div class="card">' +
                                    '<div class="card-body">' +
                                    '<div class="doctor-widget">' +
                                    '<p>' + lg_no_doctors_foun + '</p>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                            $("#doctor-list").html(html);
                        }




                        var minimized_elements = $('h4.minimize');
                        minimized_elements.each(function () {
                            var t = $(this).text();
                            if (t.length < 100)
                                return;
                            $(this).html(
                                    t.slice(0, 100) + '<span>... </span><a href="#" class="more">' + lg_more + '</a>' +
                                    '<span style="display:none;">' + t.slice(100, t.length) + ' <a href="#" class="less">' + lg_less + '</a></span>'
                                    );
                        });

                        // $('a.more', minimized_elements).click(function(event) {
                        //   event.preventDefault();
                        //   $(this).hide().prev().hide();
                        //   $(this).next().show();
                        // });

                        // $('a.less', minimized_elements).click(function(event) {
                        //   event.preventDefault();
                        //   $(this).parent().hide().prev().show().prev().show();
                        // });

                        $(".search-results").html('<span>' + obj.count + ' ' + lg_matches_for_you + '' + '</span>');
                        // $(".widget-title").html(obj.count+' Matches for your search');
                        if (obj.count == 0) {
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                            return false;
                        }


                        if (obj.current_page_no == 1 && obj.count < 5) {
                            $('page_no_hidden').val(1);
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                            return false;
                        }



                        if (obj.total_page > obj.current_page_no && obj.total_page != 0) {
                            $('#load_more_btn').removeClass('d-none');
                            $('#no_more').addClass('d-none');
                        } else {
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                        }

                    }
                }

            });
        }



        $('#load_more_btn').click(function () {
            var page_no = $('#page_no_hidden').val();
            var current_page_no = 0;

            if (page_no == 1) {
                current_page_no = 2;
            } else {
                current_page_no = Number(page_no) + 1;
            }
            $('#page_no_hidden').val(current_page_no);
            search_doctor(1);
        });
    }
}


if (modules == 'home')
{
    if (pages == 'doctors_searchmap')
    {





        var locations = [];
        function reset_doctor()
        {
            $('#orderby').val('');
            $('#keywords').val('');
            $('#appointment_type').val('');
            $('#gender').val('');
            $('#specialization').val('');
            $('#country').val('');
            $('#state').val('');
            $('#city').val('');
            // $('#search_doctor_form')[0].reset();
            search_doctor(0);
        }

        search_doctor(0);
        function search_doctor(load_more) {

            if (load_more == 0) {
                $('#page_no_hidden').val(1);
            }

            var specialization = $('#specialization').val();
            var order_by = $('#orderby').val();
            var page = $('#page_no_hidden').val();
            var gender = $("#gender").val();
            var appointment_type = $("#appointment_type").val();
            var city = $("#city").val();
            var state = $("#state").val();
            var country = $("#country").val();
            var keywords = $("#keywords").val();


            //$('#search-error').html('');

            $.ajax({
                url: base_url + 'home/search_doctor',
                type: 'POST',
                data: {
                    appointment_type: appointment_type,
                    gender: gender,
                    specialization: specialization,
                    order_by: order_by,
                    page: page,
                    keywords: keywords,
                    city: city,
                    state: state,
                    country: country
                },
                beforeSend: function () {
                    // $('#doctor-list').html('<div class="spinner-border text-success text-center" role="status"></div>');
                },
                success: function (response) {
                    //$('#doctor-list').html('');
                    if (response) {

                        var obj = $.parseJSON(response);

                        if (obj.data.length >= 1) {
                            var html = '';
                            //var locations = [];
                            $(obj.data).each(function () {



                                var services = '';

                                if (this.services.length != 0) {
                                    var service = this.services.split(',');
                                    for (var i = 0; i < service.length; i++) {
                                        services += '<span>' + service[i] + '</span>';
                                    }
                                }


                                html += '<div class="card">' +
                                        '<div class="card-body">' +
                                        '<div class="doctor-widget">' +
                                        '<div class="doc-info-left">' +
                                        '<div class="doctor-img">' +
                                        '<a href="' + base_url + 'doctor-preview/' + this.username + '">' +
                                        '<img src="' + this.profileimage + '" class="img-fluid" alt="User Image">' +
                                        '</a>' +
                                        '</div>' +
                                        '<div class="doc-info-cont">' +
                                        '<h4 class="doc-name"><a href="' + base_url + 'doctor-preview/' + this.username + '">' + lg_dr + ' ' + this.first_name + ' ' + this.last_name + '</a></h4>' +
                                        '<h5 class="doc-department"><img src="' + this.specialization_img + '" class="img-fluid" alt="Speciality">' + this.speciality + '</h5>' +
                                        '<div class="rating">';
                                for (var j = 1; j <= 5; j++) {
                                    if (j <= this.rating_value) {
                                        html += '<i class="fas fa-star filled"></i>';
                                    } else {
                                        html += '<i class="fas fa-star"></i>';
                                    }
                                }
                                html += '<span class="d-inline-block average-rating">(' + this.rating_count + ')</span>' +
                                        '</div>' +
                                        '<div class="clinic-details">' +
                                        '<p class="doc-location"><i class="fas fa-map-marker-alt"></i> ' + this.cityname + ', ' + this.countryname + '</p>' +
                                        '</div>' +
                                        '<div class="clinic-services">' + services + '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '<div class="doc-info-right">' +
                                        '<div class="clini-infos">' +
                                        '<ul>' +
                                        '<li><i class="far fa-comment"></i>' + this.rating_count + ' ' + lg_feedback + '</li>' +
                                        '<li><i class="fas fa-map-marker-alt"></i> ' + this.cityname + ', ' + this.countryname + '</li>' +
                                        '<li><i class="far fa-money-bill-alt"></i> ' + this.amount + ' </li>' +
                                        '</ul>' +
                                        '</div>' +
                                        '<div class="clinic-booking">' +
                                        '<a class="view-pro-btn" href="' + base_url + 'doctor-preview/' + this.username + '">' + lg_view_profile + '</a>' +
                                        '<a class="apt-btn" href="' + base_url + 'book-appoinments/' + this.username + '">' + lg_book_appointmen + '</a>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>';



                                location_items = {};
                                location_items["id"] = this.id;
                                location_items["doc_name"] = lg_dr + ' ' + this.first_name + ' ' + this.last_name;
                                location_items["speciality"] = services;
                                location_items["address"] = this.cityname + ', ' + this.countryname;
                                location_items["next_available"] = "Available on Fri, 22 Mar";
                                location_items["amount"] = this.amount;
                                location_items["lat"] = this.latitude;
                                location_items["lng"] = this.longitude;
                                location_items["icons"] = "default";
                                location_items["profile_link"] = base_url + 'doctor-preview/' + this.username;
                                location_items["total_review"] = this.rating_count + ' ' + lg_feedback;
                                location_items["image"] = this.profileimage;

                                locations.push(location_items);


                            });





                            initialize();


                            if (obj.current_page_no == 1) {
                                $("#doctor-list").html(html);
                            } else {
                                $("#doctor-list").append(html);
                            }

                        } else
                        {
                            var html = '<div class="card">' +
                                    '<div class="card-body">' +
                                    '<div class="doctor-widget">' +
                                    '<p>' + lg_no_doctors_foun + '</p>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                            $("#doctor-list").html(html);
                        }




                        var minimized_elements = $('h4.minimize');
                        minimized_elements.each(function () {
                            var t = $(this).text();
                            if (t.length < 100)
                                return;
                            $(this).html(
                                    t.slice(0, 100) + '<span>... </span><a href="#" class="more">' + lg_more + '</a>' +
                                    '<span style="display:none;">' + t.slice(100, t.length) + ' <a href="#" class="less">' + lg_less + '</a></span>'
                                    );
                        });

                        // $('a.more', minimized_elements).click(function(event) {
                        //   event.preventDefault();
                        //   $(this).hide().prev().hide();
                        //   $(this).next().show();
                        // });

                        // $('a.less', minimized_elements).click(function(event) {
                        //   event.preventDefault();
                        //   $(this).parent().hide().prev().show().prev().show();
                        // });

                        $(".search-results").html('<span>' + obj.count + ' ' + lg_matches_for_you + '' + '</span>');
                        // $(".widget-title").html(obj.count+' Matches for your search');
                        if (obj.count == 0) {
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                            return false;
                        }


                        if (obj.current_page_no == 1 && obj.count < 5) {
                            $('page_no_hidden').val(1);
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                            return false;
                        }



                        if (obj.total_page > obj.current_page_no && obj.total_page != 0) {
                            $('#load_more_btn').removeClass('d-none');
                            $('#no_more').addClass('d-none');
                        } else {
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                        }

                    }
                }

            });
        }



        $('#load_more_btn').click(function () {
            var page_no = $('#page_no_hidden').val();
            var current_page_no = 0;

            if (page_no == 1) {
                current_page_no = 2;
            } else {
                current_page_no = Number(page_no) + 1;
            }
            $('#page_no_hidden').val(current_page_no);
            search_doctor(1);
        });
    }
}


if (modules == 'home')
{
    if (pages == 'patients_search')
    {


        function reset_patient()
        {
            $('#orderby').val('');
            $('#search_patient_form')[0].reset();
            search_patient(0);
        }

        search_patient(0);
        function search_patient(load_more) {

            if (load_more == 0) {
                $('#page_no_hidden').val(1);
            }

            var order_by = $('#orderby').val();
            var keyword = $('#search_user').val();
            var page = $('#page_no_hidden').val();
            var gender = $("#gender").val();
            var blood_group = $("#blood_group").val();
            var city = $("#city").val();
            var state = $("#state").val();
            var country = $("#country").val();


            //$('#search-error').html('');

            $.ajax({
                url: base_url + 'home/search_patient',
                type: 'POST',
                data: {
                    gender: gender,
                    blood_group: blood_group,
                    order_by: order_by,
                    page: page,
                    keyword: keyword,
                    city: city,
                    state: state,
                    country: country
                },
                beforeSend: function () {
                    // $('#doctor-list').html('<div class="spinner-border text-success text-center" role="status"></div>');
                },
                success: function (response) {
                    //$('#doctor-list').html('');
                    if (response) {
                        var obj = $.parseJSON(response);
                        if (obj.data.length >= 1) {
                            var html = '';
                            $(obj.data).each(function () {


                                html += '<div class="col-md-6 col-lg-4 col-xl-3">' +
                                        '<div class="card widget-profile pat-widget-profile">' +
                                        '<div class="card-body">' +
                                        '<div class="pro-widget-content">' +
                                        '<div class="profile-info-widget">' +
                                        '<a href="javascript:void(0)" class="booking-doc-img">' +
                                        '<img src="' + this.profileimage + '" alt="User Image">' +
                                        '</a>' +
                                        '<div class="profile-det-info">' +
                                        '<h3><a href="javascript:void(0)">' + this.first_name + ' ' + this.last_name + '</a></h3>' +
                                        '<div class="patient-details">' +
                                        '<h5><b>' + lg_patient_id + ' :</b> #PT00' + this.user_id + '</h5>' +
                                        '<h5 class="mb-0"><i class="fas fa-map-marker-alt"></i> ' + this.cityname + ', ' + this.countryname + '</h5>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '<div class="patient-info">' +
                                        '<ul>' +
                                        '<li>' + lg_phone + ' <span>' + this.mobileno + '</span></li>' +
                                        '<li>' + lg_age + ' <span>' + this.age + ', ' + this.gender + '</span></li>' +
                                        '<li>' + lg_blood_group + ' <span>' + this.blood_group + '</span></li>' +
                                        '</ul>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>';
                            });

                            if (obj.current_page_no == 1) {
                                $("#patients-list").html(html);
                            } else {
                                $("#patients-list").append(html);
                            }



                        } else {

                            var html = '<div class="card" style="width:100%">' +
                                    '<div class="card-body">' +
                                    '<div class="doctor-widget">' +
                                    '<p>' + lg_no_patients_fou + '</p>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                            $("#patients-list").html(html);

                        }


                        var minimized_elements = $('h4.minimize');
                        minimized_elements.each(function () {
                            var t = $(this).text();
                            if (t.length < 100)
                                return;
                            $(this).html(
                                    t.slice(0, 100) + '<span>... </span><a href="#" class="more">' + lg_more + '</a>' +
                                    '<span style="display:none;">' + t.slice(100, t.length) + ' <a href="#" class="less">' + lg_less + '</a></span>'
                                    );
                        });


                        $(".search-results").html('<span>' + obj.count + ' ' + lg_matches_for_you + '' + '</span>');
                        // $(".widget-title").html(obj.count+' Matches for your search');
                        if (obj.count == 0) {
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                            return false;
                        }


                        if (obj.current_page_no == 1 && obj.count < 8) {
                            $('page_no_hidden').val(1);
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                            return false;
                        }



                        if (obj.total_page > obj.current_page_no && obj.total_page != 0) {
                            $('#load_more_btn').removeClass('d-none');
                            $('#no_more').addClass('d-none');
                        } else {
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                        }

                    }
                }

            });
        }



        $('#load_more_btn').click(function () {
            var page_no = $('#page_no_hidden').val();
            var current_page_no = 0;

            if (page_no == 1) {
                current_page_no = 2;
            } else {
                current_page_no = Number(page_no) + 1;
            }
            $('#page_no_hidden').val(current_page_no);
            search_patient(1);
        });
    }
}

if (modules == 'doctor')
{
    if (pages == 'schedule_timings')
    {

        $(document).ready(function () {

            $(document).on('click', '.timingsnav li a', function () {
                var day_id = $(this).attr('data-day-value');
                var append_html = $(this).attr('data-append-value');
                var day_name = $(this).text();
                $('#id_value').val(append_html);
                $('#day_id').val(day_id);
                $('#day_name').val(day_name);
                $('#slot_' + append_html).html('<div class="d-flex justify-content-center"><div class="spinner-grow text-success" style="width: 3rem; height: 3rem;" role="status"></div></div>');
                get_time_slot();
                setTimeout(function () {
                    $.post(base_url + 'schedule_timings/schedule_list', {day_id: day_id, day_name: day_name}, function (result) {
                        $('#slot_' + append_html).html(result);
                        //$('.overlay').hide();
                    });
                }, 500);

            });

            $(document).on('click', '.delete_schedule', function () {
                var delete_value = $(this).attr('data-delete-val');
                var append_html = $('#id_value').val();
                var c = confirm(lg_are_you_sure_to);
                if (c) {
                    $.post(base_url + 'schedule_timings/delete_schedule_time', {delete_value: delete_value}, function (res) {
                        if (res == 1) {
                            $('#' + append_html).click();
                        }
                    });
                }
            });

            $('#slots').change(function () {
                toastr.warning(lg_your_existing_s);
                $("#sunday").click();
            });

            $("#sunday").click();


            $("#schedule_form").validate({
                rules: {
                    from_time: "required",
                    to_time: "required",
                    type: "required"
                },
                messages: {
                    from_time: lg_please_select_f,
                    to_time: lg_please_select_t,
                    type: lg_please_select_t1
                },
                submitHandler: function (form) {
                    $.ajax({
                        url: base_url + 'schedule_timings/add_schedule',
                        data: $("#schedule_form").serialize(),
                        type: "POST",
                        beforeSend: function () {
                            $('#submit_btn').attr('disabled', true);
                            $('#submit_btn').html('<div class="spinner-border text-light" role="status"></div>');
                        },
                        success: function (res) {
                            $('#submit_btn').attr('disabled', false);
                            $('#submit_btn').html(lg_add10);

                            var obj = JSON.parse(res);

                            if (obj.status === 200)
                            {

                                var append_html = $('#id_value').val();
                                $('#' + append_html).click();
                                $('#add_time_slot').modal('hide');
                                $('#schedule_form')[0].reset();

                            } else
                            {
                                toastr.error(obj.msg);
                            }
                        }
                    });
                    return false;
                }
            });



        });

        function get_time_slot() {
            var slot = $('#slots').val()
            if (slot != '') {
                $.ajax({
                    type: "POST",
                    url: base_url + 'schedule_timings/get_available_time_slots',
                    data: {slot: $('#slots').val(), day_id: $('#day_id').val()},
                    beforeSend: function () {
                        $("#from_time option:gt(0)").remove();
                        $("#to_time option:gt(0)").remove();
                        $('#from_time,#to_time').find("option:eq(0)").html(lg_please_wait);
                    },
                    success: function (data) {
                        $('#from_time,#to_time').find("option:eq(0)").html(lg_select_time);
                        var obj = jQuery.parseJSON(data);
                        $(obj).each(function () {
                            var option = $('<option />');
                            if (this.added == true) {
                                option.attr('value', this.value).text(this.label);
                                option.attr('disabled', true);
                                option.addClass('d-none');
                            } else {
                                option.attr('value', this.value).text(this.label);
                            }
                            $('#from_time,#to_time').append(option);
                        });
                    }
                });
            } else {
                $("#from_time option:gt(0)").remove();
                $("#to_time option:gt(0)").remove();
            }
        }

        function get_to_time() {
            var slot = $('#slots').val();
            var from_time = $('#from_time').val();
            if (slot != '' && from_time != '') {
                $.ajax({
                    type: "POST",
                    url: base_url + 'schedule_timings/get_available_time_slots',
                    data: {slot: slot, day_id: $('#day_id').val(), from_time: from_time},
                    beforeSend: function () {
                        //$('.overlay').show();
                        $("#to_time option:gt(0)").remove();
                        $('#to_time').find("option:eq(0)").html(lg_please_wait);
                    },
                    success: function (data) {
                        // $('.overlay').hide();
                        $('#to_time').find("option:eq(0)").html(lg_select_time);
                        var obj = jQuery.parseJSON(data);
                        $(obj).each(function () {
                            var option = $('<option />');
                            if (this.added == true) {
                                option.attr('value', this.value).text(this.label);
                                option.attr('disabled', true);
                                option.addClass('d-none');
                            } else {
                                option.attr('value', this.value).text(this.label);
                            }
                            $('#to_time').append(option);
                        });
                    }
                });

            }
        }

        function add_slot()
        {
            var slot = $('#slots').val();

            if (slot == '')
            {
                toastr.error(lg_please_select_s2);
            } else
            {
                get_time_slot();
                $('#slot').val(slot);
                $('#add_time_slot').modal('show');
            }
        }
        function getDates (startDate, endDate) {
            const dates = []
            let currentDate = startDate
            const addDays = function (days) {
                const date = new Date(this.valueOf())
                date.setDate(date.getDate() + days)
                return date
            }
            while (currentDate <= endDate) {
                dates.push(currentDate)
                currentDate = addDays.call(currentDate, 1)
            }
            return dates
        }
        const ele = $("#scheduling-dates .schedule-dates-track");
        const date_track = $(".schedule-dates-track");
        const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        const now = new Date();
        let curr_year = now.getFullYear();
        let curr_mon = now.getMonth();
        let curr_day = now.getDay();
        console.log(curr_year)
        const dates = getDates( new Date(curr_year,curr_mon ,curr_day ), new Date( curr_year, curr_mon + 1, curr_day))
        dates.forEach(function (date) {

            var d= new Date(date);
            ele.append(`
                <div class="date-block">
                    <h5>${d.getDate()}</h5>
                    <h6>${days[d.getDay()]}</h6>
                </div>
            `)
            console.log(d.getDate())
            console.log(days[d.getDay()]);
        });
        const date_next = $("#scheduling-dates-next");
        const date_prev = $("#scheduling-dates-prev");
        date_next.on('click', function(){
           changeDateTrackLeftPosition('+');
        });
        date_prev.on('click', function(){
            changeDateTrackLeftPosition('-');
        });

        function changeDateTrackLeftPosition(value){
            const block_size = 77 * 3;
            const visible_length = 77 * 11;
            var scroll_width = date_track[0].scrollWidth;
            var current_position = parseInt(date_track.css('left'));

            if(value == '+' && current_position < (scroll_width-visible_length)){
                date_track.css('left', `calc(${current_position}px - ${block_size}px)`);
            }
            if(value == '-'){
                if(current_position >= 0 ){
                    date_track.css('left', '0px');
                }
                else{
                    date_track.css('left', `calc(${current_position}px + ${block_size}px)`);
                }

            }

            console.log(scroll_width-visible_length);
            console.log(current_position);
            // date_track.css('left', value);
        }

    //     $("#scheduling-dates").slick({
    //     infinite: false,
    //     slidesToShow: 9,
    //     slidesToScroll: 1,
    //     responsive: [
    //     {
    //         breakpoint: 1024,
    //         settings: {
    //             slidesToShow: 9,
    //             slidesToScroll: 3,
    //             infinite: true,
    //             dots: true
    //         }
    //     },
    //     {
    //         breakpoint: 600,
    //         settings: {
    //             slidesToShow: 9,
    //             slidesToScroll: 2
    //         }
    //     },
    //     {
    //         breakpoint: 480,
    //         settings: {
    //             slidesToShow: 1,
    //             slidesToScroll: 1
    //         }
    //     }
    //
    // ]
    // });

        // moment().format("MMM Do YY");
    }

}

if (modules == 'patient')
{
    if (pages == 'book_appoinments')
    {

        function getSchedule(id)
        {

            $('.bookingconfirmation strong').html('0 slot');
            if (id != '') {
                if (id == 1) {
                    var selected_date = $('#pre_date').val();
                } else if (id == 2) {
                    var selected_date = $('#next_date').val();
                }
            } else {
                var date = $('#schedule_date').val();
                var selected_date = date.split("/").reverse().join("-");
                if (selected_date == '') {
                    $('#schedule_date_error').html('<small class="help-block" data-bv-validator="notEmpty" data-bv-for="schedule_date" data-bv-result="INVALID" style="color:red;">' + lg_date_is_require + '</small>');
                    return false;
                }

            }

            $('#schedule_date_error').html('');
            var doctor_id = $('#doctor_id').val();
            $.post(base_url + 'book_appoinments/get_schedule_from_date', {selected_date: selected_date, doctor_id: doctor_id}, function (response) {
                $('.bookings-schedule').html(response);
            });
        }

        $(document).ready(function () {

            $('#schedule_date').datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            });

            $('input[name="type"]').click(function () {
                var type = $("input[name='type']:checked").val();

                if (type == 'online') {
                    $('.online').show();
                    $('.clinic').hide();
                } else if (type == 'clinic') {
                    $('.online').hide();
                    $('.clinic').show();
                } else if (type == 'both') {
                    $('.online,.clinic').show();
                }

            });

            $(document).on('click', '.available_slot', function () {

                if ($(this).hasClass("selected")) {
                    $(this).removeClass('selected');
                } else {
                    $(this).addClass('selected');
                }
                var selected_count = $('.selected').length;
                $('#selected_count').val(selected_count);
                if (selected_count == 1 || selected_count == 0) {
                    $('.bookingconfirmation strong').html(selected_count + ' slot');
                } else {
                    $('.bookingconfirmation strong').html(selected_count + ' slots');
                }

                if (selected_count == 0) {
                    $('.bookingconfirmation a').addClass('disabled');
                } else {
                    $('.bookingconfirmation a').removeClass('disabled');
                }


            });

            $(document).on('click', '.bookingconfirmation a', function () {
                var selected_count = $('.selected').length;
                var doctor_id = $('#doctor_id').val();
                var doctor_username = $('#doctor_username').val();
                var hourly_rate = $('#hourly_rate').val();
                var appointment_data = [];
                $('.selected').each(function (index, value) {

                    appointment_data.push({'date_value': $(this).attr('data-date'),
                        'day_id': $(this).attr('data-day-id'),
                        'day_name': $(this).attr('data-day'),
                        'start_time': $(this).attr('data-start-time'),
                        'end_time': $(this).attr('data-end-time'),
                        'time_zone': $(this).attr('data-timezone'),
                        'type': $(this).attr('data-type'),
                        'hour': '1'
                    });
                });

                if (appointment_data.length <= 0)
                {
                    toastr.warning(lg_please_select_a);
                    return false;
                }
                var appointment_details = JSON.stringify(appointment_data);

                $('.bookingconfirmation a').addClass('disabled');
                $('.bookingconfirmation a').html('<div class="spinner-border text-light" role="status"></div>');
                $.post(base_url + 'book_appoinments/set_booked_session', {
                    selected_count: $('#selected_count').val(),
                    hourly_rate: hourly_rate,
                    doctor_username: doctor_username,
                    appointment_details: appointment_details,
                    price_type: $('#price_type').val()

                }, function (res) {



                    var obj = JSON.parse(res);
                    if (obj.status === 200)
                    {
                        setTimeout(function () {
                            window.location = base_url + 'checkout';
                        }, 1000);
                    } else
                    {
                        toastr.success(lg_appoinment_requ);
                        setTimeout(function () {
                            window.location.href = base_url + "dashboard";
                        }, 3000);

                    }
                });

            });

        });

        function getDates (startDate, endDate) {
            const dates = []
            let currentDate = startDate
            const addDays = function (days) {
                const date = new Date(this.valueOf())
                date.setDate(date.getDate() + days)
                return date
            }
            while (currentDate <= endDate) {
                dates.push(currentDate)
                currentDate = addDays.call(currentDate, 1)
            }
            return dates
        }

        const ele = $("#scheduling-dates .schedule-dates-track");
        const date_track = $(".schedule-dates-track");
        const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        const now = new Date();
        let curr_year = now.getFullYear();
        let curr_mon = now.getMonth();
        let curr_day = now.getDay();
        const dates = getDates( new Date(curr_year,curr_mon ,curr_day ), new Date( curr_year, curr_mon + 1, curr_day))
        dates.forEach(function (date) {

            var d= new Date(date);
            ele.append(`
                <div class="date-block">
                    <h5>${d.getDate()}</h5>
                    <h6>${days[d.getDay()]}</h6>
                </div>
            `)
        });
        const date_next = $("#scheduling-dates-next");
        const date_prev = $("#scheduling-dates-prev");
        date_next.on('click', function(){
            changeDateTrackLeftPosition('+');
        });
        date_prev.on('click', function(){
            changeDateTrackLeftPosition('-');
        });

        function changeDateTrackLeftPosition(value){
            const block_size = 77 * 3;
            const visible_length = 77 * 11;
            var scroll_width = date_track[0].scrollWidth;
            var current_position = parseInt(date_track.css('left'));

            if(value == '+' && current_position < (scroll_width-visible_length)){
                date_track.css('left', `calc(${current_position}px - ${block_size}px)`);
            }
            if(value == '-'){
                if(current_position >= 0 ){
                    date_track.css('left', '0px');
                }
                else{
                    date_track.css('left', `calc(${current_position}px + ${block_size}px)`);
                }

            }

            console.log(scroll_width-visible_length);
            console.log(current_position);
            // date_track.css('left', value);
        }

    }

}

if (modules == 'patient')
{
    if (pages == 'checkout')
    {
        function appoinment_payment(type)
        {
            // var terms_accept=$("input[name='terms_accept']:checked").val();
            var terms_accept = 1;
            if (terms_accept == '1')
            {
                if (type == 'paypal')
                {
                    $('#payment_formid').submit();

                } else if (type == 'razorpay')
                {
                    razorpay();

                } else
                {
                    var payment_method = $("input[name='payment_methods']:checked").val();
                    if (payment_method != 'Card Payment')
                    {
                        $("#my_book_appoinment").click();
                    }

                    return false;
                }
            } else
            {
                toastr.warning(lg_please_accept_t);
            }
        }

        function razorpay()
        {
            $('#razor_pay_btn').attr('disabled', true);
            $('#razor_pay_btn').html('<div class="spinner-border text-light" role="status"></div>');
            var amount = $('#amount').val();
            $.post(base_url + 'book_appoinments/create_razorpay_orders', {amount: amount}, function (data) {

                $('#razor_pay_btn').attr('disabled', false);
                $('#razor_pay_btn').html('Confirm and Pay with Razorpay');
                var obj = jQuery.parseJSON(data);
                var options = {
                    "key": obj.key_id,
                    "amount": obj.amount,
                    "currency": obj.currency,
                    "name": obj.sitename,
                    "description": "Booking Slot",
                    "image": obj.siteimage,
                    "order_id": obj.order_id,
                    "handler": function (response) {
                        razorpay_appoinments(response.razorpay_payment_id, response.razorpay_order_id, response.razorpay_signature);
                    },
                    "prefill": {
                        "name": obj.patientname,
                        "email": obj.email,
                        "contact": obj.mobileno
                    },
                    "notes": {
                        "address": "Razorpay Corporate Office"
                    },
                    "theme": {
                        "color": "#F37254"
                    }
                };

                var rzp1 = new Razorpay(options);
                rzp1.open();


            });
        }

        function razorpay_appoinments(payment_id, order_id, signature)
        {

            $('#payment_id').val(payment_id);
            $('#order_id').val(order_id);
            $('#signature').val(signature);

            $.ajax({

                url: base_url + 'book_appoinments/razorpay_appoinments',

                data: $('#payment_formid').serialize(),

                type: 'POST',

                dataType: 'JSON',

                beforeSend: function () {
                    $('#razor_pay_btn').attr('disabled', true);
                    $('#razor_pay_btn').html('<div class="spinner-border text-light" role="status"></div>');
                },

                success: function (response) {
                    // $('.overlay').hide();
                    if (response.status == '200')
                    {

                        toastr.success(lg_transaction_suc);
                        setTimeout(function () {
                            window.location.href = base_url + 'dashboard';
                        }, 2000);

                    } else
                    {
                        toastr.error(lg_transaction_fai1);
                        setTimeout(function () {
                            window.location.href = base_url + 'dashboard';
                        }, 2000);
                    }



                },

                error: function (error) {

                    console.log(error);

                }

            });
        }

        var stripe = Stripe(stripe_api_key);

// Create an instance of Elements.
        var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

// Create an instance of the card Element.
        var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

// Handle real-time validation errors from the card Element.
        card.addEventListener('change', function (event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

// Handle form submission.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function (event) {
            event.preventDefault();

            stripe.createToken(card).then(function (result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                }
            });
        });

// Submit the form with the token ID.
        function stripeTokenHandler(token) {

            $('#access_token').val(token.id);

            $.ajax({

                url: base_url + 'book_appoinments/stripe_pay',

                data: $('#payment_formid').serialize(),

                type: 'POST',

                dataType: 'JSON',

                beforeSend: function () {
                    $('#stripe_pay_btn').attr('disabled', true);
                    $('#stripe_pay_btn').html('<div class="spinner-border text-light" role="status"></div>');
                },

                success: function (response) {
                    // $('.overlay').hide();
                    if (response.status == '200')
                    {

                        toastr.success(lg_transaction_suc);
                        setTimeout(function () {
                            window.location.href = base_url + 'dashboard';
                        }, 2000);

                    } else
                    {
                        toastr.error(lg_transaction_fai1);
                        setTimeout(function () {
                            window.location.href = base_url + 'dashboard';
                        }, 2000);
                    }



                },

                error: function (error) {

                    console.log(error);

                }

            });
        }
        $(document).ready(function () {

            $('#my_book_appoinment').click(function (e) {

                $.ajax({

                    url: base_url + 'book_appoinments/clinic_appoinments',

                    data: $('#payment_formid').serialize(),

                    type: 'POST',

                    dataType: 'JSON',

                    beforeSend: function () {

                        $('#pay_button').attr('disabled', true);
                        $('#pay_button').html('<div class="spinner-border text-light" role="status"></div>');

                    }, success: function (response) {

                        if (response.status == '200')
                        {

                            toastr.success(lg_transaction_suc);
                            setTimeout(function () {
                                window.location.href = base_url + 'dashboard';
                            }, 2000);

                        } else
                        {
                            toastr.error(lg_transaction_fai1);
                            setTimeout(function () {
                                window.location.href = base_url + 'dashboard';
                            }, 2000);
                        }



                    },

                    error: function (error) {

                        console.log(error);

                    }

                });

                e.preventDefault();

            });

        });

        $(document).ready(function () {
            $('input[type=radio][name=payment_methods]').change(function () {
                if (this.value == 'Card Payment') {
                    $('.stripe_payment').show();
                    $('.paypal_payment').hide();
                    $('.clinic_payment').hide();
                    $('.razorpay_payment').hide();
                    $('#payment_method').val('Card Payment');

                } else if (this.value == 'PayPal') {
                    $('.stripe_payment').hide();
                    $('.paypal_payment').show();
                    $('.clinic_payment').hide();
                    $('.razorpay_payment').hide();
                    $('#payment_method').val('Card Payment');

                } else if (this.value == 'Pay on Arrive') {
                    $('.stripe_payment').hide();
                    $('.paypal_payment').hide();
                    $('.clinic_payment').show();
                    $('.razorpay_payment').hide();
                    $('#payment_method').val('Pay on Arrive');

                } else if (this.value == 'Razorpay') {
                    $('.stripe_payment').hide();
                    $('.paypal_payment').hide();
                    $('.clinic_payment').hide();
                    $('.razorpay_payment').show();
                    $('#payment_method').val('Card Payment');

                } else {
                    $('.stripe_payment').hide();
                    $('.paypal_payment').hide();
                    $('.clinic_payment').hide();
                    $('.razorpay_payment').hide();
                    $('#payment_method').val('');
                }
            });
        });

    }
}

if (modules == 'doctor' || modules == 'patient') {

    if (pages == 'doctor_dashboard' || pages == 'patient_dashboard') {
        function email_verification()
        {
            $.get(base_url + 'dashboard/send_verification_email', function (data) {
                toastr.success('Activation mail has been sent your registered mail id');
            });
        }

        $('[data-target="verifyModal"]').on('click', function(){
            $("#verifyModal").modal('show');
        });
        $('#verifyByOTP').on('click', function(){
            $.ajax({
                url: base_url+'send_verify_otp',
                type: "GET",
                success: function (result){

                    $("#verifyModal .modal-dialog").html(result);
                }
            });

        })
    }
    if (pages == 'doctor_dashboard') {

        var appoinment_table;

        appoinment_table = $('#appoinments_table').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'dashboard/appoinments_list',
                "type": "POST",
                "data": function (data) {
                    data.type = $('#type').val();
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });


        function appoinments_table(type)
        {
            $('#type').val(type);
            appoinment_table.ajax.reload(null, false); //reload datatable ajax
        }

        appoinments_table(1);

        function show_appoinments_modal(app_date, book_date, amount, type) {
            $('.app_date').html(app_date);
            $('.book_date').html(book_date);
            $('.amount').html(amount);
            $('.type').html(type);
            $('#appoinments_details').modal('show');

        }

    }

    if (pages == 'my_patients') {
        my_patient(0);
        function my_patient(load_more) {

            if (load_more == 0) {
                $('#page_no_hidden').val(1);
            }

            var page = $('#page_no_hidden').val();


            //$('#search-error').html('');

            $.ajax({
                url: base_url + 'my_patients/patient_list',
                type: 'POST',
                data: {page: page},
                beforeSend: function () {
                    // $('#doctor-list').html('<div class="spinner-border text-success text-center" role="status"></div>');
                },
                success: function (response) {
                    //$('#doctor-list').html('');
                    if (response) {
                        var obj = $.parseJSON(response);
                        if (obj.data.length >= 1) {
                            var html = '';
                            $(obj.data).each(function () {


                                html += '<div class="col-md-6 col-lg-4 col-xl-3">' +
                                        '<div class="card widget-profile pat-widget-profile">' +
                                        '<div class="card-body">' +
                                        '<div class="pro-widget-content">' +
                                        '<div class="profile-info-widget">' +
                                        '<a href="' + base_url + 'mypatient-preview/' + this.userid + '" class="booking-doc-img">' +
                                        '<img src="' + this.profileimage + '" alt="User Image">' +
                                        '</a>' +
                                        '<div class="profile-det-info">' +
                                        '<h3><a href="' + base_url + 'mypatient-preview/' + this.userid + '">' + this.first_name + ' ' + this.last_name + '</a></h3>' +
                                        '<div class="patient-details">' +
                                        '<h5><b>' + lg_patient_id + ' :</b> #PT00' + this.user_id + '</h5>' +
                                        '<h5 class="mb-0"><i class="fas fa-map-marker-alt"></i> ' + this.cityname + ', ' + this.countryname + '</h5>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '<div class="patient-info">' +
                                        '<ul>' +
                                        '<li>' + lg_phone + ' <span>' + this.mobileno + '</span></li>' +
                                        '<li>' + lg_age + ' <span>' + this.age + ', ' + this.gender + '</span></li>' +
                                        '<li>' + lg_blood_group + ' <span>' + this.blood_group + '</span></li>' +
                                        '</ul>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>';
                            });

                            if (obj.current_page_no == 1) {
                                $("#patients-list").html(html);
                            } else {
                                $("#patients-list").append(html);
                            }



                            if (obj.count == 0) {
                                $('#load_more_btn').addClass('d-none');
                                $('#no_more').removeClass('d-none');
                                return false;
                            }


                            if (obj.current_page_no == 1 && obj.count < 8) {
                                $('page_no_hidden').val(1);
                                $('#load_more_btn').addClass('d-none');
                                $('#no_more').removeClass('d-none');
                                return false;
                            }



                            if (obj.total_page > obj.current_page_no && obj.total_page != 0) {
                                $('#load_more_btn').removeClass('d-none');
                                $('#no_more').addClass('d-none');
                            } else {
                                $('#load_more_btn').addClass('d-none');
                                $('#no_more').removeClass('d-none');
                            }

                        } else {
                            var html = '<div class="appointment-list">' +
                                    '<div class="profile-info-widget">' +
                                    '<p>' + lg_no_patients_fou + '</p>' +
                                    '</div>' +
                                    '</div>';
                            $("#patients-list").html(html);
                        }

                    }
                }

            });
        }



        $('#load_more_btn').click(function () {
            var page_no = $('#page_no_hidden').val();
            var current_page_no = 0;

            if (page_no == 1) {
                current_page_no = 2;
            } else {
                current_page_no = Number(page_no) + 1;
            }
            $('#page_no_hidden').val(current_page_no);
            my_patient(1);
        });
    }

    if (pages == 'mypatient_preview' || pages == 'patient_dashboard') {

        var appoinment_table;
        appoinment_table = $('#appoinment_table').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'my_patients/appoinments_list',
                "type": "POST",
                "data": function (data) {
                    data.patient_id = $('#patient_id').val();
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });


        function appoinments_table()
        {
            appoinment_table.ajax.reload(null, false);
        }

        var prescription_table;
        prescription_table = $('#prescription_table').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'my_patients/prescriptions_list',
                "type": "POST",
                "data": function (data) {
                    data.patient_id = $('#patient_id').val();
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });


        function prescriptions_table()
        {
            prescription_table.ajax.reload(null, false);
        }

        function view_prescription(pre_id) {
            $('.overlay').show();
            $.post(base_url + 'my_patients/get_prescription_details', {pre_id: pre_id}, function (res) {
                var obj = jQuery.parseJSON(res);
                var table = '<table class="table table-bordered table-hover">' +
                        '<thead>' +
                        '<tr>' +
                        '<th>' + lg_sno + '</th>' +
                        '<th>' + lg_drug_name + '</th>' +
                        '<th>' + lg_quantity + '</th>' +
                        '<th>' + lg_type + '</th>' +
                        '<th>' + lg_days + '</th>' +
                        '<th>' + lg_time + '</th>' +
                        '</tr>' +
                        '<tbody>';
                var i = 1;
                $(obj).each(function () {
                    var j = i++;

                    table += '<tr>' +
                            '<td>' + j + '</td>' +
                            '<td>' + this.drug_name + '</td>' +
                            '<td>' + this.qty + '</td>' +
                            '<td>' + this.type + '</td>' +
                            '<td>' + this.days + '</td>' +
                            '<td>' + this.time + '</td>' +
                            '</tr>';
                });
                table += '</tbody>' +
                        '</table>' +
                        '<div class="float-right">' +
                        '<img src="' + base_url + obj[0].img + '" style="width:150px"><br>' +
                        '( DR. ' + obj[0].doctor_name.charAt(0).toUpperCase() + obj[0].doctor_name.slice(1) + ' ) <br>' +
                        '<div>Doctor Signature</div><br>' +
                        '</div>';
                $('#patient_name').text(obj[0].patient_name);
                $('#view_date').text(obj[0].prescription_date);
                $('.view_title').text(lg_prescription);
                $('.view_details').html(table);
                $('#view_modal').modal('show');
                $('.overlay').hide();
            });

        }

        function delete_prescription(id)
        {
            $('#delete_id').val(id);
            $('#delete_table').val('prescription');
            $('#delete_title').text(lg_prescription);
            $('#delete_modal').modal('show');
        }


        var billing_table;
        billing_table = $('#billing_table').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'my_patients/billing_list',
                "type": "POST",
                "data": function (data) {
                    data.patient_id = $('#patient_id').val();
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });


        function billings_table()
        {
            billing_table.ajax.reload(null, false);
        }

        function view_billing(id) {
            $('.overlay').show();
            $.post(base_url + 'my_patients/get_billing_details', {id: id}, function (res) {
                var obj = jQuery.parseJSON(res);
                var table = '<table class="table table-bordered table-hover">' +
                        '<thead>' +
                        '<tr>' +
                        '<th>' + lg_sno + '</th>' +
                        '<th>' + lg_name + '</th>' +
                        '<th>' + lg_amount + '</th>' +
                        '</tr>' +
                        '<tbody>';
                var i = 1;
                $(obj).each(function () {
                    var j = i++;
                    table += '<tr>' +
                            '<td>' + j + '</td>' +
                            '<td>' + this.name + '</td>' +
                            '<td>' + this.amount + '</td>' +
                            '</tr>';
                });
                table += '</tbody>' +
                        '</table>' +
                        '<div class="float-right">' +
                        '<img src="' + base_url + obj[0].img + '" style="width:150px"><br>' +
                        '( DR. ' + obj[0].doctor_name.charAt(0).toUpperCase() + obj[0].doctor_name.slice(1) + ' ) <br>' +
                        '<div>Doctor Signature</div><br>' +
                        '</div>';
                $('#patient_name').text(obj[0].patient_name);
                $('#view_date').text(obj[0].billing_date);
                $('.view_title').text(lg_doctor_billing);
                $('.view_details').html(table);
                $('#view_modal').modal('show');
            });

        }



        function delete_billing(id)
        {
            $('#delete_id').val(id);
            $('#delete_table').val('billing');
            $('#delete_title').text(lg_bill4);
            $('#delete_modal').modal('show');
        }


        $('#medical_records_form').submit(function (e) {
            e.preventDefault();
            var oFile = document.getElementById("user_files_mr").files[0];
            if (!document.getElementById("user_files_mr").files[0]) {
                toastr.warning(lg_please_upload_m);
                return false;
            }
            if (oFile) {

                if (oFile.size > 25097152) { // 25 mb for bytes.

                    toastr.warning(lg_file_size_must_);
                    return false;
                }
            }

            var formData = new FormData($('#medical_records_form')[0]);
            $.ajax({
                url: base_url + 'my_patients/upload_medical_records',
                type: 'POST',
                data: formData,
                beforeSend: function () {
                    if (oFile) {
                        $('#medical_btn').attr('disabled', true);
                        $('#medical_btn').html('<div class="spinner-border text-light" role="status"></div>');
                    }
                },
                success: function (res) {
                    $('#medical_btn').attr('disabled', false);
                    $('#medical_btn').html(lg_submit);
                    $('#add_medical_records').modal('hide');
                    var obj = jQuery.parseJSON(res);
                    if (obj.status === 500) {
                        toastr.warning(obj.msg);
                        $('#user_files_mr').val('');

                    } else {
                        $('#medical_records_form')[0].reset();
                        toastr.success(obj.msg);
                        medical_records_table();
                    }

                },
                error: function (data) {
                    //alert('error');

                },
                cache: false,
                contentType: false,
                processData: false
            });
            return false
        });

        var medical_record_table;
        medical_record_table = $('#medical_records_table').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'my_patients/medical_records_list',
                "type": "POST",
                "data": function (data) {
                    data.patient_id = $('#patient_id').val();
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });


        function medical_records_table()
        {
            medical_record_table.ajax.reload(null, false);
        }

        function delete_medical_records(id)
        {
            $('#delete_id').val(id);
            $('#delete_table').val('medical_records');
            $('#delete_title').text(lg_medical_records);
            $('#delete_modal').modal('show');
        }



        function delete_details()
        {
            var id = $('#delete_id').val();
            var delete_table = $('#delete_table').val();
            $('#delete_btn').attr('disabled', true);
            $('#delete_btn').html('<div class="spinner-border text-light" role="status"></div>');
            $.post(base_url + 'my_patients/delete_details', {id: id, delete_table, delete_table}, function (res) {

                if (delete_table == 'prescription')
                    ;
                {
                    prescriptions_table();
                }

                if (delete_table == 'medical_records')
                    ;
                {
                    medical_records_table();
                }

                if (delete_table == 'billing')
                    ;
                {
                    billings_table();
                }

                $('#delete_btn').attr('disabled', false);
                $('#delete_btn').html(lg_yes);
                $('#delete_modal').modal('hide');


            });
        }

    }

    if (pages == 'add_prescription' || pages == 'edit_prescription' || pages == 'add_billing' || pages == 'edit_billing') {

        var wrapper = document.getElementById("signature-pad"),
                clearButton = wrapper.querySelector("[data-action=clear]"),
                saveButton = wrapper.querySelector("[data-action=save]"),
                canvas = wrapper.querySelector("canvas"),
                signaturePad;


        function resizeCanvas() {

            var ratio = window.devicePixelRatio || 1;
            canvas.width = canvas.offsetWidth * ratio;
            canvas.height = canvas.offsetHeight * ratio;
            canvas.getContext("2d").scale(ratio, ratio);
        }

        signaturePad = new SignaturePad(canvas);
        clearButton.addEventListener("click", function (event) {
            signaturePad.clear();
        });

        saveButton.addEventListener("click", function (event) {

            if (signaturePad.isEmpty()) {
                console.log('You should sign!')
            } else {
                $.ajax({
                    type: "POST",
                    url: base_url + 'my_patients/insert_signature',
                    data: {'image': signaturePad.toDataURL(), 'rowno': $('#rowno').val()},
                    beforeSend: function () {
                        $('#save2').attr('disabled', true);
                        $('#save2').html('<div class="spinner-border text-light" role="status"></div>');
                    },
                    success: function (res) {
                        $('#save2').attr('disabled', false);
                        $('#save2').html('Save');
                        signaturePad.clear();
                        $('#sign-modal').modal('hide');
                        $('.doctor_signature').html('');
                        $('.doctor_signature').html(res);
                        $('.doctor_signature').removeClass('doctor_signature');

                    }
                });
            }
        });

        $('.doctor_signature').click(function () {
            show_modal();
        });

        function show_modal() {
            $('#sign-modal').modal('show');
            $('#edit').addClass('doctor_signature');
        }


        function delete_row(id) {
            $('#delete_' + id).remove();
            //console.log(row_count);
        }

        function isNumberKey(evt)
        {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

    }

    if (pages == 'add_prescription' || pages == 'edit_prescription') {


        function add_more_row() {
            var hidden_count = $('#hidden_count').val();
            var total = Number(hidden_count) + 1;
            $('#hidden_count').val(total);


            var append_rows = '<tr id="delete_' + total + '">' +
                    '<td><input type="text" name="drug_name[]" id="drug_name' + total + '" class="form-control filter-form"></td>' +
                    '<td style="min-width: 100px; max-width: 100px;"><input type="text" onkeypress="return isNumberKey(event)" name="qty[]" id="qty' + total + '" class="form-control filter-form" maxlength="4"></td>' +
                    '<td><select class="form-control filter-form" name="type[]" id="type' + total + '">' +
                    '<option value="">' + lg_select_type + '</option>' +
                    '<option value="Before Food">' + lg_before_food + '</option>' +
                    '<option value="After Food">' + lg_after_food + '</option>' +
                    '</select>' +
                    '</td>' +
                    '<td style="min-width: 100px; max-width: 100px;"><input onkeypress="return isNumberKey(event)" type="text" name="days[]" id="days' + total + '" class="form-control filter-form text" maxlength="4" autocomplete="off"></td>' +
                    '<td>' +
                    '<div class="row">' +
                    '<div class="col-md-6">' +
                    '<input type="checkbox" name="time' + total + '[]" value="Morning" id="morning' + total + '"><label for="morning' + total + '">&nbsp;&nbsp;' + lg_morning + '</label>' +
                    '</div>' +
                    '<div class="col-md-6">' +
                    '<input type="checkbox" name="time' + total + '[]" value="Afternoon" id="afternoon' + total + '"><label for="afternoon' + total + '">&nbsp;&nbsp;' + lg_afternoon + '</label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="row">' +
                    '<div class="col-md-6">' +
                    '<input type="checkbox" name="time' + total + '[]" value="Evening" id="evening' + total + '"><label for="evening' + total + '">&nbsp;&nbsp;' + lg_evening + '</label>' +
                    '</div>' +
                    '<div class="col-md-6">' +
                    '<input type="checkbox" name="time' + total + '[]" value="Night" id="night' + total + '"><label for="night' + total + '">&nbsp;&nbsp;' + lg_night + '</label>' +
                    '</div>' +
                    '</div>' +
                    '</td>' +
                    '<td>' +
                    '<a href="javascript:void(0)" class="btn bg-danger-light trash" onclick="delete_row(' + total + ')"><i class="far fa-trash-alt"></i></a>' +
                    '</td>' +
                    '</tr>';


            $('#add_more_rows').append(append_rows);

        }



        $(document).ready(function () {
            $("#add_prescription").validate({
                rules: {
                    "drug_name[]": "required",
                    "qty[]": "required",
                    "type[]": "required",
                    "days[]": "required",
                    "time[]": "required"
                },
                messages: {
                    "drug_name[]": lg_please_enter_dr,
                    "qty[]": lg_please_enter_qt,
                    "type[]": lg_please_select_t1,
                    "days[]": lg_please_enter_da,
                    "time[]": lg_please_select_t2
                },
                submitHandler: function (form) {
                    var signature_id = $('#signature_id').val();
                    if (signature_id == 0) { /* Signature validation */
                        toastr.error(lg_please_sign_to_);
                        return false;
                    }
                    $.ajax({
                        url: base_url + 'my_patients/save_prescription',
                        data: $("#add_prescription").serialize(),
                        type: "POST",
                        beforeSend: function () {
                            $('#prescription_save_btn').attr('disabled', true);
                            $('#prescription_save_btn').html('<div class="spinner-border text-light" role="status"></div>');
                        },
                        success: function (res) {
                            $('#prescription_save_btn').attr('disabled', false);
                            $('#prescription_save_btn').html(lg_save);

                            var obj = JSON.parse(res);

                            if (obj.status === 200)
                            {
                                toastr.success(obj.msg);
                                setTimeout(function () {
                                    window.location.href = base_url + 'mypatient-preview/' + obj.patient_id;
                                }, 3000);

                            } else
                            {
                                toastr.error(obj.msg);
                            }
                        }
                    });
                    return false;
                }
            });

            $("#update_prescription").validate({
                rules: {
                    "drug_name[]": "required",
                    "qty[]": "required",
                    "type[]": "required",
                    "days[]": "required",
                    "time[]": "required"
                },
                messages: {
                    "drug_name[]": lg_please_enter_dr,
                    "qty[]": lg_please_enter_qt,
                    "type[]": lg_please_select_t1,
                    "days[]": lg_please_enter_da,
                    "time[]": lg_please_select_t2
                },
                submitHandler: function (form) {
                    var signature_id = $('#signature_id').val();
                    if (signature_id == 0) { /* Signature validation */
                        toastr.error(lg_please_sign_to_);
                        return false;
                    }
                    $.ajax({
                        url: base_url + 'my_patients/update_prescription',
                        data: $("#update_prescription").serialize(),
                        type: "POST",
                        beforeSend: function () {
                            $('#prescription_update_btn').attr('disabled', true);
                            $('#prescription_update_btn').html('<div class="spinner-border text-light" role="status"></div>');
                        },
                        success: function (res) {
                            $('#prescription_update_btn').attr('disabled', false);
                            $('#prescription_update_btn').html(lg_save);

                            var obj = JSON.parse(res);

                            if (obj.status === 200)
                            {
                                toastr.success(obj.msg);
                                setTimeout(function () {
                                    window.location.href = base_url + 'mypatient-preview/' + obj.patient_id;
                                }, 3000);

                            } else
                            {
                                toastr.error(obj.msg);
                            }
                        }
                    });
                    return false;
                }
            });


        });



    }

    if (pages == 'add_billing' || pages == 'edit_billing') {

        function add_more_row() {
            var hidden_count = $('#hidden_count').val();
            var total = Number(hidden_count) + 1;
            $('#hidden_count').val(total);
            var append_rows = '<tr id="delete_' + total + '">' +
                    '<td>' +
                    '<input name="name[]" id="name' + total + '" class="form-control filter-form" >' +
                    '<td>' +
                    '<input name="amount[]" onkeypress="return isNumberKey(event)" id="amount' + total + '" class="form-control filter-form" >' +
                    '</td>' +
                    '<td><a href="javascript:void(0)" class="btn bg-danger-light trash" onclick="delete_row(' + total + ')"><i class="far fa-trash-alt"></i></a></td>' +
                    '</tr>';

            $('#add_more_rows').append(append_rows);

        }

        $(document).ready(function () {
            $("#add_billing").validate({
                rules: {
                    "name[]": "required",
                    "amount[]": "required"
                },
                messages: {
                    "name[]": lg_please_enter_na,
                    "amount[]": lg_please_enter_am
                },
                submitHandler: function (form) {
                    var signature_id = $('#signature_id').val();
                    if (signature_id == 0) { /* Signature validation */
                        toastr.error(lg_please_sign_to_);
                        return false;
                    }
                    $.ajax({
                        url: base_url + 'my_patients/save_billing',
                        data: $("#add_billing").serialize(),
                        type: "POST",
                        beforeSend: function () {
                            $('#bill_save_btn').attr('disabled', true);
                            $('#bill_save_btn').html('<div class="spinner-border text-light" role="status"></div>');
                        },
                        success: function (res) {
                            $('#bill_save_btn').attr('disabled', false);
                            $('#bill_save_btn').html(lg_save);

                            var obj = JSON.parse(res);

                            if (obj.status === 200)
                            {
                                toastr.success(obj.msg);
                                setTimeout(function () {
                                    window.location.href = base_url + 'mypatient-preview/' + obj.patient_id;
                                }, 3000);

                            } else
                            {
                                toastr.error(obj.msg);
                            }
                        }
                    });
                    return false;
                }
            });

            $("#update_billing").validate({
                rules: {
                    "name[]": "required",
                    "amount[]": "required"
                },
                messages: {
                    "name[]": lg_please_enter_na,
                    "amount[]": lg_please_enter_am
                },
                submitHandler: function (form) {
                    var signature_id = $('#signature_id').val();
                    if (signature_id == 0) { /* Signature validation */
                        toastr.error(lg_please_sign_to_);
                        return false;
                    }
                    $.ajax({
                        url: base_url + 'my_patients/update_billing',
                        data: $("#update_billing").serialize(),
                        type: "POST",
                        beforeSend: function () {
                            $('#billing_update_btn').attr('disabled', true);
                            $('#billing_update_btn').html('<div class="spinner-border text-light" role="status"></div>');
                        },
                        success: function (res) {
                            $('#billing_update_btn').attr('disabled', false);
                            $('#billing_update_btn').html(lg_update);

                            var obj = JSON.parse(res);

                            if (obj.status === 200)
                            {
                                toastr.success(obj.msg);
                                setTimeout(function () {
                                    window.location.href = base_url + 'mypatient-preview/' + obj.patient_id;
                                }, 3000);

                            } else
                            {
                                toastr.error(obj.msg);
                            }
                        }
                    });
                    return false;
                }
            });


        });
    }



}

if (modules == 'doctor' || modules == 'patient') {
    if (pages == 'change_password') {
        $(document).ready(function () {

            $("#change_password").validate({
                rules: {
                    currentpassword: {
                        required: true,
                        remote: {
                            url: base_url + "profile/check_currentpassword",
                            type: "post",
                            data: {
                                currentpassword: function () {
                                    return $("#currentpassword").val();
                                }
                            }
                        }
                    },

                    password: {
                        required: true,
                        minlength: 6
                    },
                    confirm_password: {
                        required: true,
                        equalTo: "#password"
                    },
                },
                messages: {
                    currentpassword: {
                        required: lg_please_enter_cu,
                        remote: lg_your_current_pa
                    },
                    password: {
                        required: lg_please_enter_pa,
                        minlength: lg_your_password_m
                    },
                    confirm_password: {
                        required: lg_please_enter_co,
                        equalTo: lg_your_password_d
                    },

                },
                submitHandler: function (form) {
                    $.ajax({
                        url: base_url + 'profile/password_update',
                        data: $("#change_password").serialize(),
                        type: "POST",
                        beforeSend: function () {
                            $('#change_password_btn').attr('disabled', true);
                            $('#change_password_btn').html('<div class="spinner-border text-light" role="status"></div>');
                        },
                        success: function (res) {
                            $('#change_password_btn').attr('disabled', false);
                            $('#change_password_btn').html(lg_change_password);
                            var obj = JSON.parse(res);

                            if (obj.status === 200)
                            {
                                $('#change_password')[0].reset();
                                toastr.success(obj.msg);
                            } else
                            {
                                toastr.error(obj.msg);
                            }
                        }
                    });
                    return false;
                }
            });

        });
    }
}

if (modules == 'doctor') {
    if (pages == 'appoinments') {
        setInterval(function () {
            my_appoinments(0);
        }, 1000);

        function my_appoinments(load_more) {

            if (load_more == 0) {
                $('#page_no_hidden').val(1);
            }

            var page = $('#page_no_hidden').val();


            //$('#search-error').html('');

            $.ajax({
                url: base_url + 'appoinments/doctor_appoinments_list',
                type: 'POST',
                data: {page: page},
                beforeSend: function () {
                    // $('#doctor-list').html('<div class="spinner-border text-success text-center" role="status"></div>');
                },
                success: function (response) {
                    //$('#doctor-list').html('');
                    if (response) {
                        var obj = $.parseJSON(response);


                        if (obj.current_page_no == 1) {
                            $("#appointment-list").html(obj.data);
                        } else {
                            $("#appointment-list").append(obj.data);
                        }


                        if (obj.count == 0) {
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                            return false;
                        }


                        if (obj.current_page_no == 1 && obj.count < 8) {
                            $('page_no_hidden').val(1);
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                            return false;
                        }



                        if (obj.total_page > obj.current_page_no && obj.total_page != 0) {
                            $('#load_more_btn').removeClass('d-none');
                            $('#no_more').addClass('d-none');
                        } else {
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                        }




                    }
                }

            });
        }

        $('#load_more_btn').click(function () {
            var page_no = $('#page_no_hidden').val();
            var current_page_no = 0;

            if (page_no == 1) {
                current_page_no = 2;
            } else {
                current_page_no = Number(page_no) + 1;
            }
            $('#page_no_hidden').val(current_page_no);
            my_appoinments(1);
        });



        function show_appoinments_modal(app_date, book_date, amount, type) {
            $('.app_date').html(app_date);
            $('.book_date').html(book_date);
            $('.amount').html(amount);
            $('.type').html(type);
            $('#appoinments_details').modal('show');

        }

        function conversation_status(id, status)
        {
            if (status == 1)
            {
                $('.app-modal-title').html('Accept');
                $('#app-modal-title').html('accept');
                $('#appoinments_status').val('1');
                $('#appoinments_id').val(id);
            }

            if (status == 0)
            {
                $('.app-modal-title').html('Cancel');
                $('#app-modal-title').html('cancel');
                $('#appoinments_status').val('0');
                $('#appoinments_id').val(id);
            }

            $('#appoinments_status_modal').modal('show');

        }
        function change_status()
        {
            var appoinments_id = $('#appoinments_id').val();
            var appoinments_status = $('#appoinments_status').val();
            $('#change_btn').attr('disabled', true);
            $('#change_btn').html('<div class="spinner-border text-light" role="status"></div>');
            $.post(base_url + 'appoinments/change_status', {appoinments_id: appoinments_id, appoinments_status, appoinments_status}, function (res) {

                my_appoinments(0);

                $('#change_btn').attr('disabled', false);
                $('#change_btn').html(lg_yes);
                $('#appoinments_status_modal').modal('hide');


            });
        }

    }

}
if (modules == 'patient') {
    if (pages == 'appoinments') {
        setInterval(function () {
            my_pappoinments(0);
        }, 1000);
        function my_pappoinments(load_more) {

            if (load_more == 0) {
                $('#page_no_hidden').val(1);
            }

            var page = $('#page_no_hidden').val();


            //$('#search-error').html('');

            $.ajax({
                url: base_url + 'appoinments/patient_appoinments_list',
                type: 'POST',
                data: {page: page},
                beforeSend: function () {
                    // $('#doctor-list').html('<div class="spinner-border text-success text-center" role="status"></div>');
                },
                success: function (response) {
                    //$('#doctor-list').html('');
                    if (response) {
                        var obj = $.parseJSON(response);

                        if (obj.current_page_no == 1) {
                            $("#appointment-list").html(obj.data);
                        } else {
                            $("#appointment-list").append(obj.data);
                        }


                        if (obj.count == 0) {
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                            return false;
                        }


                        if (obj.current_page_no == 1 && obj.count < 8) {
                            $('page_no_hidden').val(1);
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                            return false;
                        }



                        if (obj.total_page > obj.current_page_no && obj.total_page != 0) {
                            $('#load_more_btn').removeClass('d-none');
                            $('#no_more').addClass('d-none');
                        } else {
                            $('#load_more_btn').addClass('d-none');
                            $('#no_more').removeClass('d-none');
                        }

                    }
                }

            });
        }

        $('#load_more_btn').click(function () {
            var page_no = $('#page_no_hidden').val();
            var current_page_no = 0;

            if (page_no == 1) {
                current_page_no = 2;
            } else {
                current_page_no = Number(page_no) + 1;
            }
            $('#page_no_hidden').val(current_page_no);
            my_pappoinments(1);
        });

        function show_appoinments_modal(app_date, book_date, amount, type) {
            $('.app_date').html(app_date);
            $('.book_date').html(book_date);
            $('.amount').html(amount);
            $('.type').html(type);
            $('#appoinments_details').modal('show');

        }


    }
}

if (pages == 'invoice')
{

    $(document).ready(function () {
        //datatables
        var invoice_table;

        invoice_table = $('#invoice_table').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'invoice/invoice_list',
                "type": "POST",
                "data": function (data) {
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });


    });
}

if (pages == 'accounts')
{

    if (modules == 'doctor') {


        //datatables
        var accounts_table;

        accounts_table = $('#accounts_table').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'accounts/doctor_accounts_list',
                "type": "POST",
                "data": function (data) {
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });



        function account_table()
        {
            accounts_table.ajax.reload(null, false);
        }


        var patient_refund_request_table;

        patient_refund_request_table = $('#patient_refund_request').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'accounts/patient_refund_request',
                "type": "POST",
                "data": function (data) {
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });


        function patient_refund_request()
        {
            patient_refund_request_table.ajax.reload(null, false);
        }

        function send_request(id, status)
        {

            $.post(base_url + 'accounts/send_request', {id: id, status: status}, function (res) {

                account_table();
                patient_refund_request();
            });
        }

    }



    if (modules == 'patient') {



        //datatables
        var doctor_request_table;

        doctor_request_table = $('#doctor_request').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'accounts/doctor_request',
                "type": "POST",
                "data": function (data) {
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });



        function doctor_request()
        {
            doctor_request_table.ajax.reload(null, false);
        }

        //datatables
        var paccounts_table;

        paccounts_table = $('#accounts_table').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'accounts/patient_accounts_list',
                "type": "POST",
                "data": function (data) {
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });



        function paccount_table()
        {
            paccounts_table.ajax.reload(null, false);
        }


        function send_request(id, status)
        {

            $.post(base_url + 'accounts/send_request', {id: id, status: status}, function (res) {

                paccount_table();
                doctor_request();
            });
        }

    }


    function add_account_details()
    {
        $.ajax({
            url: base_url + "accounts/get_account_details",
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {

                $('[name="bank_name"]').val(data.bank_name);
                $('[name="branch_name"]').val(data.branch_name);
                $('[name="account_no"]').val(data.account_no);
                $('[name="account_name"]').val(data.account_name);


                // show bootstrap modal when complete loaded


            }


        });

        $('#account_modal').modal('show');
    }

    function payment_request(type)
    {
        $('#payment_type').val(type);
        $('#payment_request_modal').modal('show');
    }

    function amount()
    {
        var a = $('#request_amount').val();
        if (a != '')
        {
            $('#request_amount').val(parseInt(a));
        }

    }


    $(document).ready(function (e) {
        $("#accounts_form").on('submit', (function (e) {
            e.preventDefault();


            var bank_name = $('[name="bank_name"]').val();
            var branch_name = $('[name="branch_name"]').val();
            var account_no = $('[name="account_no"]').val();
            var account_name = $('[name="account_name"]').val();


            if (bank_name == '') {
                toastr.error('Please enter bank name');
                return false;
            }
            if (branch_name == '') {
                toastr.error('Please enter branch name');
                return false;
            }
            if (account_no == '') {
                toastr.error('Please enter account no');
                return false;
            }
            if (account_name == '') {
                toastr.error('Please enter account name');
                return false;
            }


            $.ajax({
                url: base_url + 'accounts/add_account_details',
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {

                    $('#acc_btn').html('<div class="spinner-border text-light" role="status"></div>');
                    $('#acc_btn').attr('disabled', true);

                },
                success: function (data) {

                    $('#acc_btn').html('Save');
                    $('#acc_btn').attr('disabled', false);

                    var obj = jQuery.parseJSON(data);
                    if (obj.result == 'true')
                    {

                        toastr.success(obj.status);

                        $('#account_modal').modal('hide');
                        $('#bank_name').html(bank_name);
                        $('#branch_name').html(branch_name);
                        $('#account_no').html(account_no);
                        $('#account_name').html(account_name);

                    } else
                    {
                        toastr.error(obj.status);
                    }
                }

            });
        }));



        $("#payment_request_form").on('submit', (function (e) {
            e.preventDefault();


            var request_amount = $('[name="request_amount"]').val();

            if (request_amount == '') {
                toastr.error('Please enter amount');
                return false;
            }

            $.ajax({
                url: base_url + 'accounts/payment_request',
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {

                    $('#request_btn').html('<div class="spinner-border text-light" role="status"></div>');
                    $('#request_btn').attr('disabled', true);

                },
                success: function (data) {

                    $('#request_btn').html('Request');
                    $('#request_btn').attr('disabled', false);

                    var obj = jQuery.parseJSON(data);
                    if (obj.result == 'true')
                    {

                        toastr.success(obj.status);

                        $('#payment_request_modal').modal('hide');
                        $('#payment_request_form')[0].reset();

                        window.location.reload();
                    } else
                    {
                        toastr.error(obj.status);
                    }
                }

            });
        }));


    });




}

if (modules == 'home')
{
    if (pages == 'doctors_searchmap')
    {




        google.maps.visualRefresh = true;
        var slider, infowindow = null;
        var bounds = new google.maps.LatLngBounds();
        var map, current = 0;

        var icons = {
            'default': 'assets/img/marker.png'
        };

        function show() {
            infowindow.close();
            if (!map.slide) {
                return;
            }
            var next, marker;
            if (locations.length == 0) {
                return
            } else if (locations.length == 1) {
                next = 0;
            }
            if (locations.length > 1) {
                do {
                    next = Math.floor(Math.random() * locations.length);
                } while (next == current)
            }
            current = next;
            marker = locations[next];
            setInfo(marker);
            infowindow.open(map, marker);
        }


        function initialize() {

            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                zoom: 14,
                //center: new google.maps.LatLng(53.470692, -2.220328),
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,

            };

            map = new google.maps.Map(document.getElementById('map'), mapOptions);
            map.slide = true;

            setMarkers(map, locations);
            infowindow = new google.maps.InfoWindow({
                content: "loading..."
            });

            google.maps.event.addListener(infowindow, 'closeclick', function () {
                infowindow.close();
            });
            slider = window.setTimeout(show, 3000);
        }

        function setInfo(marker) {
            var content =
                    '<div class="profile-widget" style="width: 100%; display: inline-block;">' +
                    '<div class="doc-img">' +
                    '<a href="' + marker.profile_link + '" tabindex="0" target="_blank">' +
                    '<img class="img-fluid" alt="' + marker.doc_name + '" src="' + marker.image + '">' +
                    '</a>' +
                    '</div>' +
                    '<div class="pro-content">' +
                    '<h3 class="title">' +
                    '<a href="' + marker.profile_link + '" tabindex="0">' + marker.doc_name + '</a>' +
                    '<i class="fas fa-check-circle verified"></i>' +
                    '</h3>' +
                    '<p class="speciality">' + marker.speciality + '</p>' +
                    '<div class="rating">' +
                    '<i class="fas fa-star"></i>' +
                    '<i class="fas fa-star"></i>' +
                    '<i class="fas fa-star"></i>' +
                    '<i class="fas fa-star"></i>' +
                    '<i class="fas fa-star"></i>' +
                    '<span class="d-inline-block average-rating"> (' + marker.total_review + ')</span>' +
                    '</div>' +
                    '<ul class="available-info">' +
                    '<li><i class="fas fa-map-marker-alt"></i> ' + marker.address + ' </li>' +
                    //'<li><i class="far fa-clock"></i> ' + marker.next_available + '</li>'+
                    '<li><i class="far fa-money-bill-alt"></i> ' + marker.amount + '</li>' +
                    '</ul>' +
                    '</div>' +
                    '</div>';
            infowindow.setContent(content);
        }



        function setMarkers(map, markers) {
            for (var i = 0; i < markers.length; i++) {
                var item = markers[i];
                var latlng = new google.maps.LatLng(item.lat, item.lng);
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    doc_name: item.doc_name,
                    address: item.address,
                    speciality: item.speciality,
                    // next_available: item.next_available,
                    amount: item.amount,
                    profile_link: item.profile_link,
                    total_review: item.total_review,
                    animation: google.maps.Animation.DROP,
                    icon: icons[item.icons],
                    image: item.image
                });
                bounds.extend(marker.position);
                markers[i] = marker;
                google.maps.event.addListener(marker, "click", function () {
                    setInfo(this);
                    infowindow.open(map, this);
                    window.clearTimeout(slider);
                });
            }
            map.fitBounds(bounds);
            google.maps.event.addListener(map, 'zoom_changed', function () {
                if (map.zoom > 16)
                    map.slide = false;
            });
        }

//google.maps.event.addDomListener(window, 'load', initialize);



    }
}

if (modules== 'qa' && ( pages == 'index' || pages =='questions_answers'))
{

    function show_answer(id){


        $("#question_id").val(id);
        $('#answer_modal').modal('show');


    }
    $("body").on('keydown','#qa-answers',function(event){
        if(event.shiftKey && event.keyCode == 13){
            add_answers();
            toastr.info("Posting Question");
        }
    });
    console.log("dafafas");

    function add_questions()
    {

        var specialization = $('#specilization').val();
        var questions = $('#questions').val();
        var questions_description = $('#questions_description').val();

        if (questions != '') {
            $.ajax({
                url: base_url + 'qa/add_questions',
                data: {specialization: specialization, questions: questions, questions_description: questions_description},
                type: "POST",
                beforeSend: function () {
                    $('#post_btn').attr('disabled', true);
                    $('#post_btn').html('<div class="spinner-border text-light" role="status"></div>');
                },
                success: function (res) {
                    $('#post_btn').attr('disabled', false);
                    $('#post_btn').html('Post');

                    var obj = JSON.parse(res);

                    if (obj.status === 200)
                    {
                        $('#questions').val('');
                        $('#questions_description').val('');
                        get_questions(0);
                    } else if (obj.status === 401)
                    {
                        toastr.error(obj.msg);
                    }
                }
            });
            return false;
        }
    }


    function add_answers()
    {
        var answers = $('#qa-answers').val();
        var question_id = $('#qa-answers').data('questionid');

        if (answers != '') {
            $.ajax({
                url: base_url + 'qa/add_answers',
                data: {question_id: question_id, answers: answers},
                type: "POST",
                beforeSend: function () {
                    $('#submit_answer').attr('disabled', true);
                    $('#submit_answer').html('<div class="spinner-border text-light" role="status"></div>');
                },
                success: function (res) {
                    

                    var obj = JSON.parse(res);

                    if (obj.status === 200)
                    {
                        toastr.success("Added successfully");
                        setTimeout(function(){ location.reload(); }, 2000);


                    } else if (obj.status === 401)
                    {
                        $('#submit_answer').attr('disabled', false);
                       $('#submit_answer').html('Submit');
                        toastr.error(obj.msg);
                    }
                }
            });
            return false;
        }
    }



    get_questions(0);
    function get_questions(load_more) {

        if (load_more == 0) {
            $('#page_no_hidden').val(1);
        }

        var page = $('#page_no_hidden').val();
        var specialization = $("#specilization").val();
        var keywords = $("#keywords").val();


        //$('#search-error').html('');

        $.ajax({
            url: base_url + 'qa/get_questions',
            type: 'POST',
            data: {
                page: page,
                specialization: specialization,
                keywords: keywords

            },
            beforeSend: function () {
                // $('#doctor-list').html('<div class="spinner-border text-success text-center" role="status"></div>');
            },
            success: function (response) {
                //$('#doctor-list').html('');
                if (response) {
                    var obj = $.parseJSON(response);
                    if (obj.questions_list) {
                        var html = '';


                        if (obj.current_page_no == 1) {
                            $('#questions-list').html(obj.questions_list);
                        } else {
                            $('#questions-list').append(obj.questions_list);
                        }

                    } else
                    {
                        var html = '<li class="list-group-item">' +
                                '<div class="user-que-name d-inline-block">' +
                                '<h5 class="table-avatar">No Questions Found<h5>' +
                                '</div>' +
                                '</li>';

                        $("#doctor-list").html(html);
                    }


                    // $(".widget-title").html(obj.count+' Matches for your search');
                    if (obj.count == 0) {
                        $('#load_more_btn').addClass('d-none');
                        $('#no_more').removeClass('d-none');
                        return false;
                    }


                    if (obj.current_page_no == 1 && obj.count < 5) {
                        $('page_no_hidden').val(1);
                        $('#load_more_btn').addClass('d-none');
                        $('#no_more').removeClass('d-none');
                        return false;
                    }



                    if (obj.total_page > obj.current_page_no && obj.total_page != 0) {
                        $('#load_more_btn').removeClass('d-none');
                        $('#no_more').addClass('d-none');
                    } else {
                        $('#load_more_btn').addClass('d-none');
                        $('#no_more').removeClass('d-none');
                    }

                }
            }

        });
    }



    $('#load_more_btn').click(function () {
        var page_no = $('#page_no_hidden').val();
        var current_page_no = 0;

        if (page_no == 1) {
            current_page_no = 2;
        } else {
            current_page_no = Number(page_no) + 1;
        }
        $('#page_no_hidden').val(current_page_no);
        get_questions(1);
    });


    function get_answers(question_id)
    {
        $.post(base_url + 'qa/get_answers', {question_id: question_id}, function (data) {
            var obj = JSON.parse(data);
            $('#answers_list' + question_id).html(obj.answers_list);

        });

    }


    function update_answers()
    {

        var answer_id = $('#qaanswer_id').val();
        var question_id = $('#qaquestion_id').val();
        var answers = $('#qaanswers').val();

        if (answers != '') {
            $.ajax({
                url: base_url + 'qa/update_answers',
                data: {answer_id: answer_id, answers: answers},
                type: "POST",
                beforeSend: function () {
                    $('#qa_update_btn').attr('disabled', true);
                    $('#qa_update_btn').html('<div class="spinner-border text-light" role="status"></div>');
                },
                success: function (res) {
                    $('#qa_update_btn').attr('disabled', false);
                    $('#qa_update_btn').html('Save');
                    $('#edit_ans_view_ans').modal('hide');

                    var obj = JSON.parse(res);

                    if (obj.status === 200)
                    {
                        get_answers(question_id);
                        $('#answer_list_btn' + question_id).click();
                    } else if (obj.status === 401)
                    {
                        toastr.error(obj.msg);
                    }
                }
            });
            return false;
        }
    }


    function delete_questions(id)
    {
        $('#delete_id').val(id);
        $('#delete_table').val('questions');
        $('#delete_title').text('questions');
        $('#delete_modal').modal('show');
    }

    function edit_answers(id, question_id)
    {
        $('#qaanswer_id').val(id);
        $('#qaquestion_id').val(question_id);
        $('#qaanswers').val($('#qaanswers_edit' + id).val());
        $('#edit_ans_view_ans').modal('show');
    }

    function delete_answers(id, question_id)
    {
        $('#delete_id').val(id);
        $('#delete_question_id').val(question_id);
        $('#delete_table').val('answers');
        $('#delete_title').text('answers');
        $('#delete_modal').modal('show');
    }



    function delete_qa_details()
    {
        var id = $('#delete_id').val();
        var question_id = $('#delete_question_id').val();
        var delete_table = $('#delete_table').val();
        $('#delete_btn').attr('disabled', true);
        $('#delete_btn').html('<div class="spinner-border text-light" role="status"></div>');
        $.post(base_url + 'qa/delete_details', {id: id, delete_table, delete_table}, function (res) {

            if (delete_table == 'questions')
                ;
            {
                get_questions(0);
            }

            if (delete_table == 'answers')
                ;
            {
                get_answers(question_id);
            }


            $('#delete_btn').attr('disabled', false);
            $('#delete_btn').html('Yes');
            $('#delete_modal').modal('hide');


        });
    }



}

if (pages == 'my_questions')
{

    $(document).ready(function () {

        $("#my_ques").dataTable();
    });

    function add_question()
    {
        $('[name="method"]').val('insert');
        $('#question_form')[0].reset(); // reset form on modals
        $('#question_modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add new question'); // Set Title to Bootstrap modal title


    }


    function delete_question(id)
    {
        if (confirm('Are you sure delete this question?'))
        {
            // ajax delete data to database
            $.ajax({
                url: base_url + "qa/delete_question/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                    //if success reload ajax table
                    //$('#family_form').modal('hide');

                    toastr.success('Question deleted successfully');

                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                },
                error: function () {
                    window.location.href = base_url;
                }
            });

        }
    }

    $(document).ready(function (e) {
        $("#question_form").on('submit', (function (e) {
            e.preventDefault();

            var name = $('#questions').val();

            if (name == '') {
                toastr.error('Please enter question');
                return false;
            }


            $.ajax({
                url: base_url + 'qa/add_questions',
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {

                    $('#btnfamilyssave').html('<div class="spinner-border text-light" role="status"></div>');
                    $('#btnfamilyssave').attr('disabled', true);

                },
                success: function (data) {

                    $('#btnfamilyssave').html('Submit');
                    $('#btnfamilyssave').attr('disabled', false);

                    var obj = jQuery.parseJSON(data);
                    if (obj.result == 'true')
                    {

                        toastr.success(obj.status);

                        $('#question_modal_form').modal('hide');
                        $('#question_form')[0].reset();
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                        //window.location.reload();
                    } else if (obj.result == 'false')
                    {
                        toastr.error(obj.status);

                    } else if (obj.result == 'exe')
                    {
                        toastr.error(obj.status);
                    } else
                    {
                        window.location.reload();
                    }
                },
                error: function () {
                    window.location.href = base_url + 'dashboard';
                }

            });
        }));
    });


    function view_answers(question_id)
    {
        $.post(base_url + 'qa/view_answers', {question_id: question_id}, function (data) {
            $('.view_answers').html(data);
            $('#view_modal-question').modal('show');
        });
    }
}

if(modules =='home' && pages=='contact'){



    $("#contact_form").validate({
                rules: {
                    name:"required",
                    email: "required",
                    mobile:"required",
                },
                messages: {
                    name:"Enter your name",
                    email: lg_please_enter_em,
                    mobile:"Enter your mobile number",
                   

                },
                submitHandler: function (form) {

                    $.ajax({
                        url: base_url + 'home/send_contact_mail',
                        data: $("#contact_form").serialize(),
                        type: "POST",
                        beforeSend: function () {
                            $('#contact_button').attr('disabled', true);
                            $('#contact_button').html('<div class="spinner-border text-light" role="status"></div>');
                        },
                        success: function (res) {
                            $('#contact_button').attr('disabled', false);
                            $('#contact_button').html(lg_signin);

                            var obj = JSON.parse(res);

                            if (obj.status === 200)
                            {
                                toastr.success("Mail sent successsfully !!! We will get back to you shortly");
                                $('#contact_form')[0].reset();
                            } else
                            {
                                toastr.error(obj.msg);
                            }
                        }
                    });
                    return false;
                }
            });
}




console.log(modules);
console.log(pages);