<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register',[\App\Http\Controllers\Api\AuthController::class, 'register']);
Route::post('login',[\App\Http\Controllers\Api\AuthController::class, 'login']);
Route::get('list-doctors', [\App\Http\Controllers\Api\PublicController::class, 'listAllDoctors']);
Route::post('create', [\App\Http\Controllers\Api\PublicController::class, 'create']);
Route::post('verify', [\App\Http\Controllers\Api\PublicController::class, 'verify'])->name('verify');
Route::get("/list-home-resources", [\App\Http\Controllers\HomeController::class, 'getAllHomeResources'])->name('getHome');

Route::get('getCountries', [\App\Http\Controllers\Api\PublicController::class, 'get_countries'])->name('list_all_countries');
Route::get('getStates', [\App\Http\Controllers\Api\PublicController::class, 'get_states'])->name('list_all_states');
Route::get('getCities', [\App\Http\Controllers\Api\PublicController::class, 'get_cities'])->name('list_all_cities');






/******************** Authenticated API ROUTES ******************************/

Route::middleware('auth:sanctum')->group(function(){
 Route::post('logout',[\App\Http\Controllers\Api\AuthController::class, 'logout'])->name('logout');
});

Route::group(['middleware'=>['auth:sanctum', 'is_doctor'], 'prefix'=>'doctor'], function(){
   Route::get('get-doctors', [\App\Http\Controllers\Api\DoctorsDetailController::class, 'index']);
   Route::post('update-avatar', [\App\Http\Controllers\Api\DoctorsDetailController::class, 'updateAvatar']);
   Route::post('update-doctor-details', [\App\Http\Controllers\Api\DoctorsDetailController::class, 'save']);
   Route::get('get-appointments', [\App\Http\Controllers\Api\AppointmentController::class, 'getAppointments']);
   Route::post('set-business-hours', [\App\Http\Controllers\Api\BusinessHourController::class, 'save']);
   Route::post('appointment-slots', [\App\Http\Controllers\Api\AppointmentController::class, 'createSlots']);

});

Route::group(['middleware'=>'auth:sanctum', 'prefix'=>'patient'], function(){
    Route::get('appointments', [\App\Http\Controllers\Api\AppointmentController::class, 'patientList']);
    Route::post('book-appointment', [\App\Http\Controllers\Api\AppointmentController::class, 'store']);
    Route::post('cancel-appointment', [\App\Http\Controllers\Api\AppointmentController::class, 'patientCancelAppointment']);
});
