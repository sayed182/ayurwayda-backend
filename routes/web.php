<?php

use App\Http\Controllers\QAController;
use App\Http\Controllers\ReplyController;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/chat', function(){
    return Inertia::render('Chat');
});
Route::get('/dev/login/{user}', function (User $user) {
	Auth::login($user);
	return redirect()->home();
});
Route::get('/dev/logout', function () {
	Auth::logout();
	return redirect()->home();
});

Route::get('/login', [\App\Http\Controllers\AuthenticationController::class, 'index'])->name('user.get.login');
Route::get('/register', [\App\Http\Controllers\AuthenticationController::class, 'index'])->name('user.get.register');
Route::get('/doctor-register', [\App\Http\Controllers\AuthenticationController::class, 'index'])->name('doctor.get.register');
Route::post('/login', [\App\Http\Controllers\AuthenticationController::class, 'login'])->name('user.post.login');
Route::post('/logout', [\App\Http\Controllers\AuthenticationController::class, 'logout'])->name('user.post.logout');
Route::post('/register', [\App\Http\Controllers\AuthenticationController::class, 'login'])->name('user.post.register');

Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('about', [\App\Http\Controllers\HomeController::class, 'about'])->name('about');
Route::get('blog', [\App\Http\Controllers\HomeController::class, 'blog'])->name('blog');
Route::get('/blog/{slug}', [\App\Http\Controllers\BlogController::class, 'index'])->name('blog.details');
Route::get('/search', [\App\Http\Controllers\SearchController::class, 'index'])->name('doctor.search');
//Route::get('/search/{param}', [\App\Http\Controllers\SearchController::class, 'search'])->where('param', '.+')->name('doctor.search.query');
Route::get('/contact', [\App\Http\Controllers\HomeController::class, 'contact'])->name('contact');

Route::prefix('qa')->group(function(){
    Route::get('/', [\App\Http\Controllers\QAController::class, 'index'])->name('qa');
    Route::get('/create', [\App\Http\Controllers\QAController::class, 'create'])->name('qa.create');
    Route::post('/', [\App\Http\Controllers\QAController::class, 'store'])->name('qa.store');
    Route::get('/{question}', [\App\Http\Controllers\QAController::class, 'show'])->name('qa.details');

	Route::post('/{question}/replies', [ReplyController::class, 'store'])->name('qa.replies.store')->middleware('auth');
	Route::put('/replies/{reply}', [ReplyController::class, 'update'])->name('qa.replies.update')->middleware('auth');
	Route::delete('/replies/{reply}', [ReplyController::class, 'destroy'])->name('qa.replies.destroy')->middleware('auth');
});



Route::prefix('doctor')->group(function (){
    Route::get('/profile/{user}', [\App\Http\Controllers\DoctorsController::class, 'showProfile'])->name('doctor.show.profile');
    Route::group(['middleware' => 'auth:web'], function () {
        Route::get('/', [\App\Http\Controllers\DoctorsController::class, 'index'])->name('doctor.dashboard');
        Route::get('/appointments', [\App\Http\Controllers\DoctorsController::class, 'appointment'])->name('doctor.appointment');
        Route::get('/profile-settings', [\App\Http\Controllers\DoctorsController::class, 'profileSettings'])->name('doctor.profile_settings');
        Route::post('/profile-settings', [\App\Http\Controllers\DoctorsController::class, 'storeProfileSettings'])->name('post.doctor.profile_settings');
        Route::get('chat-message/{userid}', [\App\Http\Controllers\BookAppointments::class, 'textChat'])->name('doctor.chat');
        Route::get('patients', [\App\Http\Controllers\DoctorsController::class, 'showPatients'])->name('doctor.show.patients');
        Route::group(['prefix' => 'qa'], function(){
            Route::get('/', [\App\Http\Controllers\DoctorsController::class, 'loadQA'])->name('doctor.load.qa');
        });
        Route::group(['prefix' => 'blog'], function(){
            Route::get('/', [\App\Http\Controllers\DoctorsController::class, 'blog'])->name('doctor.blog');
            Route::get('/{id}', [\App\Http\Controllers\BlogController::class, 'index'])->name('doctor.blog.details');
        });
    });
});

Route::group(['prefix' =>'patient', 'middleware' => ['web']], function (){
    Route::get('profile', [\App\Http\Controllers\PatientController::class, 'index'])->name('patient.profile.index');
    Route::get('settings', [\App\Http\Controllers\PatientController::class, 'settings'])->name('patient.profile.setting');
    Route::get('appointments', [\App\Http\Controllers\PatientController::class, 'appointments'])->name('patient.appointments.list');
    Route::get('calender', [\App\Http\Controllers\PatientController::class, 'calender'])->name('patient.calender.view');
    Route::get('invoice', [\App\Http\Controllers\PatientController::class, 'invoice'])->name('patient.invoice.list');
    Route::get('accounts', [\App\Http\Controllers\PatientController::class, 'accounts'])->name('patient.accounts.list');
    Route::get('change-password', [\App\Http\Controllers\PatientController::class, 'showPassword'])->name('patient.change.password.show');
    Route::post('change-password', [\App\Http\Controllers\PatientController::class, 'setPassword'])->name('patient.change.password.set');
    Route::group(['prefix' => 'qa'], function(){
       Route::get('/', [\App\Http\Controllers\PatientController::class, 'loadQA'])->name('patient.load.qa');
    });
});

Route::middleware('web')->group(function(){
    Route::get('/{doctor}/book-appointment', [\App\Http\Controllers\BookAppointments::class, 'index'])->name('patient.book-appointment');
    Route::post('/get-slots', [\App\Http\Controllers\BookAppointments::class, 'getSlots'])->name('get-slots');
//    Route::post('/book-appointment', [\App\Http\Controllers\BookAppointments::class, 'create'])->name('post.patient.book-appointment');
    Route::post('/checkout', [\App\Http\Controllers\BookAppointments::class, 'checkout'])->name('patient.checkout');
    Route::post('/success', [\App\Http\Controllers\BookAppointments::class, 'store'])->name('patient.store');
});


Route::get('chat-message/{userid}', [\App\Http\Controllers\BookAppointments::class, 'textChat']);

Route::get('{place}', [\App\Http\Controllers\SearchController::class, 'placeSearch']);
Route::get('{place}/{specialization}', [\App\Http\Controllers\SearchController::class, 'placeDoctorSearch']);










