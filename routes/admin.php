<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*****************ADMIN ROUTES*******************/



Route::get('/login', ['\App\Http\Controllers\Admin\Auth\AuthController', 'index'])->name('login');
Route::post('/login', ['\App\Http\Controllers\Admin\Auth\AuthController', 'login'])->name('login.post');
Route::get('/register', ['\App\Http\Controllers\Admin\Auth\AuthController', 'showRegisterForm'])->name('register');
Route::get('/forgot-password', function () {
    return view('admin.forgot-password');
})->name('forgot-password');
Route::post('/logout', [\App\Http\Controllers\Admin\Auth\AuthController::class, 'logout'])->name('admin.logout');



Route::middleware('auth:admins')->group( function() {

    Route::get('/', [\App\Http\Controllers\Admin\AdminController::class, 'index'])->name('dashboard');
    Route::get('/specialization', [\App\Http\Controllers\Admin\SpecializationController::class, 'index'])->name('specialization');
    Route::post('/specialization', [\App\Http\Controllers\Admin\SpecializationController::class, 'add'])->name('specialization.add');
    Route::post('/specialization/edit', [\App\Http\Controllers\Admin\SpecializationController::class, 'edit'])->name('specialization.edit');
    Route::post('/specialization/delete', [\App\Http\Controllers\Admin\SpecializationController::class, 'delete'])->name('specialization.delete');

    //---------- Blog Controller -------------//
    Route::get('/blogs', [\App\Http\Controllers\Admin\BlogController::class, 'index'])->name('blogs');
    Route::get('/blog/add', [\App\Http\Controllers\Admin\BlogController::class, 'add'])->name('blogs.add');
    Route::post('/blog/add', [\App\Http\Controllers\Admin\BlogController::class, 'save'])->name('blogs.save');
    Route::get('/blog/edit/{blogid}', [\App\Http\Controllers\Admin\BlogController::class, 'edit'])->name('blogs.edit');
    Route::post('/blog/edit', [\App\Http\Controllers\Admin\BlogController::class, 'update'])->name('blogs.update');
    Route::post('/blog/delete', [\App\Http\Controllers\Admin\BlogController::class, 'delete'])->name('blogs.delete');
    Route::get('/blog/categories', [\App\Http\Controllers\Admin\BlogController::class, 'categories'])->name('categories');
    Route::post('/blog/categories', [\App\Http\Controllers\Admin\BlogController::class, 'categories_add'])->name('categories.add');
    Route::post('/blog/categories/edit', [\App\Http\Controllers\Admin\BlogController::class, 'categories_edit'])->name('categories.edit');
    Route::post('/blog/categories/delete', [\App\Http\Controllers\Admin\BlogController::class, 'categories_delete'])->name('categories.delete');

    Route::get('/appointment-list', function () {
        return view('admin.appointment-list');
    })->name('appointment-list');
    Route::get('/doctor-list', function () {
        return view('admin.doctor-list');
    })->name('doctor-list');
    Route::get('/patient-list', function () {
        return view('admin.patient-list');
    })->name('patient-list');
    Route::get('/reviews', function () {
        return view('admin.reviews');
    })->name('reviews');
    Route::get('/transactions-list', function () {
        return view('admin.transactions-list');
    })->name('transactions-list');
    Route::get('/settings', function () {
        return view('admin.settings');
    })->name('settings');
    Route::get('/invoice-report', function () {
        return view('admin.invoice-report');
    })->name('invoice-report');
    Route::get('/profile', function () {
        return view('admin.profile');
    })->name('profile');
});

    /*
     * Authentication Routes Starts
     * */




Route::get('/lock-screen', function () {
    return view('admin.lock-screen');
})->name('lock-screen');
Route::get('/error-404', function () {
    return view('admin.error-404');
})->name('error-404');
Route::get('/error-500', function () {
    return view('admin.error-500');
})->name('error-500');
Route::get('/blank-page', function () {
    return view('admin.blank-page');
})->name('blank-page');
Route::get('/components', function () {
    return view('admin.components');
})->name('components');
Route::get('/form-basic-inputs', function () {
    return view('admin.form-basic-inputs');
})->name('form-basic');
Route::get('/form-input-groups', function () {
    return view('admin.form-input-groups');
})->name('form-inputs');
Route::get('/form-horizontal', function () {
    return view('admin.form-horizontal');
})->name('form-horizontal');
Route::get('/form-vertical', function () {
    return view('admin.form-vertical');
})->name('form-vertical');
Route::get('/form-mask', function () {
    return view('admin.form-mask');
})->name('form-mask');
Route::get('/form-validation', function () {
    return view('admin.form-validation');
})->name('form-validation');
Route::get('/tables-basic', function () {
    return view('admin.tables-basic');
})->name('tables-basic');
Route::get('/data-tables', function () {
    return view('admin.data-tables');
})->name('data-tables');
Route::get('/invoice', function () {
    return view('invoice');
})->name('invoice');
Route::get('/calendar', function () {
    return view('admin.calendar');
})->name('calendar');








