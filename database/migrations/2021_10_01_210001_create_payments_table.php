<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('appointment_id')->constrained();
            $table->string('invoice_no');
            $table->integer('per_hour_charge');
            $table->integer('total_amount');
            $table->string('currency_code')->default('INR');
            $table->string('price_is_usd')->nullable();
            $table->string('transaction_status')->default('pending');
            $table->string('payment_merchant');
            $table->double('tax_percentage');
            $table->double('transaction_charge')->default(0.0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
