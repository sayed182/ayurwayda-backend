<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_requests', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('patient_id');
            // $table->foreign('patient_id')->references('id')->on('users');
            $table->bigInteger('doctor_id');
            // $table->foreign('doctor_id')->references('id')->on('users');
            $table->string('payment_type');
            $table->integer('amount')->default(0);
            $table->string('currency_code')->default('INR');
            $table->text('description')->nullable();
            $table->enum('status',['requested', 'completed']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_requests');
    }
}
