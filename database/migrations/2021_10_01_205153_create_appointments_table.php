<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->string('payment_id');
            $table->bigInteger('patient_id');
            // $table->foreign('patient_id')->references('id')->on('users');
            $table->bigInteger('doctor_id');
            // $table->foreign('doctor_id')->references('id')->on('users');
            $table->enum('type', ['online', 'clinic']);
            $table->date('appointment_date');
            $table->time('start_time');
            $table->time('end_time');
            $table->string('payment_status')->nullable();
            $table->boolean('approved')->default(false);
            $table->enum('status', ['completed', 'new', 'expired', 'canceled'])->default('new');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
