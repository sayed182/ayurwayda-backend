<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserDetailsTableRemoveDoctorInformation extends Migration
{
    /**
     * TODO:
     * Remove Clinic Information from UserDetails Table.
     * Remove Price Information
     * Remove Currency Code information
     */
    public function up()
    {
        Schema::table('user_details', function (Blueprint $table) {
            $table->dropColumn('clinic_name');
            $table->dropColumn('clinic_address');
            $table->dropColumn('currency_code');
            $table->dropColumn('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
