<?php

namespace Database\Factories;

use App\Models\Question;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuestionImageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
		$question = Question::all()->random();
		
        return [
            'question_id' => $question->id,
			'image' => 'questions/' . $this->faker->image(public_path('storage/questions'), 1000, 1000, null, false),
        ];
    }
}
