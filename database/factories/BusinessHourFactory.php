<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\BusinessHour;

class BusinessHourFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = BusinessHour::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        return [
            'user_id' => $this->faker->randomNumber(),
            'day_name' => $this->faker->word,
            'from_time' => $this->faker->time(),
            'to_time' => $this->faker->time(),
        ];
    }
}
