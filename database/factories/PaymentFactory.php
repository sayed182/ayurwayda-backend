<?php

namespace Database\Factories;

use App\Models\Appointment;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Payment;

class PaymentFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Payment::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        $appointment_id = Appointment::all()->pluck('id')->random();
        return [
            'appointment_id' => 10,
            'invoice_no' => $this->faker->isbn10(),
            'per_hour_charge' => $this->faker->randomNumber(),
            'total_amount' => $this->faker->randomNumber(),
            'currency_code' => 'INR',
            'price_is_usd' => 0.013,
            'transaction_status' => $this->faker->randomElement(['failed', 'success']),
            'payment_merchant' => 'RazorPay',
            'tax_percentage' => $this->faker->randomFloat(),
            'transaction_charge' => $this->faker->randomFloat(),
        ];
    }
}
