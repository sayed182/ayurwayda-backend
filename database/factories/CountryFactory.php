<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Country;

class CountryFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Country::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        return [
            'country_code' => $this->faker->word,
            'country_name' => $this->faker->word,
            'country_currency' => $this->faker->word,
            'tel_code' => $this->faker->word,
        ];
    }
}
