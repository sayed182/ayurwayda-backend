<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuestionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
		$user = User::where('role', 'patient')->get()->random();

        return [
            'user_id' => $user->id,
			'body' => $this->faker->text() . '?'
        ];
    }
}
