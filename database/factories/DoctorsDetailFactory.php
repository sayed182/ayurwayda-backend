<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\DoctorsDetail;

class DoctorsDetailFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = DoctorsDetail::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        return [
            'user_id' => $this->faker->randomNumber(),
            'education' => $this->faker->word,
            'experience' => $this->faker->word,
            'awards' => $this->faker->word,
            'memberships' => $this->faker->word,
            'documents' => $this->faker->word,
        ];
    }
}
