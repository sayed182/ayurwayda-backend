<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\PaymentRequest;

class PaymentRequestFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = PaymentRequest::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        return [
            'patient_id' => $this->faker->randomNumber(),
            'doctor_id' => $this->faker->randomNumber(),
            'payment_type' => $this->faker->word,
            'amount' => $this->faker->randomNumber(),
            'currency_code' => $this->faker->word,
            'description' => $this->faker->text,
            'status' => $this->faker->randomElement(['requested', 'completed']),
        ];
    }
}
