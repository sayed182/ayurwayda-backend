<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Specialization;

class SpecializationFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Specialization::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        $list = ["Cardiology - Non Interventional",
    "Corneal Transplant",
    "General Surgery",
    "Infectious Diseases",
    "In-Vitro Fertilisation (IVF)",
"Laboratory Medicine",
    "Liver Transplant & Hepatic Surgery",
    "Medical Gastroenterology"];
        return [
            'specialization_name' => $this->faker->randomElement($list),
            'specialization_image' => $this->faker->imageUrl('124', '124'),
            'status' => true,
        ];
    }
}
