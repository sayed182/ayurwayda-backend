<?php

namespace Database\Factories;

use App\Models\DoctorTimeSlots;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class DoctorTimeSlotsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DoctorTimeSlots::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $userId = User::where('role', 'doctor')->get()->pluck('id')->random();
        return [
            'user_id' => $userId,
            'day_name' => $this->faker->dayOfWeek,
            'start_time' => $this->faker->time(),
            'duration_in_min' => 60,
            'type' => $this->faker->randomElement(['online', 'clinic']),
        ];
    }
}
