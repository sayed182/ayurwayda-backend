<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\City;
use App\Models\Country;
use App\Models\State;

class CityFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = City::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        $country = Country::all()->random();
		$state = State::all()->random();
        return [
            'country_id' => $country->id,
            'state_id' => $state->id,
            'city_name' => $this->faker->word,
        ];
    }
}
