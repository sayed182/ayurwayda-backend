<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\BankAccount;

class BankAccountFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = BankAccount::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        return [
            'user_id' => $this->faker->randomNumber(),
            'bank_name' => $this->faker->word,
            'branch _name' => $this->faker->word,
            'account_number' => $this->faker->word,
            'account_name' => $this->faker->word,
            'status' => $this->faker->word,
        ];
    }
}
