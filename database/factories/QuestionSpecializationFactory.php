<?php

namespace Database\Factories;

use App\Models\Question;
use App\Models\Specialization;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuestionSpecializationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
		$specialization = Specialization::all()->random();
		$question = Question::all()->random();

        return [
			'question_id' => $question->id,
            'specialization_id' => $specialization->id,
        ];
    }
}
