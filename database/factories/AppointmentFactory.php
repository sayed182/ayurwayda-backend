<?php

namespace Database\Factories;

use App\Models\DoctorTimeSlots;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Appointment;

class AppointmentFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = Appointment::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        $doctor_id = User::where('role', 'doctor')->get()->pluck('id')->random();
        $patient_id = User::where('role', 'patient')->get()->pluck('id')->random();
        $slot = DoctorTimeSlots::all()->pluck('id')->random();
        return [
            'payment_id' => $this->faker->uuid(),
            'patient_id' => $patient_id,
            'doctor_id' => $doctor_id,
            'type' => $this->faker->randomElement(['online', 'clinic']),
            'doctor_time_slot' => $slot,
            'appointment_date' => $this->faker->date(),
            'approved' => $this->faker->boolean,
            'status' => $this->faker->randomElement(['completed', 'new', 'expired']),
        ];
    }
}
