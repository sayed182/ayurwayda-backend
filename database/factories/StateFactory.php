<?php

namespace Database\Factories;

use App\Models\Country;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\State;

class StateFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = State::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        $country = Country::all()->random();
        return [
            'country_id' => $country->id,
            'state_name' => $this->faker->word,
            'state_code' => $this->faker->word,
        ];
    }
}
