<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\Country;
use App\Models\Specialization;
use App\Models\State;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\UserDetails;

class UserDetailsFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = UserDetails::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        $user_id =  User::all()->pluck('id')->random();
		$city = City::all()->random();
		$specialization = Specialization::all()->random();
        return [
            'user_id' => $user_id,
            'gender' => $this->faker->randomElement(['male', 'female', 'other']),
            'dob' => $this->faker->date(),
            'blood_group' => $this->faker->word,
            'biography' => $this->faker->text,
            // 'clinic_name' => $this->faker->word,
            // 'clinic_address' => $this->faker->word,
            'primary_address' => $this->faker->word,
            'secondary_address' => $this->faker->word,
            'country_id' => $city->country_id,
            'state_id' => $city->state_id,
            'city_id' => $city->id,
            'post_code' => $this->faker->word,
            // 'currency_code' => $this->faker->word,
            // 'price' => $this->faker->randomNumber(),
            'specialization_id' => $specialization->id,
        ];
    }
}
