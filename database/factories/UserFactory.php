<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\User;

class UserFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var  string
    */
    protected $model = User::class;

    /**
    * Define the model's default state.
    *
    * @return  array
    */
    public function definition(): array
    {
        return [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->safeEmail,
            'username' => $this->faker->userName,
            'mobileno' => $this->faker->word,
            'profile_image' => $this->faker->word,
            'password' => bcrypt("password"),
            'login_otp' => $this->faker->randomNumber(5),
            'role' => $this->faker->randomElement(['doctor', 'patient']),
            'status' => $this->faker->randomElement(['active', 'inactive', 'deleted']),
			'slug' => $this->faker->slug(),
            'remember_token' => Str::random(10),
            'account_verified_at' => $this->faker->dateTime(),
            'deleted_at' => $this->faker->dateTime(),
        ];
    }
}
