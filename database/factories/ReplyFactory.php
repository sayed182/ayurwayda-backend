<?php

namespace Database\Factories;

use App\Models\Question;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReplyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
		$question = Question::all()->random();
		$qn_specializations = $question->specializations->pluck('id');
		$type = $this->faker->randomElement(['answer', 'question']);
		if ($type === 'question') {
			$user = User::where('role', 'patient')->where('status', 'active')->inRandomOrder()->first();
		} else {
			$user = User::where('role', 'doctor')
						->where('status', 'active')
						->whereHas('specialization', function ($query) use ($qn_specializations) {
							// This will filter out those users who don't have any specializations common with
							// the question's specializations.
							return $query->whereIn('id', $qn_specializations);
						})->inRandomOrder()->first();
		}

        return [
            'question_id' => $question->id,
			'user_id' => $user->id,
			'type' => $type,
			'body' => implode(PHP_EOL.PHP_EOL, $this->faker->paragraphs(rand(1, 3)))
        ];
    }
}
