<?php

namespace Database\Seeders;

use App\Models\Appointment;
use App\Models\DoctorTimeSlots;
use App\Models\Specialization;
use App\Models\User;
use Database\Factories\SpecializationFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        \App\Models\User::factory(20)->create();

//		$specializations = [
//			"Cardiology - Non Interventional",
//			"Corneal Transplant",
//			"General Surgery",
//			"Infectious Diseases",
//			"In-Vitro Fertilisation (IVF)",
//			"Laboratory Medicine",
//			"Liver Transplant & Hepatic Surgery",
//			"Medical Gastroenterology"
//		];
//
//		foreach($specializations as $specialization) {
//			\App\Models\Specialization::factory()->create([
//				'specialization_name' => $specialization
//			]);
//		}
//
//		$specializations = Specialization::all()->pluck('id');
//
//		// Create specializations for each doctor
//		User::where('role', 'doctor')->get()->each(function (User $doctor) use ($specializations) {
//			$cur_specializations = $specializations->random(rand(1, $specializations->count()));
//
//			$doctor->specialization()->attach($cur_specializations);
//		});

//	   \App\Models\Country::factory(10)->create();
//	   \App\Models\State::factory(10)->create();
//       \App\Models\City::factory(10)->create();
//        \App\Models\UserDetails::factory(5)->create();
//         \App\Models\Services::factory(10)->create();
        //  \App\Models\Payment::factory(3)->create();
//        DoctorTimeSlots::factory(5)->create();
//        Appointment::factory(10)->create();


		$this->call([
			ReviewsSeeder::class,
			QuestionSeeder::class
		]);
    }
}
