<?php

namespace Database\Seeders;

use App\Models\Question;
use App\Models\QuestionImage;
use App\Models\Reply;
use App\Models\Specialization;
use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Question::factory(20)->create()->each(function (Question $question) {
			QuestionImage::factory()->create([
				'question_id' => $question->id
			]);

			$specializations = Specialization::where('status', true)->get()->random(2);

			$question->specializations()->attach($specializations);
		});

		Reply::factory(50)->create();
    }
}
