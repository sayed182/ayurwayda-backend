<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		User::factory(5)->create([
			'role' => 'doctor',
			'status' => 'active'
		]);
		User::factory(5)->create([
			'role' => 'patient',
			'status' => 'active'
		]);

    }
}
