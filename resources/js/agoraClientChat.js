(function ($) {

    $(document).ready(async function () {
        var rtmAppID = '117f1fb8d9754863a54fad7910c2f864';
        const token = $('input[name=agoraToken]').val()
        const peer = $('input[name=agoraPeer]').val();
        const user = $('input[name=agoraUser]').val();
        const channel = $('input[name=channelName]').val();
        const client = AgoraRTM.createInstance(rtmAppID);
        // connectClient(client, token, channel, user);
        await client.login({
            uid: user,
            token: token
        })
        // persistMessages(channel, user);

        await receiveMessage(client, channel, peer);

        $("#messageBox").on('keyup', async function (event) {
            if (event.key == 'Enter') {
                await sendMessage(client, peer, channel, user);
            }
        });
        $("#sendMessage").on('click', async function (event) {
            await sendMessage(client, peer, channel, user);
        });

        $("#startChatTime").html(getCurrentTime());

        $('#send-image').on('click', async function (e) {
            // e.preventDefault()
            uploadToAws(client, peer);
            //
            // const src = $('#sendImg').attr('src')
            // imageToBlob(src, (blob) => {
            //     uploadImage(client, blob, params.peerId)
            // })

        });


    });
})(jQuery);

function getCurrentTime() {
    let date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

// const connectClient = (client, token, channel, user) => async
// {
//     $.ajax({
//         url: 'http://localhost:8000/agora/chat-token',
//         method: 'post',
//         data: {_token: $('meta[name="csrf-token"]').attr('content'), channel: channel, user: user},
//         success: async function (response) {
//             if (response) {
//                 let options = {
//                     uid: response.user_id,
//                     token: response.token
//                 }
//                 console.log(options);
//                 try {
//                     await client.login(options)
//                 } catch (e) {
//                     console.log(e.toString());
//                 }
//             }
//         },
//         error: function (err) {
//             console.log(err)
//         }
//     });
// }

const sendMessage = async (client, peer, channel, user) => {
    let message = $("#messageBox").val().toString();
    console.log(message);
    // $("#chatWindow").append(outgoingMessageTemplate(message));
    if (message.length > 0) {
        $("#messageBox").val('');
        await client.sendMessageToPeer(
            {text: message, mime: 'text'},
            peer,
        ).then(sendResult => {
            if (sendResult.hasPeerReceived) {
                $("#chatWindow").append(outgoingMessageTemplate(message, getCurrentTime()));
                setLocalStorage(channel, user, message);

            } else {
                $("#chatWindow").append(outgoingMessageTemplate(message, getCurrentTime()));
                console.log(sendResult);
                // clonedOutgoing.html('<p>'+message+'</p>').end().appendTo('#chatWindow');
            }
        });
        $(".ms-scrollable").animate({scrollTop: 100000}, "fast");
    }

}

const receiveMessage = async (client, channel, peer) => {
    client.on('MessageFromPeer', async function (message, peerId) {
        console.log(message);
        if (message.messageType === 'IMAGE') {
            // const blob = await client.downloadMedia(message.mediaId)
            // console.log(blob);
            // blobToImage(blob, (image) => {
            //     const view = $('<div/>', {
            //         text: [' peer: ', peerId].join('')
            //     })
            $("#chatWindow").append(incomingImageTemplate(`<img src= '${message.mediaId}'/>`));
            // })
        } else {
            $("#chatWindow").append(incomingMessageTemplate(message.text, getCurrentTime()));
        }


        setLocalStorage(channel, peer, message.text);
    })
}

//----------------- Messaging Templates ---------------
function outgoingMessageTemplate(message, time) {
    console.log("Called Template");
    var template = `
                <li class="media sent">
                    <div class="media-body">
                        <div class="msg-box">
                            <div>
                                <p>${message}</p>
                                <ul class="chat-msg-info">
                                    <li>
                                        <div class="chat-time">
                                            <span>${time}</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
            `;
    return template;
}

function incomingMessageTemplate(message, time) {
    var template = `
                <li class="media received">
                    <div class="media-body">
                        <div class="msg-box">
                            <div>
                                <p>${message}</p>
                                <ul class="chat-msg-info">
                                    <li>
                                        <div class="chat-time">
                                            <span>${time}</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </li>
            `;
    return template;
}

function incomingImageTemplate(img, time) {
    var template = `
                <div class="ms-chat-bubble ms-chat-message ms-chat-outgoing incoming-template media clearfix">
                <div class="media-body">
                    <div class="ms-chat-text">
                            ${img}
                    </div>
                    <p class="ms-chat-time">${time}</p>
                </div>
            </div>
            `;
    return template;
}

const setLocalStorage = (caseid, user_id, message) => {
    var string = {
        "caseid": caseid,
        "sender": user_id,
        "message": message,
    };
    var messages = localStorage.getItem('agora_message');
    if (messages == null) {
        var data = JSON.stringify([string]);
        localStorage.setItem('agora_message', data);
    } else {
        var messages = JSON.parse(messages);
        messages.push(string)
        var data = JSON.stringify(messages);
        localStorage.setItem('agora_message', data);
    }
}

const getFromStorage = (caseid, user_id) => {
    var messages = localStorage.getItem('agora_message');
    var messages = JSON.parse(messages);

    return messages;
}

const persistMessages = (case_id, user_id) => {
    var messages = localStorage.getItem('agora_message');
    if (messages == null) {
        return;
    }
    var messages = JSON.parse(messages);
    messages.forEach((element) => {
        if (element.caseid == case_id) {
            if (element.sender == user_id) {
                $("#chatWindow").append(outgoingMessageTemplate(element.message))
            } else {
                console.log(element);
                $("#chatWindow").append(incomingMessageTemplate(element.message));
            }
        }
    });
    $(".ms-scrollable").animate({scrollTop: 100000}, "fast");
}


function imageToBlob(src, cb) {
    imageToCanvas(src, function (canvas) {
        cb(dataURLToBlob(canvasToDataURL(canvas)))
    })
}

function canvasToDataURL(canvas, format, quality) {
    return canvas.toDataURL(format || 'image/jpeg', quality || 1.0)
}

function dataURLToBlob(dataurl) {
    var arr = dataurl.split(',')
    var mime = arr[0].match(/:(.*?);/)[1]
    var bstr = atob(arr[1])
    var n = bstr.length
    var u8arr = new Uint8Array(n)
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n)
    }
    return new Blob([u8arr], {type: mime})
}

function imageToCanvas(src, cb) {
    var canvas = document.createElement('CANVAS')
    var ctx = canvas.getContext('2d')
    var img = new Image()
    img.src = src
    img.onload = function () {
        canvas.width = img.width
        canvas.height = img.height
        ctx.drawImage(img, 0, 0)
        cb(canvas)
    }
}

function fileOrBlobToDataURL(obj, cb) {
    var a = new FileReader()
    a.readAsDataURL(obj)
    a.onload = function (e) {
        cb(e.target.result)
    }
}


function blobToImage(blob, cb) {
    fileOrBlobToDataURL(blob, function (dataurl) {
        var img = new Image()
        img.src = dataurl
        cb(img)
    })
}

async function uploadImage(client, blob, peerId) {
    const mediaMessage = await client.createMessage({
        messageType: 'IMAGE',
        fileName: 'agora.jpg',
        description: 'send image',
        mediaId: 'https://elasticbeanstalk-us-east-1-290452992164.s3.amazonaws.com/public/jL4Ou3ELy3vceNrOr7VVrWjaJljnsPge5xc5FRAR.png',
        // width: 100,
        // height: 200,
        thumbnailWidth: 50,
        thumbnailHeight: 200,
    });
    console.log(mediaMessage);
    return client.sendMessageToPeer(mediaMessage, peerId)
}

function uploadToAws(client, peerId) {
    var image = $('#file-input')[0].files[0];
    var form = new FormData();
    form.append('_token', $('meta[name="csrf_token"]').attr('content'));
    form.append('file', image);
    $.ajax({
        url: 'http://localhost:8000/api/v1/upload-image',
        data: form,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: async function (response) {
            console.log(response);
            if (response.data !== undefined) {
                var path = response.data.path;
                const mediaMessage = await client.createMessage({
                    messageType: 'IMAGE',
                    fileName: 'agora.jpg',
                    description: 'send image',
                    mediaId: 'https://elasticbeanstalk-us-east-1-290452992164.s3.amazonaws.com/public/jL4Ou3ELy3vceNrOr7VVrWjaJljnsPge5xc5FRAR.png',
                    // width: 100,
                    // height: 200,
                    thumbnailWidth: 50,
                    thumbnailHeight: 200,
                });
                console.log(mediaMessage);
                return client.sendMessageToPeer(mediaMessage, peerId)
            }
        }
    });
}

