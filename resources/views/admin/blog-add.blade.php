@extends('layout.mainlayout_admin')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
								<h3 class="page-title">Blank Page</h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item"><a href="index">Dashboard</a></li>
									<li class="breadcrumb-item active">Blank Page</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /Page Header -->

					<div class="row">
						<div class="col-sm-12">
                            <form action="{{route('blogs.save')}}" method="post" id="submit_blog">
                                @csrf
                            <div class="card">

                                <div class="card-body">

                                        <div class="form-group row">
                                            <label class="col-form-label col-md-12">Blog Title</label>
                                            <div class="col-md-12">
                                                <input type="text" class="form-control" name="title">
                                            </div>
                                        </div>
                                        <div class="form-group row">

                                            <div class="col-md-12">
                                                <textarea id="blog-body" class="form-control" rows="20" name="blog-body"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-md-12">Categories</label>
                                            <div class="col-md-12">
                                                <select class="select categories-select" >
                                                    <option>Select</option>
                                                    @foreach($categories as $cat)
                                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-md-12" id="categories-result">
                                                <input type="hidden" name="category_id[]">
                                            </div>
                                        </div>


                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-success" type="submit" form="submit_blog">Submit</button>
                                </div>

                            </div>
                            </form>
						</div>
					</div>

				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->
@endsection

{{--@extends('layout.partials.footer_admin-scripts')--}}
@section('scripts')
    <script src="https://cdn.tiny.cloud/1/gjmekmk09iqn41o45kuao41aqilqic4lw2zlz4llupmu86di/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            toolbar_mode: 'floating',
        });

        $('.categories-select').on('change', function (e){
            var cat = $(this).find(":selected").text();
            var id = $(this).find(":selected").val();
            $("#categories-result").append(`
                <input type="hidden" name="category_id[]" value="${id}">
                <span class="badge badge-pill badge-dark"> ${cat} <span class="delete-category"><i class="fa fa-remove"></i></span></span>
            `);
            e.stopImmediatePropagation();
        })

        $('body').on('click', '.delete-category', function (e){
            e.stopImmediatePropagation();
            $(this).parent().remove();
        });
    </script>
@endsection
