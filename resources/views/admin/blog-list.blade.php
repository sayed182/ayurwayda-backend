@extends('layout.mainlayout_admin')
@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
								<h3 class="page-title">Blogs</h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item"><a href="index">Dashboard</a></li>
									<li class="breadcrumb-item active">Blogs</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /Page Header -->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">

                                <div class="card-body">

                                    <div class="table-responsive">
                                        <table class="datatable table table-stripped">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Categories</th>
                                                <th>By User</th>
                                                <th>Status</th>
                                                <th>Published Date</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($blogs as $blog)
                                            <tr>
                                                <td>{{ $blog->title }}</td>
                                                <td>
                                                    @foreach($blog->categories() as $category)
                                                    {{ $category->name }},
                                                    @endforeach
                                                </td>
                                                <td>61</td>
                                                <td>
                                                    <div class="status-toggle">
                                                        <input type="checkbox" id="status_1" class="check" {{ $blog->is_published?'checked':''}}>
                                                        <label for="status_1" class="checktoggle">checkbox</label>
                                                    </div>
                                                </td>
                                                <td>{{ date_create($blog->created_at)->format('d-M-y') }}</td>
                                                <td>
                                                    <div class="actions">
                                                        <a class="btn btn-sm bg-success-light" data-id="{{$blog->id}}" href="{{route('blogs.edit', ["blogid"=> $blog->id])}}" >
                                                            <i class="fe fe-pencil"></i> Edit
                                                        </a>
                                                        <a  data-toggle="modal" href="#delete_modal" data-id="{{$blog->id}}"  class="btn btn-sm bg-danger-light">
                                                            <i class="fe fe-trash"></i> Delete
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

				</div>
			</div>
			<!-- /Page Wrapper -->

        </div>
		<!-- /Main Wrapper -->

<!-- Delete Modal -->
<div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <!--	<div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>-->
            <div class="modal-body">
                <div class="form-content p-2">
                    <h4 class="modal-title">Delete</h4>
                    <p class="mb-4">Are you sure want to delete?</p>
                    <form action="{{Route('blogs.delete')}}" method="post" id="delete_form" class="d-inline-block">
                        @csrf
                        <input type="hidden" name="id" id="delete_id"/>
                        <button type="submit" class="btn btn-primary" name="delete">Save </button>
                    </form>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Modal -->
@endsection

@section('scripts')
    <script>
        $(document).on('click', 'a[href="#edit_categories_details"]', function(){
            var id= $(this).data('id');
            var name= $(this).data('name');
            $('#edit_categories_details_form input[name="id"]').val(id);
            $('#edit_categories_details_form input[name="name"]').val(name);
        });
        $(document).on('click', 'a[href="#delete_modal"]', function(){
            var id= $(this).data('id');
            console.log(id);
            $('#delete_id').val(id);
        });
    </script>
@endsection
