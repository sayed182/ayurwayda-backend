<!-- Sidebar -->
<div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
					<div id="sidebar-menu" class="sidebar-menu">
						<ul>
							<li class="menu-title">
								<span>Main</span>
							</li>
							<li class="{{ Route::is(route('dashboard')) ? 'active' : '' }}">
								<a href="{{route('dashboard')}}"><i class="fe fe-home"></i> <span>Dashboard</span></a>
							</li>
							<li class="{{ Request::is('admin/appointment-list') ? 'active' : '' }}">
								<a href="/admin/appointment-list"><i class="fe fe-layout"></i> <span>Appointments</span></a>
							</li>
							<li  class="{{ Route::is(route('specialization')) ? 'active' : '' }}">
								<a href="{{route('specialization')}}"><i class="fe fe-users"></i> <span>Specialities</span></a>
							</li>
							<li  class="{{ Request::is('admin/doctor-list') ? 'active' : '' }}">
								<a href="/admin/doctor-list"><i class="fe fe-user-plus"></i> <span>Doctors</span></a>
							</li>
							<li  class="{{ Request::is('admin/patient-list') ? 'active' : '' }}">
								<a href="/admin/patient-list"><i class="fe fe-user"></i> <span>Patients</span></a>
							</li>
							<li  class="{{ Request::is('admin/reviews') ? 'active' : '' }}">
								<a href="/admin/reviews"><i class="fe fe-star-o"></i> <span>Reviews</span></a>
							</li>
							<li  class="{{ Request::is('admin/transactions-list') ? 'active' : '' }}">
								<a href="/admin/transactions-list"><i class="fe fe-activity"></i> <span>Transactions</span></a>
							</li>
							<li   class="{{ Request::is('admin/settings') ? 'active' : '' }}">
								<a href="/admin/settings"><i class="fe fe-vector"></i> <span>Settings</span></a>
							</li>
							<li class="submenu">
								<a href="#"><i class="fe fe-document"></i> <span> Reports</span> <span class="menu-arrow"></span></a>
								<ul style="display: none;">
									<li><a class="{{ Request::is('admin/invoice-report') ? 'active' : '' }}" href="{{ url('admin/invoice-report') }}">Invoice Reports</a></li>
								</ul>
							</li>
							<li class="menu-title">
								<span>Pages</span>
							</li>
							<li  class="{{ Request::is('admin/profile') ? 'active' : '' }}">
								<a href="/admin/profile"><i class="fe fe-user-plus"></i> <span>Profile</span></a>
							</li>
                            <li class="menu-title">
                                <span>Blogs</span>
                            </li>
                            <li  class="{{ Route::is(route('blogs')) ? 'active' : '' }}">
                                <a href="{{ route('blogs') }}"><i class="fe fe-user-plus"></i> <span>All Blogs</span></a>
                            </li>
                            <li  class="{{ Route::is(route('blogs.add')) ? 'active' : '' }}">
                                <a href="{{ route('blogs.add') }}"><i class="fe fe-user-plus"></i> <span>Add Blog</span></a>
                            </li>
                            <li  class="{{ Route::is(route('categories')) ? 'active' : '' }}">
                                <a href="{{ route('categories') }}"><i class="fe fe-user-plus"></i> <span>Categories</span></a>
                            </li>
						</ul>
					</div>
                </div>
            </div>
			<!-- /Sidebar -->
