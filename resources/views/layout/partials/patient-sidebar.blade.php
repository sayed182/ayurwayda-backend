<?php
$user = \Auth::user();
$details = $user->userDetails()->first();
?>

<div class="profile-sidebar">
    <div class="widget-profile pro-widget-content">
        <div class="profile-info-widget">
            <a href="#" class="booking-doc-img">
                <img src="{{ get_profile_image($user) }}" class="avatar-view-img" alt="User Image">
            </a>
            <div class="profile-det-info">
                <h3>{{ user_fullname($user) }}</h3>
                <div class="patient-details">
                    <h5><i class="fas fa-birthday-cake"></i> 10 Apr 1987, 35 Years</h5>
                    <h5 class="mb-0"><i class="fas fa-map-marker-alt"></i>Warangal, India</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-widget">
        <nav class="dashboard-menu">
            <ul>
                <li class="{{  is_active_menu('patient.profile.index') }}">
                    <a href="{{ route('patient.profile.index') }}">
                        <i class="fas fa-columns"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="{{  is_active_menu('patient.appointments.list') }}">
                    <a href="{{ route('patient.appointments.list') }}">
                        <i class="fas fa-calendar-check"></i>
                        <span>Appointments</span>
                    </a>
                </li>
                <li class="{{  is_active_menu('patient.calender.view') }}">
                    <a href="{{ route('patient.calender.view') }}">
                        <i class="fas fa-calendar-check"></i>
                        <span>Calendar</span>
                    </a>
                </li>
                <li class="{{  is_active_menu('patient.invoice.list') }}">
                    <a href="{{ route('patient.invoice.list') }}">
                        <i class="fas fa-file-invoice"></i>
                        <span>Invoice</span>
                    </a>
                </li>
                <li class="{{  is_active_menu('patient.profile.messages') }}">
                    <a href="http://localhost/ayurwayda/messages">
                        <i class="fas fa-comments"></i>
                        <span>Messages</span>
                        <small class="unread-msg unread_msg_count">0</small>
                    </a>
                </li>
                <li class="{{  is_active_menu('patient.accounts.list') }}">
                    <a href="{{ route('patient.accounts.list') }}">
                        <i class="fas fa-file-invoice"></i>
                        <span>Accounts</span>
                    </a>
                </li>
                <li class="{{  is_active_menu('patient.profile.setting') }}">
                    <a href="{{ route('patient.profile.setting') }}">
                        <i class="fas fa-user-cog"></i>
                        <span>Profile Settings</span>
                    </a>
                </li>
                <li class="{{  is_active_menu('patient.load.qa') }}">
                    <a href="{{ route('patient.load.qa') }}">
                        <i class="fas fa-file-invoice"></i>
                        <span>Q&amp;A - My Questions</span>
                    </a>
                </li>

                <li class="{{  is_active_menu('patient.change.password.show') }}">
                    <a href="{{ route('patient.change.password.show') }}">
                        <i class="fas fa-lock"></i>
                        <span>Change Password</span>
                    </a>
                </li>
                <li class="{{  is_active_menu('patient.profile.index') }}">
                    <a href="#">
                        <i class="fas fa-sign-out-alt"></i>
                        <span>Logout</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>

</div>
<div class="resize-sensor"
     style="position: absolute; inset: 0px; overflow: hidden; z-index: -1; visibility: hidden;">
    <div class="resize-sensor-expand"
         style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
        <div
            style="position: absolute; left: 0px; top: 0px; transition: all 0s ease 0s; width: 295px; height: 1204px;"></div>
    </div>
    <div class="resize-sensor-shrink"
         style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
        <div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%"></div>
    </div>
</div>
