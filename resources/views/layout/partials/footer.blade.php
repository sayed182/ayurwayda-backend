<!-- Footer -->
<footer class="footer">

    <!-- Footer Top -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-about">
                        <div style="width: 213px;height: 71px;">
                            <img src="{{ asset("assets/img/logo-negetive.svg") }}" alt="logo" class="w-75">

                        </div>
                        <div class="footer-about-content footer-widget footer-menu">
                            <h4 class="footer-title mt-2 mb-3">Reach Us</h4>
                            <ul class="list-unstyled ">
                                <li>
                                    <a href="{{ route('about') }}">About</a>
                                </li>
                                <li>
                                    <a href="{{ route('blog') }}">Blog</a>
                                </li>

                                <li>
                                    <a href="{{ route('contact') }}">Contact Us</a>
                                </li>
                            </ul>

                            <div class="social-icon">
                                <ul>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Footer Widget -->

                </div>

                <div class="col-lg-3 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-menu mt-2">
                        <h2 class="footer-title">For Patients</h2>
                        <ul>
                            <li><a href="{{ route('doctor.search') }}">Search for Doctors</a></li>
                            <li><a href="{{ route('user.get.login') }}">Login</a></li>
                            <li><a href="{{ route('user.get.register') }}">Register</a></li>
                            <li><a href="{{ route('patient.profile.index') }}">Patient Dashboard</a></li>
                            <li><a href="{{ route('doctor.search') }}">Booking Appointments</a></li>
                        </ul>
                    </div>
                    <!-- /Footer Widget -->

                </div>

                <div class="col-lg-3 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-menu mt-2">
                        <h2 class="footer-title">For Doctors</h2>
                        <ul>
                            <li><a href="#">Appointments</a></li>
                            <li><a href="#">Chat</a></li>
                            <li><a href="{{ route('user.get.login') }}">Login</a></li>
                            <li><a href="{{ route('doctor.get.register') }}">Register</a></li>
                            <li><a href="{{ route('doctor.dashboard') }}">Doctor Dashboard</a></li>
                        </ul>
                    </div>
                    <!-- /Footer Widget -->

                </div>

                <div class="col-lg-3 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-contact">
                        <h4 class="footer-title text-right mb-3">Follow Us On</h4>
                        <ul class="mt-3 list-unstyled list-inline-item social-icons float-right">
                            <li class="list-inline-item">
                                <a href="#"><i class="fab fa-linkedin-in"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="fab fa-youtube"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </li>
                        </ul>
                        <!--  <div class="footer-contact-info">
                           <div class="footer-address">
                             <span><i class="fas fa-map-marker-alt"></i></span>
                             <p>   </p>
                           </div>
                           <p>
                             <i class="fas fa-phone-alt"></i>
                             98495 69365                    </p>
                           <p class="mb-0">
                             <i class="fas fa-envelope"></i>
                             ayurwaydic@gmail.com                    </p>
                         </div> -->
                    </div>
                    <!-- /Footer Widget -->

                </div>

            </div>
        </div>
    </div>
    <!-- /Footer Top -->

    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="container">

            <!-- Copyright -->
            <div class="copyright">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="copyright-text">
                            <!-- <p class="mb-0">&copy; 2022 Ayurwayda . </p> -->
                            <p class="mb-0">© {{ date('Y') }} Ayurwayda. All rights reserved.</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">

                        <!-- Copyright Menu -->
                        <div class="copyright-menu">
                            <ul class="policy-menu">
                                <li><a href="http://localhost/ayurwayda/terms-conditions">Terms and Conditions</a></li>
                                <li><a href="http://localhost/ayurwayda/privacy-policy">Privacy Policy</a></li>
                            </ul>
                        </div>
                        <!-- /Copyright Menu -->

                    </div>
                </div>
            </div>
            <!-- /Copyright -->

        </div>
    </div>
    <!-- /Footer Bottom -->

</footer>
<!-- /Footer -->
