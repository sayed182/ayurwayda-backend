<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        @if(!Route::is(['appointment-list','specialities','doctor-list','patient-list','reviews','transactions-list','settings','invoice-report','profile','login','register','forgot-password','lock-screen','error-404','error-500','blank-page','components','form-basic','form-inputs','form-horizontal','form-vertical','form-mask','form-validation','tables-basic','data-tables','invoice','calendar']))
        <title>Ayurwayda - Dashboard</title>
        @endif
        @if(Route::is(['appointment-list']))
        <title>Ayurwayda - Appointment List Page</title>
        @endif
        @if(Route::is(['specialities']))
        <title>Ayurwayda - Specialities Page</title>
		@endif
        @if(Route::is(['doctor-list']))
        <title>Ayurwayda - Doctor List Page</title>
        @endif
        @if(Route::is(['patient-list']))
        <title>Ayurwayda - Patient List Page</title>
        @endif
        @if(Route::is(['reviews']))
        <title>Ayurwayda - Reviews Page</title>
        @endif
        @if(Route::is(['transactions-list']))
        <title>Ayurwayda - Transactions List Page</title>
        @endif
        @if(Route::is(['settings']))
        <title>Ayurwayda - Settings Page</title>
        @endif
        @if(Route::is(['invoice-report']))
        <title>Ayurwayda - Invoice Report Page</title>
        @endif
        @if(Route::is(['profile']))
        <title>Ayurwayda - Profile</title>
        @endif
        @if(Route::is(['login']))
        <title>Ayurwayda - Login</title>
		@endif
        @if(Route::is(['register']))
        <title>Ayurwayda - Register</title>
        @endif
        @if(Route::is(['forgot-password']))
        <title>Ayurwayda - Forgot Password</title>
        @endif
        @if(Route::is(['lock-screen']))
        <title>Ayurwayda - Lock Screen</title>
        @endif
        @if(Route::is(['error-404']))
        <title>Ayurwayda - Error 404</title>
		@endif
        @if(Route::is(['error-500']))
        <title>Ayurwayda - Error 500</title>
		@endif
        @if(Route::is(['blank-page']))
        <title>Ayurwayda - Blank Page</title>
        @endif
        @if(Route::is(['components']))
        <title>Ayurwayda - Components</title>
        @endif
        @if(Route::is(['form-basic']))
        <title>Ayurwayda - Basic Inputs</title>
        @endif
        @if(Route::is(['form-inputs']))
        <title>Ayurwayda - Form Input Groups</title>
        @endif
        @if(Route::is(['form-horizontal']))
        <title>Ayurwayda - Horizontal Form</title>
        @endif
        @if(Route::is(['form-vertical']))
        <title>Ayurwayda - Vertical Form</title>
        @endif
        @if(Route::is(['form-mask']))
        <title>Ayurwayda - Form Mask</title>
        @endif
        @if(Route::is(['form-validation']))
        <title>Ayurwayda - Form Validation</title>
        @endif
        @if(Route::is(['tables-basic']))
        <title>Ayurwayda - Tables Basic</title>
        @endif
        @if(Route::is(['data-tables']))
        <title>Ayurwayda - Data Tables</title>
        @endif
        @if(Route::is(['invoice']))
        <title>Ayurwayda - Invoice</title>
        @endif
        @if(Route::is(['calendar']))
        <title>Ayurwayda - Calendar</title>
        @endif
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets_admin/img/favicon.png')}}">

		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{asset('assets_admin/css/bootstrap.min.css')}}">

		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="{{asset('assets_admin/css/font-awesome.min.css')}}">

		<!-- Feathericon CSS -->
        <link rel="stylesheet" href="{{asset('assets_admin/css/feathericon.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/morris/morris.css')}}">
        <!-- Select2 CSS -->
		<link rel="stylesheet" href="{{asset('assets_admin/css/select2.min.css')}}">
        	<!-- Datetimepicker CSS -->
		<link rel="stylesheet" href="{{asset('assets_admin/css/bootstrap-datetimepicker.min.css')}}">

		<!-- Full Calander CSS -->
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/fullcalendar/fullcalendar.min.css')}}">
        <!-- Datatables CSS -->
		<link rel="stylesheet" href="{{asset('assets_admin/plugins/datatables/datatables.min.css')}}">

		<!-- <link rel="stylesheet" href="assets/plugins/morris/morris.css"> -->

		<!-- Main CSS -->
        <link rel="stylesheet" href="{{asset('assets_admin/css/admin.css')}}">
