<div class="container p-0">
    <!-- Header -->
    <header class="header">

        <nav class="navbar navbar-expand-lg header-nav">
            <div class="navbar-header">
                <a id="mobile_btn" href="javascript:void(0);">
              <span class="bar-icon">
                <span></span>
                <span></span>
                <span></span>
              </span>
                </a>
                <a href="{{ route('home') }}" class="navbar-brand logo">
                    <img src="{{ asset('assets/img/logo.svg') }}" class="img-fluid d-block w-100"
                         alt="Logo">


                    <!-- <img src="assets/img/logo.png" class="img-fluid" alt="Logo"> -->
                </a>
            </div>
            <div class="main-menu-wrapper">
                <div class="menu-header">
                    <a href="{{ route('home') }}" class="menu-logo">
                        <img src="{{ asset('assets/img/logo.svg') }}"
                             class="img-fluid d-block w-100" alt="Logo" >

                    </a>
                    <a id="menu_close" class="menu-close" href="javascript:void(0);">
                        <i class="fas fa-times"></i>
                    </a>
                </div>
                <ul class="main-nav">
                    <li>
                        <a href="{{ route('home') }}">Home</a>
                    </li>
                    <li>
                        <a href="{{ route('about') }}">About</a>
                    </li>
                    <li>
                        <a href="{{ route('blog') }}">Blog</a>
                    </li>


                    <li>
                        <a href="{{ route('doctor.search') }}">Doctors</a>
                    </li>

                    <li>
                        <a href="{{ route('qa') }}">Q&A</a>
                    </li>

                    <li>
                        <a href="{{ route('contact') }}">Contact</a>
                    </li>


{{--                    <li class="login-link">--}}
{{--                        <a href="{{ route('user.get.login') }}">Login / Signup</a>--}}
{{--                    </li>--}}
                </ul>
                @guest
                <div class="nav-item">
                    <a class="nav-link header-login" href="{{ route('user.get.login') }}">Login / Signup </a>
                </div>
                @endguest
                @auth('web')
                <li class="nav-item dropdown has-arrow" style="min-width: auto;">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                        <span class="user-img"><img class="rounded-circle" src={{asset("assets_admin/img/profiles/avatar-01.jpg")}} width="31" alt="Ryan Taylor"></span>
                    </a>
                    <div class="dropdown-menu">
                        <div class="user-header">
                            <div class="avatar avatar-sm">
                                <img src={{asset("assets_admin/img/profiles/avatar-01.jpg")}} alt="UserImage" class="avatar-img rounded-circle">
                            </div>
                            <div class="user-text">
                                <h6>{{ user_fullname(auth()->user()) }}</h6>
                                <p class="text-muted mb-0">{{  isset(auth()->user()->role)?auth()->user()->role:'Admin'}}</p>
                            </div>
                        </div>
                        @if(auth()->user()->role == 'patient')
                        <a class="dropdown-item" href="{{ route('patient.profile.index') }}">My Profile</a>
                        <a class="dropdown-item" href="settings">Settings</a>
                        @else
                        <a class="dropdown-item" href="{{ route('doctor.dashboard') }}">My Profile</a>
                        <a class="dropdown-item" href="settings">Settings</a>
                        @endif
                        <form action="{{route('user.post.logout')}}" method="post" id="admin_logout_form">
                            @csrf
                        </form>
                        <a class="dropdown-item" href="javascript:void(0)" onclick="logout()">Logout</a>
                        <script>
                            function logout(){
                                var t=document.getElementById('admin_logout_form');
                                t.submit();
                            }
                        </script>
                    </div>
                </li>
                @endauth

            </div>
        </nav>

    </header>
</div>
<!-- /Header -->
