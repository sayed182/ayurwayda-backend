<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layout.partials.head')
  </head>
  @if(Route::is(['map-grid']))
  <body class="map-page">
  @endif
@include('layout.partials.header')

  <div class="content">
      <div class="container">
          @include('components.alert')
          <div class="row">
              <div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">
                  @if($profile == 'doctor')
                    @include('layout.partials.doctors-sidebar')
                  @endif
                  @if($profile == 'patient')
                      @include('layout.partials.patient-sidebar')
                  @endif
              </div>
              <div class="col-md-7 col-lg-8 col-xl-9">
                  @yield('content')
              </div>
          </div>

      </div>

  </div>
  <!-- /Page Content -->
  </div>

@if(!Route::is(['chat-doctor','map-grid','map-list','chat','voice-call','video-call']))
@include('layout.partials.footer')
@endif
@include('layout.partials.footer-scripts')
@toastr_render
  </body>
</html>

