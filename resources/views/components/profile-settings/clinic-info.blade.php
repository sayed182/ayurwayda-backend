<div class="card">
    <div class="card-body"><h4 class="card-title">Clinic Info</h4>
        <div class="row form-row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Clinic Name</label>
                    <input type="text" class="form-control" name="clinic_name" value="{{ $doctor_profile->doctorDetails->clinic_name??'' }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Clinic Address</label>
                    <input type="text" class="form-control" name="clinic_address" value="{{ $doctor_profile->doctorDetails->clinic_address??'' }}">
                </div>
            </div>
            <div class="col-md-12"><input type="file" name="clinic_images[]" multiple=""></div>
        </div>
    </div>
</div>

