<div class="card">
    <div class="card-body"><h4 class="card-title">Awards</h4>
        <div class="awards-info">
            <div class="row form-row awards-cont">
                <div class="col-12 col-md-5">
                    <div class="form-group"><label>Awards</label><input type="text"
                                                                        class="form-control"
                                                                        name="award[0][name]">
                    </div>
                </div>
                <div class="col-12 col-md-5">
                    <div class="form-group"><label>Year</label><input type="text"
                                                                      class="form-control"
                                                                      name="award[0][year]">
                    </div>
                </div>
            </div>
        </div>
        <div class="add-more"><a class="add-award"><i class="fa fa-plus-circle"></i> Add
                More</a></div>
    </div>
</div>
