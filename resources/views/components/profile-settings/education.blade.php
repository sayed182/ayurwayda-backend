<div class="card">
    <div class="card-body"><h4 class="card-title">Education</h4>
        <div class="education-info">
            <div class="row form-row education-cont">
                <div class="col-12 col-md-10 col-lg-11">
                    <div class="row form-row">
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label>Degree</label>
                                <input type="text" class="form-control" name="education[0][degree]"/>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label>College/Institute</label>
                                <input type="text" class="form-control" name="education[0][institute]"/>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label>Year of Completion</label>
                                <input type="text" class="form-control" name="education[0][year]"/>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label>Certificate</label>
                                <input type="file" class="form-control" name="education[0][certificate]"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="add-more">
            <button type="button" class="btn add-education" href="#">
                <i class="fa fa-plus-circle"></i> Add More
            </button>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        var index = 1;

        $(".add-more").on('click', function (e) {
                var template = `
            <div class="col-12 col-md-10 col-lg-11" >
                <div class="row form-row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Degree</label>
                            <input type="text" class="form-control" name="education[${index}][degree]" />
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>College/Institute</label>
                            <input type="text" class="form-control" name="education[${index}][institute]"/>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Year of Completion</label>
                            <input type="text" class="form-control" name="education[${index}][year]"/>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Certificate</label>
                            <input type="file" class="form-control" name="education[${index}][certificate]" />
                        </div>
                    </div>
                </div>
            </div>
            `;
            $('.education-cont').append(template);
            console.log(template);
            index++;
        });
    </script>

@endpush
