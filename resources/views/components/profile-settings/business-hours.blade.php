@php
$days = [ 'monday' => ['to' => null, 'from' => null],
            'tuesday' => ['to' => null, 'from' => null],
            'wednesday' => ['to' => null, 'from' => null],
            'thursday' => ['to' => null, 'from' => null],
            'friday' => ['to' => null, 'from' => null],
            'saturday' => ['to' => null, 'from' => null],
            'sunday' => ['to' => null, 'from' => null] ];
$business_hours = $profile->businessHours;
foreach ($business_hours as $data){
    $days[$data->day_name] = [ 'to' => $data->to_time, 'from' => $data->from_time];
}
@endphp
<div class="card">
    <div class="card-body"><h4 class="card-title">Business Hours</h4>
        <div class="registrations_ct_01">
            <div class="table-responsice">
                <table class="table availability-table">
                    <tbody>
                    @foreach($days as $day_name => $day)
                    <tr>
                        <input type="hidden" name="availability[1][user_id]" value="{{ auth()->user()->id }}">
                        <td width="50%">
                            <input type="checkbox" class="days_check" name="availability[{{ $loop->index }}][status]" {{ $day['from']!=null?'checked':'' }} />
                            <input type="hidden" name="availability[{{ $loop->index }}][day_name]" value="{{ $day_name }}"/>
                            {{ \Str::ucfirst($day_name) }}
                        </td>
                        <td width="25%">
                            <span>From : </span>
                            <input type="time" name="availability[{{ $loop->index }}][from_time]" value="{{ $day['from'] }}"/>
                        </td>
                        <td width="25%">
                            <span>To : </span>
                            <input type="time" name="availability[{{ $loop->index }}][to_time]" value="{{ $day['to'] }}">
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
