<div class="card">
    <div class="card-body"><h4 class="card-title">Registrations</h4>
        <div class="registrations-info">
            <div class="row form-row reg-cont">
                <div class="col-12 col-md-5">
                    <div class="form-group"><label>Registrations</label><input type="text"
                                                                               class="form-control"
                                                                               name="registration[0][name]">
                    </div>
                </div>
                <div class="col-12 col-md-5">
                    <div class="form-group"><label>Year</label><input type="text"
                                                                      class="form-control"
                                                                      name="registration[0][year]">
                    </div>
                </div>
            </div>
        </div>
        <div class="add-more"><a class="add-reg"><i class="fa fa-plus-circle"></i> Add More</a>
        </div>
    </div>
</div>
