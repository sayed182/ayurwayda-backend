<div class="card">
    <div class="card-body"><h4 class="card-title">Memberships</h4>
        <div class="membership-info">
            <div class="row form-row membership-cont">
                <div class="col-12 col-md-10 col-lg-5">
                    <div class="form-group"><label>Memberships</label><input type="text"
                                                                             class="form-control"
                                                                             name="membership[0]">
                    </div>
                </div>
            </div>
        </div>
        <div class="add-more"><a class="add-membership"><i class="fa fa-plus-circle"></i> Add
                More</a></div>
    </div>
</div>
