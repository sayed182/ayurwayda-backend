<div class="card">
    <div class="card-body"><h4 class="card-title">Experience</h4>
        <div class="experience-info">
            <div class="row form-row experience-cont">
                <div class="col-12 col-md-10 col-lg-11">
                    <div class="row form-row">
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group"><label>Hospital Name</label><input
                                    type="text" class="form-control"
                                    name="experience[0][hospital_name]"></div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group"><label>From</label><input type="text"
                                                                              class="form-control"
                                                                              name="experience[0][from]">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group"><label>To</label><input type="text"
                                                                            class="form-control"
                                                                            name="experience[0][to]">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group"><label>Designation</label><input type="text"
                                                                                     class="form-control"
                                                                                     name="experience[0][designation]">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="add-more"><a class="add-experience"><i class="fa fa-plus-circle"></i> Add
                More</a></div>
    </div>
</div>
