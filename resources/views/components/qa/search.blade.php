<form class="form-inline" method="GET" action="{{ route('qa') }}">
	<div class="form-group col-md-3 pl-0 mb-2">
		<label for="specialization" class="sr-only" id="specialization">Choose Speciality </label>
		<select class="form-control w-100" id="specilization" name="specialization">
			<option value="">Choose Speciality</option>
			@foreach ($specializations as $specialization)
				<option value="{{ $specialization->id }}" 
					@if ($specialization->id == request('specialization')) selected @endif>
					{{ $specialization->specialization_name }}
				</option>
			@endforeach
		</select>
	</div>
	<div class="form-group col-md-7 mb-2">
		<input type="text" class="form-control w-100" name="search" id="keywords"
			   placeholder="Search articles, answer, health tools, etc..." 
			   @if (request('search')) value="{{ request('search') }}" @endif>
	</div>

	<button class="btn btn-primary form-control col-md-2 w-100 mb-2 ">Search</button>
</form>