<div class="patient-query card border-0 reply" data-reply-id="{{ $reply->id }}">
    @if ($reply->type === 'question')
		<div class="card-header text-white align-self-start px-3 py-2" style="background: #1D72CD;">
			Patient's Query
		</div>
	@elseif ($firstAnswer != null && $reply->id === $firstAnswer->id)
        <div class="card-header text-white align-self-start px-3 py-2" style="background: #43B483;">
            Answered By Dr. {{ $reply->replier->full_name }}
        </div>
    @endif
    <div class="card-body border h-100">
        <div class="patient-image">
            <img src="{{ asset('assets/img/profiles/avatar-01.jpg') }}" alt="" class="user-avatar">
        </div>
        <div class="patient-query-body h-100 d-flex flex-column justify-content-between">
            <p class="reply-body">
                {!! nl2br($reply->body) !!}
            </p>
            @if ($reply->type === 'answer')
                <div class="mt-3 d-flex" style="gap: 0.75rem;">
                    <span class="fw-bold">Answered by </span>
                    <a href="{{ route('doctor.show.profile', $reply->replier->slug) }}" style="color: #3dac7b;">
                        Dr. {{ $reply->replier->full_name }}
                    </a>
					<div class="ml-auto">
						{{ $reply->created_at->diffForHumans() }}	
					</div>
                </div>
            @endif
        </div>
    </div>
	@if ($reply->type === 'answer' || $reply->user_id === auth()->id())
		<div class="card-footer border border-top-0 rounded-bottom d-flex align-items-center">
			@if ($reply->type === 'answer')
				<div>
					<a href="" class="mr-4">
						<i class="far fa-thumbs-up"></i>
						Useful
					</a>
					<a href="" class="">
							<i class="far fa-thumbs-down"></i>
							Not Useful
						</a>
				</div>
			@endif
			@if ($reply->user_id === auth()->id())
				<div class="ml-auto">
					<span class="mr-4 edit-btn" style="cursor: pointer;">
						Edit Reply
					</span>
					<a href="" class="text-danger">
						Delete Reply
					</a>
				</div>
			@endif
		</div>
	@endif
</div>
