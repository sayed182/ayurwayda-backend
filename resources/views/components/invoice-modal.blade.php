<!-- Appointment Details Modal -->
<a href="#" class="btn btn-sm bg-info-light" data-toggle="modal"
   data-target="#appt_details">
    <i class="far fa-eye"></i> View
</a>
<div class="modal fade custom-modal" id="appt_details">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Appointment Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="info-details">
                    <li>
                        <div class="details-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="title">#APT{{ str_pad($appointment->id, 5, '0', STR_PAD_LEFT) }}</span>
                                    <span class="text">{{ get_formatted_date($appointment->created_at).' '.get_formatted_time($appointment->created_at) }} </span>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-right">
                                        <button type="button" class="btn bg-success-light btn-sm" id="topup_status">
                                            {{ ucwords($appointment->status) }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <span class="title">Status:</span>
                        <span class="text">{{ucwords($appointment->status)}}</span>
                    </li>
                    <li>
                        <span class="title">Confirm Date:</span>
                        <span class="text">{{ get_formatted_date($appointment->updated_at).' '.get_formatted_time($appointment->updated_at) }}</span>
                    </li>
                    <li>
                        <span class="title">Paid Amount</span>
                        <span class="text">{{ get_money_sign() }}450</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- /Appointment Details Modal -->
@push('scripts')
    <script>
        $('#appt_details').on('shown.bs.modal', function (event) {
            if(event.relatedTarget.dataset.appointment != undefined){
                var appointment = JSON.parse(event.relatedTarget.dataset.appointment);
                console.log(appointment);
            }

        })
    </script>
@endpush
