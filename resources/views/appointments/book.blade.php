@push('styles')
    <style>
        .date-block{
            cursor: pointer;
        }
        .date-block.active{
            background-color: #4cac7c;
        }
        .date-block.active h5,.date-block.active h6{
            color: #fff8f8 !important;
        }
    </style>
@endpush
@extends('layout.mainlayout_withoutsidebar')
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-body">
                        <div class="booking-doc-info">
                            <a href="{{ route('doctor.show.profile', ['user' => $doctor->slug]) }}"
                               class="booking-doc-img">
                                <img src="{{ get_profile_image($doctor) }}"
                                     alt="User Image">
                            </a>
                            <div class="booking-info">
                                <h4>
                                    <a href="{{ route('doctor.show.profile', ['user' => $doctor->slug]) }}">{{ user_fullname($doctor) }}</a>
                                </h4>
                                <div class="rating">
                                    <i class="fas fa-star filled"></i><i class="fas fa-star filled"></i><i
                                        class="fas fa-star filled"></i><i class="fas fa-star filled"></i><i
                                        class="fas fa-star filled"></i> <span
                                        class="d-inline-block average-rating">(1)</span>
                                </div>
                                <p class="text-muted mb-0"><i
                                        class="fas fa-map-marker-alt"></i> {{ user_city($userDetails) }}
                                    , {{ user_state($userDetails) }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Schedule Widget -->
                <div class="card booking-schedule schedule-widget bookings-schedule">

                    <!-- Schedule Header -->
                    <div class="schedule-header">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{ route('patient.checkout') }}" method="post" id="checkout-form">
                                    @csrf
                                    <input type="hidden" name="doctor_id" id="doctor_id" value="{{ $doctor->id }}">
                                    <input type="hidden" name="doctor_username" id="doctor_username"
                                           value="{{ $doctor->slug }}">
                                    <input id="selected_count" type="hidden">
                                    <input type="hidden" name="date_selected" />
                                    <input type="hidden" name="time_selected" />
                                    <input type="hidden" name="price_type" id="price_type" value="Custom Price">
                                    <input type="hidden" name="hourly_rate" id="hourly_rate" value="5.2">

                                    <div class="booking-option px-4 pt-3">
                                        <label class="payment-radio credit-card-option">
                                            <input type="radio" name="type" value="online" id="online" checked>
                                            <span class="checkmark"></span>
                                            Online </label>
                                        <label class="payment-radio credit-card-option">
                                            <input type="radio" name="type" value="clinic" id="clinic" disabled>
                                            <span class="checkmark"></span>
                                            Clinic </label>
                                        <label class="payment-radio credit-card-option">
                                            <input type="radio" name="type" value="both" id="both" disabled>
                                            <span class="checkmark"></span>
                                            Both </label>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <div class="track-dates-container">
                                    <h3 class="text-center">{{ now()->format('d F, Y') }}</h3>
                                    <div class="d-flex" id="scheduling-dates">
                                        <div class="date-block" id="scheduling-dates-prev">
                                            <span class="fas fa-chevron-left"></span>
                                        </div>
                                        <div id="scheduling-dates-wrapper">
                                            <div class="schedule-dates-track" style="left: calc(0px);">
                                                @for($i=0; $i <13; $i++)
                                                    @php
                                                        $date = Carbon\Carbon::now()->addDay($i);
                                                    @endphp
                                                    @foreach($slots as $time)
                                                        @if(strcasecmp($date->dayName, $time->day_name) == 0)
                                                            <div class="date-block schedule-dates" data-date="{{ $date->toDateString() }}">
                                                                <h5>{{ $date->day }}</h5>
                                                                <h6>{{ $date->dayName  }}</h6>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @endfor
                                            </div>
                                        </div>
                                        <div class="date-block" id="scheduling-dates-next">
                                            <span class="fas fa-chevron-right"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- Schedule Content -->
                    <div class="schedule-cont">
                        <div class="row">
                            <div class="col-md-12">

                                <!-- Time Slot -->
                                <div class="time-slot">
                                    <ul class="available-slots-list">
{{--                                        @foreach($slots as $slot)--}}
{{--                                            <li class="time-slot badge badge-light m-1 ">--}}
{{--                                                <button class="btn" onclick="selectCurrentSlot(event)" data-slot-id="{{ $slot->id }}">--}}
{{--                                                    {{ generate_slot($slot) }}--}}
{{--                                                </button>--}}
{{--                                            </li>--}}
{{--                                        @endforeach--}}

                                    </ul>

                                </div>
                                <!-- /Time Slot -->

                            </div>
                        </div>
                    </div>
                    <!-- /Schedule Content -->

                    <input type="hidden" name="pre_date" id="pre_date" class="form-control" value="2022-04-10">
                    <input type="hidden" name="next_date" id="next_date" class="form-control" value="2022-04-12">

                </div>
                <!-- /Schedule Widget -->


                <!-- Submit Section -->
                <div class="submit-section proceed-btn text-right bookingconfirmation">
                    You have booked <strong class="pr-2"><span id="slot-selection-count">0</span> Slot</strong> <button
                        form="checkout-form"
                        class="btn btn-primary submit-btn">Proceed
                        to Pay</button>
                </div>
                <!-- /Submit Section -->

            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var slot = 0;
        const date_track = $(".schedule-dates-track");
        const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        const date_next = $("#scheduling-dates-next");
        const date_prev = $("#scheduling-dates-prev");
        date_next.on('click', function () {
            changeDateTrackLeftPosition('+');
        });
        date_prev.on('click', function () {
            changeDateTrackLeftPosition('-');
        });

        function changeDateTrackLeftPosition(value) {
            const block_size = 77 * 3;
            const visible_length = 77 * 11;
            var scroll_width = date_track[0].scrollWidth;
            var current_position = parseInt(date_track.css('left'));

            if (value == '+' && current_position < (scroll_width - visible_length)) {
                date_track.css('left', `calc(${current_position}px - ${block_size}px)`);
            }
            if (value == '-') {
                if (current_position >= 0) {
                    date_track.css('left', '0px');
                } else {
                    date_track.css('left', `calc(${current_position}px + ${block_size}px)`);
                }

            }
        }

        $('.date-block').on('click', function(event){
            $.ajax('{{ route('get-slots') }}', {
                method:'POST',
                data: {
                    '_token' : '{{ csrf_token() }}',
                    'day_name': 'monday',
                    'doctor_id' : '{{ $doctor->id }}'
                },
                success: function(response) {
                    console.log(response)
                },
                error: function(err){
                    console.log(err);
                }
            })
            $(this).addClass('active');
        })

        function selectCurrentSlot(event) {
            $(event.target).parent().addClass('selected')
            $("input[name='time_selected']").val($(event.target).data('slot-id'));
            slot++;
            $("#slot-selection-count").html(slot);
        }

        $(document).ready(function(){
            $(".schedule-dates").click(selectDate);
        })
        function selectDate(event){
            console.log(event.currentTarget.dataset);
            if(event.currentTarget.dataset.date == undefined){
                alert("Cannot select date.");
                return;
            }
            $("input[name='date_selected']").val(event.currentTarget.dataset.date);
        }

    </script>
@endpush
