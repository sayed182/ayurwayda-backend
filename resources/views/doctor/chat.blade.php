<?php $page = "chat-doctor"; ?>
@extends('layout.mainlayout_withoutsidebar')
@section('content')

<!-- Page Content -->
<div class="content" style="padding:0;">
	<div class="container-fluid" style="padding:0;">
        <input type="hidden" name="agoraToken" value="{{ $token }}">
        <input type="hidden" name="agoraUser" value="{{ auth()->user()->email }}">
        <input type="hidden" name="agoraPeer" value="{{ $peer->email }}">
        <input type="hidden" name="channelName" value="my-channel">
		<div class="row">
			<div class="col-xl-12">
				<div class="chat-window">

					<!-- Chat Left -->
					<div class="chat-cont-left">
						<div class="chat-header">
							<span>Chats</span>
							<a href="javascript:void(0)" class="chat-compose">
								<i class="material-icons">control_point</i>
							</a>
						</div>
						<form class="chat-search">
							<div class="input-group">
								<div class="input-group-prepend">
									<i class="fas fa-search"></i>
								</div>
								<input type="text" class="form-control" placeholder="Search">
							</div>
						</form>
						<div class="chat-users-list">
							<div class="chat-scroll">
								<a href="javascript:void(0);" class="media">
									<div class="media-img-wrap">
										<div class="avatar avatar-away">
											<img src="{{asset('assets/img/patients/patient.jpg')}}" alt="User Image" class="avatar-img rounded-circle">
										</div>
									</div>
									<div class="media-body">
										<div>
											<div class="user-name">{{ user_fullname($peer) }}</div>
											<div class="user-last-chat">Hey, How are you?</div>
										</div>
										<div>
											<div class="last-chat-time block">2 min</div>
											<div class="badge badge-success badge-pill">15</div>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<!-- /Chat Left -->

					<!-- Chat Right -->
					<div class="chat-cont-right">
						<div class="chat-header">
							<a id="back_user_list" href="javascript:void(0)" class="back-user-list">
								<i class="material-icons">chevron_left</i>
							</a>
							<div class="media">
								<div class="media-img-wrap">
									<div class="avatar avatar-online">
										<img src="{{asset('assets/img/patients/patient.jpg')}}" alt="User Image" class="avatar-img rounded-circle">
									</div>
								</div>
								<div class="media-body">
									<div class="user-name">{{ user_fullname($peer) }}</div>
									<div class="user-status">online</div>
								</div>
							</div>
							<div class="chat-options">
								<a href="javascript:void(0)" data-toggle="modal" data-target="#voice_call">
									<i class="material-icons">local_phone</i>
								</a>
								<a href="javascript:void(0)" data-toggle="modal" data-target="#video_call">
									<i class="material-icons">videocam</i>
								</a>
								<a href="javascript:void(0)">
									<i class="material-icons">more_vert</i>
								</a>
							</div>
						</div>
						<div class="chat-body">
							<div class="chat-scroll">
								<ul class="list-unstyled" id="chatWindow">
									<li class="media sent">
										<div class="media-body">
											<div class="msg-box">
												<div>
													<p>Hello. What can I do for you?</p>
													<ul class="chat-msg-info">
														<li>
															<div class="chat-time">
																<span>8:30 AM</span>
															</div>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</li>
									<li class="media received">
										<div class="avatar">
											<img src="{{asset('assets/img/patients/patient.jpg')}}" alt="User Image" class="avatar-img rounded-circle">
										</div>
										<div class="media-body">
											<div class="msg-box">
												<div>
													<p>I'm just looking around.</p>
													<p>Will you tell me something about yourself?</p>
													<ul class="chat-msg-info">
														<li>
															<div class="chat-time">
																<span>8:35 AM</span>
															</div>
														</li>
													</ul>
												</div>
											</div>
											<div class="msg-box">
												<div>
													<p>Are you there? That time!</p>
													<ul class="chat-msg-info">
														<li>
															<div class="chat-time">
																<span>8:40 AM</span>
															</div>
														</li>
													</ul>
												</div>
											</div>
											<div class="msg-box">
												<div>
													<div class="chat-msg-attachments">
														<div class="chat-attachment">
															<img src="{{asset('assets/img/img-02.jpg')}}" alt="Attachment">
															<div class="chat-attach-caption">placeholder.jpg</div>
															<a href="" class="chat-attach-download">
																<i class="fas fa-download"></i>
															</a>
														</div>
														<div class="chat-attachment">
															<img src="{{asset('assets/img/img-03.jpg')}}" alt="Attachment">
															<div class="chat-attach-caption">placeholder.jpg</div>
															<a href="" class="chat-attach-download">
																<i class="fas fa-download"></i>
															</a>
														</div>
													</div>
													<ul class="chat-msg-info">
														<li>
															<div class="chat-time">
																<span>8:41 AM</span>
															</div>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</li>

								</ul>
							</div>
						</div>
						<div class="chat-footer">
							<div class="input-group">
								<div class="input-group-prepend">
									<div class="btn-file btn">
										<i class="fa fa-paperclip"></i>
										<input type="file">
									</div>
								</div>
								<input type="text" id="messageBox" class="input-msg-send form-control" placeholder="Type something">
								<div class="input-group-append">
									<button type="button" id="sendMessage" class="btn msg-send-btn"><i class="fab fa-telegram-plane"></i></button>
								</div>
							</div>
						</div>
					</div>
					<!-- /Chat Right -->

				</div>
			</div>
		</div>
		<!-- /Row -->

	</div>

</div>
<!-- /Page Content -->
</div>
</div>
<!-- Voice Call Modal -->
<div class="modal fade call-modal" id="voice_call">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">

				<!-- Outgoing Call -->
				<div class="call-box incoming-box">
					<div class="call-wrapper">
						<div class="call-inner">
							<div class="call-user">
								<img alt="User Image" src="{{asset('assets/img/patients/patient.jpg')}}" class="call-avatar">
								<h4>Richard Wilson</h4>
								<span>Connecting...</span>
							</div>
							<div class="call-items">
								<a href="javascript:void(0);" class="btn call-item call-end" data-dismiss="modal" aria-label="Close"><i class="material-icons">call_end</i></a>
								<a href="voice-call" class="btn call-item call-start"><i class="material-icons">call</i></a>
							</div>
						</div>
					</div>
				</div>
				<!-- Outgoing Call -->

			</div>
		</div>
	</div>
</div>
<!-- /Voice Call Modal -->

<!-- Video Call Modal -->
<div class="modal fade call-modal" id="video_call">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">

				<!-- Incoming Call -->
				<div class="call-box incoming-box">
					<div class="call-wrapper">
						<div class="call-inner">
							<div class="call-user">
								<img class="call-avatar" src="{{asset('assets/img/patients/patient.jpg')}}" alt="User Image">
								<h4>Richard Wilson</h4>
								<span>Calling ...</span>
							</div>
							<div class="call-items">
								<a href="javascript:void(0);" class="btn call-item call-end" data-dismiss="modal" aria-label="Close"><i class="material-icons">call_end</i></a>
								<a href="video-call" class="btn call-item call-start"><i class="material-icons">videocam</i></a>
							</div>
						</div>
					</div>
				</div>
				<!-- /Incoming Call -->

			</div>
		</div>
	</div>
</div>
<!-- Video Call Modal -->
<!-- jQuery -->
@endsection

@push('scripts')
    <script src="{{ asset('assets/js/agora/agora-rtm-sdk.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush
