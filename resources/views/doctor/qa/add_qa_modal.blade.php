<div id="question_modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="#" enctype="multipart/form-data" autocomplete="off" id="question_form" method="post">
                <div class="modal-header">
                    <h5 class="modal-title">Add Questions</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">

                    <input type="hidden" value="" name="id">
                    <input type="hidden" value="" name="method">
                    <div class="form-group">
                        <label for="category_name" class="control-label mb-10"> Question <span class="text-danger">*</span></label>
                        <input type="text" parsley-trigger="change" id="questions" name="questions" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="slug" class="control-label mb-10">Question Description </label>
                        <input type="text" parsley-trigger="change" id="questions_description" name="questions_description" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="description" class="control-label mb-10">Specilization </label>
                        <select class="form-control" name="specialization" id="specialization">

                            <option value="1">Stress &amp; Anxiety</option>
                            <option value="2">Digestive Disorders</option>
                            <option value="3">Arthritis</option>
                            <option value="4">Respiratory Disorders </option>
                            <option value="5">Weight Management</option>
                            <option value="6">Skin Problems</option>
                            <option value="7">Infertility</option>
                            <option value="8">Diabetes</option>
                            <option value="9">Chronic Disorders</option>
                            <option value="10">Insomnia</option>

                        </select>
                    </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline btn-default btn-sm btn-rounded" data-dismiss="modal">Close</button>
                    <button type="submit" id="btnfamilyssave" class="btn btn-outline btn-success ">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
