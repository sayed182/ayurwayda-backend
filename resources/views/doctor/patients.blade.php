<?php $page = "appointments"; ?>
@extends('layout.mainlayout', ['profile' => 'doctor'])
@section('content')

    <!-- Page Content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row row-grid">
                @foreach($users as $user)
                <div class="col-md-6 col-lg-4 col-xl-3">
                    <div class="card widget-profile pat-widget-profile">
                        <div class="card-body">
                            <div class="pro-widget-content">
                                <div class="profile-info-widget">
                                    <a href="/patient-profile" class="booking-doc-img">
                                        <img src="{{ asset('assets/img/profile-holder.png') }}" alt="User Image">
                                    </a>
                                    <div class="profile-det-info">
                                        <h3><a href="https://doccure-laravel.dreamguystech.com/template/public/patient-profile">{{$user->first_name}} {{ $user->last_name }}</a></h3>
                                        <div class="patient-details">
                                            <h5><b>Patient ID :</b> P{{ str_pad($user->id, 6,"0", STR_PAD_LEFT) }}</h5>
                                            <h5 class="mb-0"><i class="fas fa-map-marker-alt"></i> {{ isset($user->userDetails) ? $user->userDetails->state->state_name: ''}} ,{{ isset($user->userDetails) ? $user->userDetails->country->country_name: ''}}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="patient-info">
                                <ul>
                                    <li>Phone <span>{{ $user->mobileno }}</span></li>
                                    <li>Age <span>{{ isset($user->userDetails)? \Carbon\Carbon::make($user->userDetails->dob)->longAbsoluteDiffForHumans() :'' }}</span></li>
                                    <li>Blood Group <span>{{ isset($user->userDetails)? $user->userDetails->blood_group :'' }}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

    </div>
    <!-- /Page Content -->
    </div>

    <!-- Appointment Details Modal -->
    <div class="modal fade custom-modal" id="appt_details">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Appointment Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="info-details">
                        <li>
                            <div class="details-header">
                                <div class="row">
                                    <div class="col-md-6">
                                        <span class="title">#APT0001</span>
                                        <span class="text">21 Oct 2019 10:00 AM</span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-right">
                                            <button type="button" class="btn bg-success-light btn-sm" id="topup_status">
                                                Completed
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <span class="title">Status:</span>
                            <span class="text">Completed</span>
                        </li>
                        <li>
                            <span class="title">Confirm Date:</span>
                            <span class="text">29 Jun 2019</span>
                        </li>
                        <li>
                            <span class="title">Paid Amount</span>
                            <span class="text">$450</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /Appointment Details Modal -->
@endsection
