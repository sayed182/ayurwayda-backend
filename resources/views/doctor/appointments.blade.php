<?php $page = "appointments"; ?>
@extends('layout.mainlayout', ['profile' => 'doctor'])
@section('content')

    <!-- Page Content -->
    <div class="content">
        <div class="container-fluid">
            <div class="appointments">
            @forelse($appointments as $appointment)
                <!-- Appointment List -->
                    <div class="appointment-list">
                        <div class="profile-info-widget">
                            <a href="patient-profile" class="booking-doc-img">
                                <img src="{{ get_profile_image($appointment->patient) }}" alt="User Image">
                            </a>
                            <div class="profile-det-info">
                                <h3>
                                    <a href="patient-profile">{{ user_fullname($appointment->patient) }}</a>
                                </h3>
                                <div class="patient-details">
                                    <h5>
                                        <i class="far fa-clock"></i> {{ get_formatted_date($appointment->appointment_date).', '.generate_slot($appointment->time_slot)   }}</h5>
                                    <h5><i class="fas fa-map-marker-alt"></i> {{ user_address($appointment->patient) }} </h5>
                                    <h5>
                                        <i class="fas fa-envelope"></i> {{ $appointment->patient->email }}
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div class="appointment-action">
                            <x-invoice-modal :appointment='$appointment'/>

{{--                            <a href="javascript:void(0);" class="btn btn-sm bg-success-light"data-appointment='{{ json_encode($appointment) }}'>--}}
{{--                                <i class="fas fa-check"></i> Accept--}}
{{--                            </a>--}}
{{--                            <a href="javascript:void(0);" class="btn btn-sm bg-danger-light" data-appointment='{{ json_encode($appointment) }}'>--}}
{{--                                <i class="fas fa-times"></i> Cancel--}}
{{--                            </a>--}}
                        </div>
                    </div>
                    <!-- /Appointment List -->
                @empty
                @endforelse

            </div>
        </div>

    </div>
    <!-- /Page Content -->
    </div>


@endsection

