@push('styles')
    <style>
        .availability-table tbody tr td:first-child {
            width: 80%;
        }
    </style>
@endpush
@section('title', 'Doctor Profile Settings')
@extends('layout.mainlayout', ['profile' => 'doctor'])
@section('content')
    <form action="{{ route('post.doctor.profile_settings') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card">
            <div class="card-body"><h4 class="card-title">Basic Information</h4>
                <div class="row form-row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="change-avatar">
                                <div class="profile-img">
                                    <div
                                        style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                                        <div
                                            style="box-sizing: border-box; display: block; max-width: 100%;">
                                            <img alt="" aria-hidden="true"
                                                 src="{{ asset('/assets/img/profile-holder.png') }}"
                                                 style="max-width: 100%; display: block; margin: 0px; border: none; padding: 0px;">
                                        </div>
                                    </div>
                                </div>
                                <div class="upload-img">
                                    <div class="change-photo-btn"><span><i class="fa fa-upload"></i> Upload Photo</span><input
                                            type="file" class="upload"></div>
                                    <small class="form-text text-muted">Allowed JPG, GIF or PNG. Max
                                        size of 2MB</small></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Username <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" readonly value="{{ ($profile->slug)??'' }}"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email <span class="text-danger">*</span></label>
                            <input type="email" class="form-control" readonly value="{{ $profile->email??'' }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>First Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="first_name" required
                                   value="{{ $profile->first_name }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Last Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="last_name" required
                                   value="{{ $profile->last_name }}" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" class="form-control" name="mobileno" required
                                   value="{{ $profile->mobileno }}" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group"><label>Gender</label>
                            <select class="form-control select" name="gender">
                                <option>Select</option>
                                <option
                                    value="male" {{ $profile->userDetails!=null?($profile->userDetails->gender == 'male'?'selected':''):'' }}>
                                    Male
                                </option>
                                <option
                                    value="female" {{ $profile->userDetails!=null?($profile->userDetails->gender == 'female'?'selected':''):'' }}>
                                    Female
                                </option>
                            </select></div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-0"><label>Date of Birth</label>
                            <input type="date" class="form-control" name="dob" required
                                   value="{{ $profile->userDetails != null?($profile->userDetails->dob)->format('Y-m-d'):'' }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body"><h4 class="card-title">About Me</h4>
                <div class="form-group mb-0">
                    <label>Biography</label>
                    <textarea class="form-control" rows="5"
                              name="biography">{{ $profile->userDetails->biography??'' }}</textarea>
                </div>
            </div>
        </div>

        <div class="card contact-card">
            <div class="card-body"><h4 class="card-title">Contact Details</h4>
                <div class="row form-row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Address Line 1</label>
                            <input type="text" class="form-control" name="primary_address" required
                                   value="{{ $profile->userDetails->primary_address??'' }}"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Address Line 2</label>
                            <input type="text" class="form-control" name="secondary_address"
                                   value="{{ $profile->userDetails->secondary_address??'' }}"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Country</label>
                            <select class="form-control" name="country_id" required>
                                <option value="">Select an option</option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}"
                                        {{$profile->userDetails !=null?($profile->userDetails->country->id == $country->id?'selected':''):''}}
                                    >
                                        {{ $country->country_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">State / Province</label>
                            <select class="form-control" name="state_id" data-states="{{$states}}" required>
                                <option value="{{$profile->userDetails != null?$profile->userDetails->state->id:''}}"
                                        selected>
                                    {{ $profile->userDetails != null ? $profile->userDetails->state->state_name : ''}}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>City</label>
                            <input type="text" class="form-control" name="city_name" required
                                   value="{{ $profile->userDetails !=null ? $profile->userDetails->city_name : '' }}"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Postal Code</label>
                            <input type="text" class="form-control" name="post_code" required
                                   value="{{ $profile->userDetails->post_code??'' }}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('components.profile-settings.clinic-info')

        <div class="card">
            <div class="card-body"><h4 class="card-title">Pricing</h4>
                <div class="form-group mb-0">
                    <div id="pricing_select">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="price_free" name="price_selector" class="custom-control-input"
                                   value="free" checked/>
                            <label class="custom-control-label" for="price_free">Free</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="price_custom" name="price_selector"
                                   class="custom-control-input" value="paid"/>
                            <label class="custom-control-label" for="price_custom">Custom Price (per hour)</label>
                        </div>
                    </div>
                    <input type="text" class="form-control mt-2 d-none" id="price" name="price" value="0"/>
                </div>
            </div>
        </div>

        @include('components.profile-settings.education')
        @include('components.profile-settings.business-hours')

        <div class="submit-section submit-btn-bottom">
            <button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
        </div>
    </form>
@endsection
@push('scripts')
    <script>
        {{--   For State Selection   --}}
        $('select[name="country_id"]').on('change', function (e) {
            var id = $(this).val();
            var states = $('select[name="state_id"]').data('states');
            states = states.filter(state => state.country_id == id);
            var state_html = "";
            states.forEach(function (state) {
                state_html += `<option value=${state.id}>${state.state_name}</option>`;
            });
            $('select[name="state_id"]').html(state_html);
        });
        {{--   End For State Selection   --}}

        {{--   For Pricing Selection   --}}
        $('input[name="price_selector"]').on('change', function (e) {
            var selected_option = $(this).val();
            if (selected_option == "free") {
                $("#price").val(0);
                $("#price").addClass('d-none');
            } else {
                $("#price").removeClass('d-none');
            }
        })
    </script>
@endpush
