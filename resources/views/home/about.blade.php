@extends('layout.mainlayout_withoutsidebar')
@section('content')
    <section class="py-5 about-sec">
        <img src="{{asset("assets/img/about-bg.png")}}" class="about-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>AYURWAYDA</h1>
                    <h2> ____  The Way Of Life</h2>
                    <p class="py-3">We are introducing the first-ever dedicated platform for the traditional Indian medicine Ayurveda, <span>“AYURWAYDA-The Way of Life”</span>, Your Ultimate Guide to Healthy Living.</p>
                    <p>Today we are living in a world of A.I, Artificial Intelligence. The Healthcare industry has shown tremendous technological advancements. Taking it into consideration, Ayurwayda has come forward to offer expert solutions not only to your health-related issues but also to guide you on Healthy Living. This all comes to you with a tap of a few buttons, great comfort, ease, and fastidious approach through our highly advanced application.</p>
                </div>
                <div class="col-md-6">
                    <img src="{{asset("assets/img/about-banner.png")}}">
                </div>
            </div>
        </div>
    </section>
    <section class="ser-sec py-5 services-section">
        <img src="{{asset("assets/img/leaves.png")}}" alt="Image" class="leaves" style="bottom: unset;top: -60px">
        <img src="{{("assets/img/leaves1.png")}}" alt="Image" class="leaves" style="top: 40%;right: -54px;left: unset;width: 14%">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Our App Offers Services</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-md-6">
                    <div class="blue-bg"></div>
                    <img src="{{asset("assets/img/service1.png")}}">
                </div>
                <div class="col-md-6">
                    <h4 class="pt-4">Healthcare Seekers</h4>
                    <p class="pt-4">Our app lets you operate from your mobile or laptop or tablet from any corner of the world. We strictly comply with social and medical ethics, so your identity remains anonymous and your medical details. You can opt for free queries or directly <span>connect with the doctor</span> of your choice after paying the consultation charges. Our app also allows you to <span>book prepaid appointments</span> at your preferred time and day, with your doctor or Healthcare institution/Hospital. Your investigations can also be booked through our app, and it even allows you to avail of the medicines right at your doorstep just by uploading the prescriptions. Complete guidance on staying fit and restoration of the Health, exclusively on Ayurvedic Guidelines, can be obtained by our healthy customers, as well.</p>
                </div>
            </div>
            <div class="row pt-5">

                <div class="col-md-6">
                    <h4 class="pt-5">Healthcare Providers</h4>
                    <p class="pt-4">As doctors, you can contact patients across the globe right from your clinic/hospital; offer valuable guidance and treatments through our simple yet highly advanced app. Moreover, the medical details of your patients are easily accessible for further/future references. Our easy to operate app ensures to <span>boost your clinical practice</span>, allows better connectivity with your patients, and makes your presence known at broader aspects.</p>
                </div>
                <div class="col-md-6">
                    <img src="{{asset("assets/img/service2.png")}}">
                    <div class="blue-bg1"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="ser-sec py-5 teams">
        <img src="{{asset("assets/img/leaves.png")}}" alt="Image" class="leaves" style="bottom: unset;top: -60px">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>A Team With Passionate People</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </div>
            </div>
            <div class="row pt-4 text-center">
                <div class="col-md-4">
                    <img src="{{asset("assets/img/team1.png")}}">
                    <h6 class="pt-3">Jhone Doe</h6>
                    <p>CEO &amp; Founder</p>
                    <ul class="list-unstyled list-inline-item social-link">
                        <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <img src="{{asset("assets/img/team2.png")}}">
                    <h6 class="pt-3">Jhone Doe</h6>
                    <p>CEO &amp; Founder</p>
                    <ul class="list-unstyled list-inline-item social-link">
                        <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <img src="{{asset("assets/img/team3.png")}}">
                    <h6 class="pt-3">Jhone Doe</h6>
                    <p>CEO &amp; Founder</p>
                    <ul class="list-unstyled list-inline-item social-link">
                        <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>

        </div>
    </section>
    <section class="ser-sec py-5 modus">
        <div class="container">

            <div class="row pt-5">

                <div class="col-md-6">
                    <h4 class="">Our Modus Operandi</h4>
                    <p class="pt-4">The most precious thing that we cherish all our lives is Life itself. And the essence of the Good Life is Health in its Optimum state. Ayurwayda-The Way of living helps you achieve this! Our mottos are to congregate experts <span>Vaidyas (Ayurvedic doctors)</span> from all across India and get their expertise optimized by reaching all those seeking to benefit but are either unable due to time and travel constraints or unaware of such specialties.</p>
                    <p>Our services have to be the finest. We will make every possible effort to ensure that our app is user friendly at every level of its usage. Feel free to reach us, anytime, if there is any technical glitch or issue that needs our attention.</p>
                    <p>We are glad to welcome you in the <span>journey of Blissful Living through AYURWAYDA</span>. At the same time, we express our sincere gratitude towards you for being a part of our dream of making every living soul on earth to be as Healthy and as Fit as ever!</p>
                </div>
                <div class="col-md-6">
                    <img src="{{asset("assets/img/modus.png")}}">
                </div>
            </div>
        </div>
    </section>

@endsection
