@push('styles')
    <style>
        .card-disclaimer {
            background: #FFF8F2;
            padding: 20px 40px;
            border-color: transparent;
        }

        #specialization {
            color: #B6B6B6;
        }

        input::placeholder {
            color: #B6B6B6;
        }

        #keywords::-webkit-input-placeholder {
            color: red;
        }

        .tags-container {
            padding: 1.25rem;
        }

        .tags-heading {
            font-size: 18px;
            font-weight: 600;
            line-height: 26px;
            margin-bottom: 0px;
        }

        .all-qa-section {
            padding-bottom: 80px;
        }

        .section-heading {
            font-size: 24px;
            font-weight: 700;
            line-height: 26px;
        }

        .qa-list-item {
            padding: 20px;
            display: flex;
            border: 1px solid #00000033;
            border-radius: 5px;
            margin-bottom: 20px;
        }

        .qa-list-item .card-body {
            padding: 0px 0px 0px 20px;
        }

        .qa-image {
            height: 150px;
            width: 172px;
            border-radius: 5px;
            background-size: cover;
        }

        .qa-body {
            width: calc(100% - 172px);
        }

        .qa-title {
            font-size: 20px;
            font-weight: 700;
            line-height: 26px;
            color: #050505;
        }

        .qa-subtext {
            font-size: 14px;
            font-weight: 400;
            line-height: 26px;
            color: rgba(0, 0, 0, 0.54);
        }

        .qa-reference {
            font-size: 16px;
            font-weight: 600;
            line-height: 26px;
            color: #050505;
        }

        .qa-leaf {
            max-width: 146px;
            position: fixed;
            z-index: 100;
            right: -28px;
            top: 50%;
            transform: translateY(-50%);
        }

        .qa-leaf img {
            display: block;
            width: 100%;
        }

    </style>
@endpush
@extends('layout.mainlayout_withoutsidebar')
@section('content')
    <div class="qa-leaf">
        <img src="{{ asset('assets/img/qa-leaf.svg') }}" alt="">
    </div>
    <div class="container mt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card verify-notification">
                    <div class="col-md-8">
                        <h4 style="color:#1D72CD">Want to share queries &amp; get answers?</h4>
                        <h6 style="color:#1D72CD;font-weight:300">Get solutions &amp; advice from Doctors</h6>
                    </div>
                    <div class="col-md-2"><a href="{{ route('qa.create') }}">
                            <button class="btn btn-info" style="background:#1D72CD; border: none;">Ask a Question
                            </button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-1">
                <div class="card card-disclaimer">
                    <div class="row">
                        <div class="col-2">
                            <h4 class="text-right">Disclaimer </h4>
                        </div>
                        <div class="col-10">
                            <p>
                                The purpose of this article is to provide you with information. If you use any medicine,
                                therapy, or herb, make sure you do it under the supervision of a qualified Ayurveda
                                doctor.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="card border-0">

            <x-qa.search :specializations="$specializations" />

        </div>
    </div>

    <div class="container">
        <div class="col-lg-12 pt-4">
            <h4 class="section-heading">Latest Questions</h4>
            <div class="doctor-slider slider">
                @forelse ($latestQuestions as $question)
                    <div class="blog grid-blog">
                        <div class="blog-image">
                            <a href="{{ route('qa.details', $question->id) }}">
								<img class="img-fluid" src="{{ asset('storage/' . $question->images()->first()->image) }}"
                                    alt="Post Image">
								</a>
                        </div>
                        <div class="blog-content">
                            <h3 class="blog-title">
								<a
                                    href="{{ route('qa.details', $question->id) }}">
									{{ \Illuminate\Support\Str::words($question->body, 10, '...') }}
								</a>
							</h3>
                            <ul class="entry-meta meta-item">
                                <li>
                                    <div class="post-author">
                                        @if ($question->answers->isNotEmpty())
                                            <a href="doctor-profile"><img src="assets/img/doctors/doctor-thumb-01.jpg"
                                                    alt="Post Author">
                                                <span>
                                                    Answered By {{ $question->answers()->first()?->replier->full_name }}
												</span>
											</a>
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
				@empty
					<div>No questions found.</div>
                @endforelse
            </div>
        </div>

        <div class="col-12">
            <div class="card tags-container">
                <div class="row align-items-center">
                    <div class="col-2">
                        <h4 class="tags-heading">Top Speciallities:</h4>
                    </div>
                    <div class="col-8">
                        <ul class="list-unstyled d-flex flex-wrap mb-0" style="gap: 10px">
                            @foreach ($specializations as $specialization)
								<a href="{{ route('qa', ['specialization' => $specialization->id]) }}">
									<li class="rounded-pill bg-success px-3 py-1 fs-6 text-white">
										{{ $specialization->specialization_name }}
									</li>
								</a>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="all-qa-section">
        <div class="container">
            <div class="col-12">
                <h4 class="section-heading">All Q&A</h4>
            </div>
            <div class="col-12 mt-3">
                <ul class="list-unstyled qa-list">
                    @forelse ($questions as $question)
                        <li class="qa-list-item">
                            <div class="qa-image"
                                style="background-image: url('{{ asset('storage/' . $question->images()->first()->image) }}')">
                            </div>
                            <div class="qa-body">
                                <div class="card-body h-100 d-flex flex-column justify-content-between" style="">
                                    <h5 class="card-title qa-title">
                                        <a href="/qa/{{ $question->id }}">
                                            {{ ($questions->currentPage() - 1) * $questions->perPage() + $loop->index + 1 }}.
                                            {{ $question->body }}
                                        </a>
                                    </h5>
									
									@if ($question->answers->isNotEmpty())
										@php
											$answer = $question->answers->first();
											$doctor = $answer->replier;
										@endphp

										<p class="card-text qa-subtext">
											{{ Illuminate\Support\Str::words($answer->body, 30, '...') }}
										</p>
										<p class="card-text qa-reference">
											Answered by 
											<a href="{{ route('doctor.show.profile', $doctor->slug) }}" class="small text-success">
												{{ $doctor->full_name }}
											</a>
										</p>
									@endif
                                    <div>
                                        <ul class="list-unstyled d-flex flex-wrap mb-0" style="gap: 10px">
                                            @foreach ($question->specializations as $specialization)
												<a href="{{ route('qa', ['specialization' => $specialization->id]) }}">
													<li class="rounded-pill bg-success px-3 py-1 fs-6 text-white">
														{{ $specialization->specialization_name }}
													</li>
												</a>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
					@empty
						<div class="mt-5 text-center">No questions found.</div>
                    @endforelse
                </ul>

                @if ($questions->hasMorePages())
                    <div class="text-center d-block mt-3 pagination">
                        <a href="{{ $questions->nextPageUrl() }}" class="btn btn-outline-success rounded-pill"
                            id="load-more">Load More</a>
                    </div>
                @endif
            </div>
        </div>
    </section>

    <div class="alert alert-danger alert-dismissible fade" id="loadMoreAlert" role="alert"
        style="position: fixed; top: 1rem; right: 1rem;">
        Something went wrong. Unable to load more.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endsection

@push('scripts')
    <script>
        $('.alert').alert()

        let qaList = document.querySelector('.qa-list');
        let loadMore = document.querySelector('#load-more');

        loadMore.addEventListener('click', async function(e) {
            e.preventDefault();
            let nextLink = loadMore.getAttribute('href');

            loadMore.innerHTML =
                `<span class="spinner-border spinner-border-sm mr-3" role="status" aria-hidden="true"></span>Loading...`;

            try {
                let res = await fetch(nextLink);
                let html = await res.text();

                let parser = new DOMParser();
                let doc = parser.parseFromString(html, 'text/html');

                let questions = doc.querySelectorAll('.qa-list-item');
                qaList.append(...questions);

                if (doc.querySelector('#load-more') === null) {
                    console.log('Reached end of page.');
                    document.querySelector('.pagination').remove();
                    return;
                }

                loadMore.setAttribute('href', 'http://aloo.com');
                // loadMore.setAttribute('href', doc.querySelector('#load-more').getAttribute('href'));
            } catch (error) {
                console.error(error);

                $('#loadMoreAlert').removeClass('hide').addClass('show');
            } finally {
                loadMore.innerHTML = 'Load More';
            }
        })

        $('#loadMoreAlert').on('close.bs.alert', function(e) {
            e.preventDefault();
            $('#loadMoreAlert').removeClass('show').addClass('hide');
        })
    </script>
@endpush
