@push('styles')
    <style>
        .card-disclaimer {
            background: #FFF8F2;
            padding: 20px 40px;
            border-color: transparent;
        }

        #specialization {
            color: #B6B6B6;
        }

        input::placeholder {
            color: #B6B6B6;
        }

        #keywords::-webkit-input-placeholder {
            color: red;
        }

        .tags-container {
            padding: 1.25rem;
        }

        .tags-heading {
            font-size: 18px;
            font-weight: 600;
            line-height: 26px;
            margin-bottom: 0px;
        }

        .all-qa-section {
            padding-bottom: 80px;
        }

        .section-heading {
            font-size: 24px;
            font-weight: 700;
            line-height: 26px;
        }

        .qa-list-item {
            padding: 20px;
            display: flex;
            border: 1px solid #00000033;
            border-radius: 5px;
            margin-bottom: 20px;
        }

        .qa-list-item .card-body {
            padding: 0px 0px 0px 20px;
        }

        .qa-image {
            height: 150px;
            width: 172px;
            border-radius: 5px;
            background-size: cover;
        }

        .qa-body {
            width: calc(100% - 172px);
        }

        .qa-title {
            font-size: 20px;
            font-weight: 700;
            line-height: 26px;
            color: #050505;
        }

        .qa-subtext {
            font-size: 14px;
            font-weight: 400;
            line-height: 26px;
            color: rgba(0, 0, 0, 0.54);
        }

        .qa-reference {
            font-size: 16px;
            font-weight: 600;
            line-height: 26px;
            color: #050505;
        }

        .qa-image-container {
            height: 500px;
            display: block;
            border-radius: 16px;
            margin-top: 60px;
            margin-bottom: 100px;
            background-color: black;
            background-size: contain;
            background-repeat: no-repeat;
        }

        .patient-image {
            width: 120px;
            height: 120px;
        }

        .patient-query .card-body {
            display: flex;
            flex-direction: row;
            align-items: flex-start;
        }

        .patient-image img {
            width: 80px;
            height: 80px;
            padding: 15px;
            border-radius: 100%;
        }

        .patient-query-body {
            font-size: 14px;
			flex: 1;
        }

    </style>
@endpush
@extends('layout.mainlayout_withoutsidebar')
@section('content')
    <section class="search-section mt-3">
        <div class="container">
            <div class="card border-0">

                <x-qa.search :specializations="$specializations" />

            </div>
        </div>
    </section>

    <section class="qa-detail">
        <div class="container">
            <h4 class="section-heading">Q. {{ $question->body }}</h4>
            <small class="text-muted"> Published on {{ $question->created_at->format('M d, Y') }} </small>
			
			@foreach ($question->images as $image)
				<div class="qa-image-container "
					style="background-image: url('{{ asset('storage/' . $image->image) }}'); background-position: center">
				</div>
			@endforeach
			
            @forelse ($question->replies as $reply)
                <x-qa.reply :reply="$reply" :first-answer="$firstAnswer" />
            @empty
                <h3 class="text-center mb-5">No one has answered yet.</h3>
            @endforelse
        </div>
    </section>

	@auth
    <section class="qa-reply-form">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('qa.replies.store', $question->id) }}" method="post">
                        @csrf

                        <div class="form-group">
                            <label for="reply-body-input">
                                Your Answer
                            </label>
                            <textarea name="body" id="reply-body-input" placeholder="Enter your answer" class="form-control" aria-invalid="false"
                                rows="7" required>{{ old('body') }}</textarea>

                            @error('body')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-success">
                            Post Your Answer
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
	@endauth
	@guest
		<section>
			<div class="container">
				<div class="text-lg my-5">
					<a href="{{ route('user.get.login') }}" class="text-primary">Log In</a> or <a href="{{ route('user.get.register') }}" class="text-primary">Sign Up</a> to post a reply.
				</div>
			</div>
		</section>
	@endguest

    <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="edit-modal-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Reply</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('qa.replies.store', $question->id) }}" method="post">
                        @csrf

                        <div class="form-group">
                            <label for="reply-body-input">
                                Your Answer
                            </label>
                            <textarea name="body" id="reply-body-input" placeholder="Enter your answer" class="form-control" aria-invalid="false"
                                rows="7" required></textarea>

                            @error('body')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.edit-btn').click(function() {
                $(this).addClass(
                    'edit-btn-clicked'
                ); //useful for identifying which trigger was clicked and consequently grab data from the correct row and not the wrong one.

                $('#edit-modal').modal();
            })

            // // on modal show
            // $('#edit-modal').on('show.bs.modal', function() {
            //     var el = $(".edit-btn-clicked"); // See how its usefull right here? 
            //     var row = el.closest(".reply");

            //     // get the data
            //     var id = row.data('reply-id');
            //     var replyBody = row.children(".reply-body").text();

            //     // fill the data in the input fields
            //     $("#edit-modal form").attr('action', "/qa/replies/" + id);
            //     $("#edit-modal textarea").val(replyBody);

            // })

            // // on modal hide
            // $('#edit-modal').on('hide.bs.modal', function() {
            //     $('.edit-btn-clicked').removeClass('edit-btn-clicked')
            //     $("#edit-modal form").trigger("reset");
            // })
        })
    </script>
@endpush
