@push('styles')
    <style>
        .card-disclaimer {
            background: #FFF8F2;
            padding: 20px 40px;
            border-color: transparent;
        }

        #specialization {
            color: #B6B6B6;
        }

        input::placeholder {
            color: #B6B6B6;
        }

        #keywords::-webkit-input-placeholder {
            color: red;
        }

        .tags-container {
            padding: 1.25rem;
        }

        .tags-heading {
            font-size: 18px;
            font-weight: 600;
            line-height: 26px;
            margin-bottom: 0px;
        }

        .all-qa-section {
            padding-bottom: 80px;
        }

        .section-heading {
            font-size: 24px;
            font-weight: 700;
            line-height: 26px;
        }

        .qa-list-item {
            padding: 20px;
            display: flex;
            border: 1px solid #00000033;
            border-radius: 5px;
            margin-bottom: 20px;
        }

        .qa-list-item .card-body {
            padding: 0px 0px 0px 20px;
        }

        .qa-image {
            height: 150px;
            width: 172px;
            border-radius: 5px;
            background-size: cover;
        }

        .qa-body {
            width: calc(100% - 172px);
        }

        .qa-title {
            font-size: 20px;
            font-weight: 700;
            line-height: 26px;
            color: #050505;
        }

        .qa-subtext {
            font-size: 14px;
            font-weight: 400;
            line-height: 26px;
            color: rgba(0, 0, 0, 0.54);
        }

        .qa-reference {
            font-size: 16px;
            font-weight: 600;
            line-height: 26px;
            color: #050505;
        }

        .qa-image-container {
            height: 500px;
            display: block;
            border-radius: 16px;
            margin-top: 60px;
            margin-bottom: 100px;
        }

        .patient-image {
            width: 120px;
            height: 120px;
        }

        .patient-query {
            padding: 20px;
            display: flex;
            flex-direction: row;
        }

        .patient-image img {
            width: 80px;
            height: 80px;
            padding: 15px;
            border-radius: 100%;
        }

        .patient-query-body {
            font-size: 14px;
        }

    </style>
@endpush
@extends('layout.mainlayout_withoutsidebar')
@section('content')
    <section class="create-qa">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <h1>Ask a doctor</h1>
                            <form action="{{ route('qa.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf

                                <div class="create-form">
                                    <div class="form-group">
                                        <label for="query">Enter Your Health Query <span
                                                class="text-danger">*</span></label>
                                        <input type="text" name="query" id="query"
                                            placeholder="Type your health query with symptoms" class="form-control valid"
                                            aria-invalid="false" value="{{ old('query') }}" required>

                                        @error('query')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Choose A Speciality <span class="text-danger">*</span></label>
                                        <select class="form-control select select2-hidden-accessible"
                                            name="specializations[]" id="specializations" tabindex="-1" aria-hidden="true"
                                            multiple required>
                                            @foreach ($specializations as $specialization)
                                                <option value="{{ $specialization->id }}"
                                                    @if (in_array($specialization->id, old('specializations', []))) selected @endif>
                                                    {{ $specialization->specialization_name }}
                                                </option>
                                            @endforeach
                                        </select>

                                        @error('specializations')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Add images (max: 4)</label>
										<div id="image-inputs" class="d-flex flex-column" style="gap: 1rem;">
										</div>
										<button class="btn btn-outline-success btn-sm my-3" id="add-more" type="button" onclick="addImageInput()">Add More</button>

                                        @error('images')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror

                                        @error('images.*')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-success">Submit & Continue</button>
                            </form>
                        </div>
                        <div class="col-6">
                            <img src="{{ asset('assets/img/qa-create-banner.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
	<script>
		let addMoreBtn = document.querySelector('#add-more');

		function addImageInput() {
			let imageInputs = document.querySelectorAll('input[type=file]');
			if (imageInputs.length >= 4) {
				return;
			}

			let inputDiv = document.createElement("div");
			inputDiv.innerHTML = `<input type="file" accept="image/png, image/jpg, image/jpeg" name="images[]" class="form-control valid">`;
			inputDiv.innerHTML += "<button class='btn btn-danger btn-sm px-3 delete-image' type='button'><i class='fas fa-trash'></i></button>";
			inputDiv.className = "image-input-container d-flex";

			document.getElementById('image-inputs').appendChild(inputDiv);

			if (imageInputs.length === 3) {
				addMoreBtn.setAttribute('disabled', true);
				return;
			}
		}

		$(document).on('click', '.delete-image', function() {
			$(this).closest('.image-input-container').remove();
			addMoreBtn.removeAttribute('disabled');
		})
	</script>
@endpush
