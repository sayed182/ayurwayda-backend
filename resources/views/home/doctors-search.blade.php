@extends('layout.mainlayout_withoutsidebar')
@section('content')
    <div class="breadcrumb-bar">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-8 col-12">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Doctor Search</li>
                        </ol>
                    </nav>
                    <h2 class="breadcrumb-title search-results"></h2>
                </div>
                <div class="col-md-4 col-12 d-md-block d-none">
                    <div class="sort-by">
                        <span class="sort-title">Sort by</span>
                        <span class="sortby-fliter">
									<select class="select form-control select2-hidden-accessible" id="orderby"
                                            onchange="search_doctor(0)" data-select2-id="orderby" tabindex="-1"
                                            aria-hidden="true">
										<option value="" data-select2-id="2">Select</option>
										<option class="sorting" value="Rating">Rating</option>

										<option class="sorting" value="Latest">Latest</option>
										<option class="sorting" value="Free">Free</option>
									</select>
								</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content" style="transform: none; min-height: 27.312px;">
        <div class="container" style="transform: none;">

            <div class="row" style="transform: none;">
                <div class="col-md-12 col-lg-4 col-xl-3 theiaStickySidebar"
                     style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                    <div class="theiaStickySidebar"
                         style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none;">
                        <div class="card search-filter">
                            <div class="card-header">
                                <h4 class="card-title mb-0">Search Filter <a href="javascript:void(0);"
                                                                             onclick="reset_doctor()"
                                                                             class="text-danger text-sm float-right">Reset</a>
                                </h4>
                            </div>
                            <div class="card-body">
                                <form id="search_doctor_form">
                                    <div class="filter-widget">
                                        <select class="form-control " name="appointment_type" id="appointment_type">
                                            <option value=""> Booking Type</option>
                                            <option value="online">Online</option>
                                            <option value="clinic">Clinic</option>
                                        </select>
                                    </div>

                                    <div class="filter-widget">
                                        <select name="specialization" class="form-control " id="specialization">
                                            <option value="">Select Specialization</option>
                                            @foreach($specializations as $sp)
                                                <option value="{{ $sp->id }}">{{ $sp->specialization_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="filter-widget">
                                        <select name="city" class="form-control " id="city">
                                            <option value="">Select City</option>
                                            @foreach($cities as $city)
                                                <option value="{{ $city->slug }}">{{ $city->city_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="btn-search">
                                        <button type="button" onclick="search_doctor(0)" class="btn btn-block">Search
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="resize-sensor"
                             style="position: absolute; inset: 0px; overflow: hidden; z-index: -1; visibility: hidden;">
                            <div class="resize-sensor-expand"
                                 style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
                                <div
                                    style="position: absolute; left: 0px; top: 0px; transition: all 0s ease 0s; width: 295px; height: 654px;"></div>
                            </div>
                            <div class="resize-sensor-shrink"
                                 style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
                                <div
                                    style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-lg-8 col-xl-9">
                    <input type="hidden" name="page" id="page_no_hidden" value="1">
                    <div id="doctor-list">
                        @foreach($doctors as $doctor)
                            <div class="card">
                                <div class="card-body">
                                    <div class="doctor-widget">
                                        <div class="doc-info-left">
                                            <div class="doctor-img">
                                                <a href="{{route('doctor.show.profile', ['user' => $doctor->slug])}}">
                                                    <img src="{{ get_profile_image($doctor) }}" class="img-fluid" alt="User Image">
                                                </a>
                                            </div>
                                            <div class="doc-info-cont">
                                                <h4 class="doc-name"><a href="{{ route('doctor.show.profile', ['user' => $doctor->slug]) }}">Dr. {{ user_fullname($doctor) }}</a></h4>
                                                <p class="doc-speciality">{{ isset($doctor->doctorDetails->education)?$doctor->doctorDetails->education:"" }}</p>
                                                <h5 class="doc-department"><img src="https://doccure-laravel.dreamguystech.com/template/public/assets/img/specialities/specialities-05.png" class="img-fluid" alt="Speciality">Dentist</h5>
                                                <div class="rating">
                                                    <i class="fas fa-star filled"></i>
                                                    <i class="fas fa-star filled"></i>
                                                    <i class="fas fa-star filled"></i>
                                                    <i class="fas fa-star filled"></i>
                                                    <i class="fas fa-star"></i>
                                                    <span class="d-inline-block average-rating">(17)</span>
                                                </div>
                                                <div class="clinic-details">
                                                    <p class="doc-location"><i class="fas fa-map-marker-alt"></i> {{ isset($doctor->doctorDetails->clinic_address)?$doctor->doctorDetails->clinic_address:"" }} </p>
                                                    @if(isset($doctor->doctorDetails->clinic_image))
                                                    <ul class="clinic-gallery">
                                                        @foreach($doctor->doctorDetails->clinic_image as $image)
                                                        <li>
                                                            <a href="{{ $image }}" data-fancybox="gallery">
                                                                <img src="{{ $image }}" alt="Feature">
                                                            </a>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                    @endif
                                                </div>
                                                @if(isset($doctor->doctorDetails->experience))
                                                <div class="clinic-services">
                                                    @foreach($doctor->doctorDetails->experience as $experience)
                                                        <span>{{ $experience }}</span>
                                                    @endforeach
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="doc-info-right">
                                            <div class="clini-infos">
                                                <ul>
                                                    <li><i class="far fa-thumbs-up"></i> 98%</li>
                                                    <li><i class="far fa-comment"></i> 17 Feedback</li>
                                                    <li><i class="fas fa-map-marker-alt"></i> {{ isset($doctor->doctorDetails->clinic_address)?$doctor->doctorDetails->clinic_address:"" }} </li>
                                                    <li><i class="far fa-money-bill-alt"></i> {{ get_money_sign() }}300 - {{ get_money_sign() }}1000 <i class="fas fa-info-circle" data-bs-toggle="tooltip" title="" data-bs-original-title="Lorem Ipsum" aria-label="Lorem Ipsum"></i> </li>
                                                </ul>
                                            </div>
                                            <div class="clinic-booking">
                                                <a class="view-pro-btn" href="{{ route('doctor.show.profile', ['user' => $doctor->id]) }}">View Profile</a>
                                                <a class="apt-btn" href="{{ route('patient.book-appointment', ['doctor' => $doctor->slug]) }}">Book Appointment</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="load-more text-center d-none" id="load_more_btn">
                        <a class="btn btn-primary btn-sm" href="javascript:void(0);">Load More</a>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
