<?php
$specilization = "";
$city = "N/A";

if ($doctor->specialization()->get() !== null) {
    foreach ($doctor->specialization()->get() as $sp) {
        $specilization.= $sp->specialization_name . ', ';
    }
    trim($specilization, ',');
}

if($doctor->userDetails != null){
    $city = $doctor->userDetails->city_name;
}

?>
<!-- Doctor Widget -->
<div class="profile-widget dr-profile">
    <div class="doc-img">
        <a href="{{ route('doctor.show.profile', ['user' => $doctor->slug]) }}">
            <img width="228" height="152" class="img-fluid" alt="User Image"
                 src="{{ get_profile_image($doctor) }}">
        </a>
        <a href="javascript:void(0)" id="favourities_4" onclick="add_favourities('4')"
           class="fav-btn ">
            <i class="fas fa-heart"></i>
        </a>
    </div>
    <div class="pro-content">
        <h3 class="title">
            <a class="font-weight-bold"
               href="{{ route('doctor.show.profile', ['user' => $doctor->slug]) }}">Dr.
                {{ user_fullname($doctor)}}</a>
            <i class="fas fa-check-circle verified"></i>
        </h3>
        <!-- <p class="speciality">Stress & Anxiety</p> -->
        <p class="speciality">
            {{ $specilization }}
        </p>
        <!-- <p class="speciality">5 Years Experience</p> -->
        <div class="rating">
            <span class="d-inline-block average-rating">1</span>
            <i class="fas fa-star filled"></i>
        </div>
        <ul class="available-info">

            <li>
                <i class="fas fa-map-marker-alt"></i>
                {{ $city }}
            </li>
            <li>
                <i class="far fa-money-bill-alt"></i> ₹5.2
            </li>
        </ul>
        <div class="row row-sm">
            <div class="col-6">
                <a href="{{ route('doctor.show.profile', ['user' => $doctor->slug]) }}"
                   class="btn view-btn">View Profile</a>
            </div>
            <div class="col-6">
                <a href="{{ route('patient.book-appointment', ['doctor' => $doctor->slug]) }}"
                   class="btn book-btn">Book Now</a>
            </div>
        </div>
    </div>
</div>
<!-- /Doctor Widget -->
