@push('styles')
    <style>
        .bg {
            /* The image used */
            background-image: url("assets/img/login-banner-005-a.jpg");

            /* Full height */
            height: 100%;

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;

        }

        .background-multiply .content {
            background-color: #636363 !important;
            background-blend-mode: multiply;
        }
    </style>

    <style>
        .inner-addon {
            position: relative;
        }

        .eg{
            transition: all 0.5s ease;
        }


        #locationClickableIcon {
            position: absolute;
            cursor: pointer;
            top: 50%;
            left: 90%;
            transform: translateY(-50%);
            z-index: 20;
        }

        input#search_keywords::placeholder {
            color: black !important;
            font-size: 14px;
        }

        input#search_keywords:focus + .eg{
            bottom: 40px;
        }

        input#search_location:focus + .eg{
            bottom: 40px;
        }

    </style>
@endpush

@extends('layout.mainlayout_withoutsidebar')
@section('content')
    <!-- Home Banner -->
    <div class="bg_img1">
        <section class="section section-search">
            <div class="container">
                <div class="banner-wrapper w-100">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="banner-header">
                                <!-- <h1>Search Ayurveda Doctor, Make an Appointment</h1> -->
                                <h1>
                                    Got A Health Issue? <br/>Consult Top <span>Ayurvedic Doctors</span>
                                </h1>
                                <p>Discover the best ayurvedic doctors, clinic & hospital the city nearest to you.</p>
                            </div>
                            <!-- map changes -->
                            <input id="street_number" type="hidden"/>
                            <input disabled="disabled" id="route" type="hidden"/>
                            <input disabled="disabled" id="locality" type="hidden"/>
                            <input disabled="disabled" id="administrative_area_level_1" type="hidden"/>
                            <input disabled="disabled" id="postal_code" type="hidden"/>
                            <input disabled="disabled" id="country" type="hidden"/>
                            <!-- map changes -->
                            <!-- Search -->
                            <div class="search-box">
                                <form method="post" action="#" style="position: relative;">
                                    <i class="fa fa-crosshairs fa-lg for-mob" id="autolocate"
                                       style="color: #3dac7b"></i>
                                    <div class="form-group mb-0 search-location input-container">
                                            <span id="locationClickableIcon" onclick="locate()"><i
                                                    class="fa fa-crosshairs fa-lg for-web" id='autolocate'
                                                    style="color: #3dac7b"></i></span>
                                        <div style="position: absolute;top: 50%;transform: translateY(-42%);">
                                            <input type="text" class="form-control" autocomplete="off"
                                                   id="search_location"
                                                   name="location" placeholder="Search Location">
                                            <span class="eg">Eg:Delhi</span>
                                        </div>


                                        <div class="location_result"></div>

                                    </div>
                                    <div class="form-group mb-0 search-info input-container">
                                        <div
                                            style="position: absolute;top: 50%;transform: translateY(-42%); width: 100%">
                                            <input type="text" class="form-control" autocomplete="off"
                                                   onkeyup="search_keyword()" id="search_keywords" name="keywords"
                                                   placeholder="Search Doctors, clinics, hospitals,etc.">
                                            <span class="eg eg1">Eg. Fortis Hospital or Cancer</span>
                                        </div>

                                        <!-- <span class="form-text">Ex : Dental or Sugar Check up etc</span> -->
                                        <div class="keywords_result"></div>
                                    </div>
                                    <button type="button" class="btn btn-primary search-btn" id="search_button">Search
                                        <span>Search</span></button>
                                </form>

                            </div>
                            <!-- /Search -->
                        </div>

                    </div>
                    <div class="row pt-4">
                        <div class="col-md-3">
                            <ul class="list-unstyled mob-0">
                                <li>Book an appointment</li>
                                <li>Convenient and easy</li>

                            </ul>
                        </div>
                        <div class="col-md-3">
                            <ul class="list-unstyled">
                                <li>Consult with verified doctors</li>
                                <li>Free Follow up</li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <br/><br/><br/>
        <!-- /Home Banner -->

        <section class="py-5 looking-for">
            <div class="container for-web">
                <div class="row">
                    <div class="col-md-3">
                        <div class="pt-5 pb-5 pr-3">
                            <h2>What Are You Looking For?</h2>
                            <p>Ayurvedic treatment has proven the best result in healthcare and we are providing you
                                every service at home.</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card text-center h-100">
                            <div class="card-body">
                                <img src="assets/img/icon-1.png" alt="image">
                                <h5>Visit A Doctor</h5>
                                <p>Select preferred ayurvedic doctor and book an appointment.</p>
                                <a href="http://localhost/ayurwayda/doctors-search"
                                   class="text-decoration-none d-inline-block mt-2">Book Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card text-center h-100">
                            <div class="card-body">
                                <img src="{{asset('assets/img/icon-2.png')}}" alt="image">
                                <h5>Find A Pharmacy</h5>
                                <p>Browse through our listed pharmacies to get your ayurvedic medicines.</p>
                                <a href="#" class="text-decoration-none d-inline-block mt-2">Coming soon</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card text-center h-100">
                            <div class="card-body">
                                <img src="{{asset('assets/img/icon-3.png')}}" alt="image">
                                <h5>Find A Lab</h5>
                                <p>Get accurate tests done at the ease of your home.</p>
                                <a href="#" class="text-decoration-none d-inline-block mt-2">Coming soon</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container for-mob">
                <div class="row pt-3">
                    <div class="col-md-12 text-center">
                        <h2>What Are You Looking For?</h2>
                        <p>Ayurvedic treatment has proven the best result in healthcare and we are providing you every
                            service at home.</p>
                    </div>
                </div>

                <div id="testimonial_4"
                     class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x"
                     data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">

                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div class="testimonial4_slide mob-slider">

                                <div class="card text-center">
                                    <div class="card-body">
                                        <img src="{{asset('assets/img/icon-1.png')}}" alt="image">
                                        <h5>Visit A Doctor</h5>
                                        <p>Select preferred ayurvedic doctor and book an appointment.</p>
                                        <a href="http://localhost/ayurwayda/doctors-search"
                                           class="text-decoration-none d-inline-block mt-2">Book Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="testimonial4_slide mob-slider">
                                <div class="card text-center">
                                    <div class="card-body">
                                        <img src="{{asset('assets/img/icon-2.png')}}" alt="image">
                                        <h5>Find A Pharmacy</h5>
                                        <p>Browse through our listed pharmacies to get your ayurvedic medicines.</p>
                                        <a href="http://localhost/ayurwayda/doctors-search"
                                           class="text-decoration-none d-inline-block mt-2">Coming soon</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="testimonial4_slide mob-slider">

                                <div class="card text-center">
                                    <div class="card-body">
                                        <img src="{{asset('assets/img/icon-3.png')}}" alt="image">
                                        <h5>Find A Lab</h5>
                                        <p>Get accurate tests done at the ease of your home.</p>
                                        <a href="http://localhost/ayurwayda/doctors-search"
                                           class="text-decoration-none d-inline-block mt-2">Coming soon</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#testimonial_4" data-slide="prev">
                    <span class="carousel-control-prev-icon">
                      <img src="{{asset('assets/img/left-arrow.png')}}" alt="image">
                    </span>
                    </a>
                    <a class="carousel-control-next" href="#testimonial_4" data-slide="next">
                    <span class="carousel-control-next-icon">
                      <img src="{{asset('assets/img/right-arrow.png')}}" alt="image">
                    </span>
                    </a>
                </div>
            </div>

        </section>

        <img src="{{asset('assets/img/leaves.png')}}" alt="Image" class="leaves">
    </div>

    <section class="section section-specialities treatment py-5">
        <div class="container">
            <div class="section-header text-center mb-4">
                <h1>Treatments</h1>
                <p class="sub-title">Our doctors specialise in treating some specific illness through the practice of
                    Ayurveda.</p>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-12">

                    <div class="row">
                        @foreach($specializations  as $specialization)
                            <div class="col-3 mb-4">
                                <div class="speicality-item text-center">
                                    <div class="speicality-img pb-3"
                                         style="background-image: url('{{ \Avatar::create($specialization->specialization_name)->setShape('square')->toBase64() }}')"
                                    >
                                    </div>
                                    <div class="speicality-title">
                                        <h4>{{$specialization->specialization_name}}</h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>


                </div>
            </div>
        </div>
    </section>


    <!-- Popular Section -->
    <section class="section section-doctor">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="section-header ">
                        <h1>Take Consultation From Our Doctors </h1>

                    </div>
                    <div class="about-content">
                        <p>Choose from our team of top rated specialists for various ayurvedic treatments.</p>
                        <!-- <a href="javascript:;">Read More..</a> -->
                    </div>
                </div>
                <div class="col-lg-12 pt-4">
                    <div class="doctor-slider slider">
                        @foreach($doctors as $doctor)

                            @include('home.components.doctors-card')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Popular Section -->

    <section class="py-5 green-bg">
        <div class="container">
            <div class="row py-4">
                <div class="col-3">
                    <div class="d-flex">
                        <div>
                            <img src="{{asset('assets/img/gr-icon1.png')}}" alt="Image">
                        </div>
                        <div>
                            <h5 class="text-white py-3 ml-2">Cut Down On<br/> Travel Time</h5>
                        </div>
                    </div>

                </div>
                <div class="col-3">
                    <div class="d-flex">
                        <div>
                            <img src="{{asset('assets/img/gr-icon2.png')}}" alt="Image">
                        </div>
                        <div>
                            <h5 class="text-white py-3 ml-2">Zero Waiting <br/>Period</h5>
                        </div>
                    </div>

                </div>
                <div class="col-3">
                    <div class="d-flex">
                        <div>
                            <img src="{{asset('assets/img/gr-icon3.png')}}" alt="Image">
                        </div>
                        <div>
                            <h5 class="text-white py-3 ml-2">Certified<br/> Experts</h5>
                        </div>
                    </div>

                </div>
                <div class="col-3">
                    <div class="d-flex">
                        <div>
                            <img src="{{asset('assets/img/gr-icon4.png')}}" alt="Image">
                        </div>
                        <div>
                            <h5 class="text-white py-3 ml-2">24x7 Pharmacy<br/> Availability</h5>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- /blog Section starts-->
    <section class="section section-doctor for-web" style="position: relative;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="section-header ">
                        <h1>Read Articles From Health Experts</h1>
                        <p class="sub-title w-100">Some of the most recent articles on trending topics that help you
                            maintain a healthy Ayurvedic lifestyle all year round.</p>
                    </div>

                </div>
                <div class="col-lg-12 pt-4">
                    <div class="doctor-slider slider read-article">
                        @foreach ($blogs as $blog)
                        <div class="">
                            <!-- Blog Post -->
                            <div class="blog grid-blog">
                                <div class="blog-image">
                                    <a href="{{ route('blog.details', ['slug' => $blog->slug]) }}"><img
                                            class="img-fluid" src="{{ asset('assets/img/blog/blog-01.jpg') }}"
                                            alt="Post Image">
                                    </a>
                                </div>
                                <div class="blog-content">
                                    @forelse ($blog->categories as $category)
                                        <span class="blog-tag">{{ $category->name }}</span>
                                    @empty
                                    @endforelse
                                    <h3 class="blog-title">
                                        <a href="{{ route('blog.details', ['slug' => $blog->slug]) }}">{{$blog->title}}</a>
                                    </h3>
                                    <p class="mb-0">When we are not in good health, we face many problems and disappointments...</p>

                                    <ul class="entry-meta meta-item">
                                        <li>
                                            <div class="post-author">
                                                <a href="javascript:void(0);"><img
                                                        src="{{ get_profile_image($blog->user) }}"
                                                        alt="Post Author"> <span>{{ user_fullname($blog->user) }}</span></a>
                                            </div>
                                        </li>
                                        <li><i class="far fa-clock"></i>{{ \Carbon\Carbon::make($blog->created_at)->format('d M Y') }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="view-all text-center">
                <a href="{{ route('blog') }}" class="btn btn-primary">View All</a>
            </div>
        </div>
        <img src="{{asset('assets/img/leaves1.png')}}" alt="Image" class="leaves"
             style="bottom: 0;right: -54px;left: unset">
    </section>
    <!-- /blog Section ends-->


    <section class="section section-blogs for-mob">
        <div class="container">

            <!-- Section Header -->
            <div class="section-header text-center">
                <!-- <h1>Blogs and News</h1> -->
                <h1>Read Articles From Health Experts</h1>
                <p class="sub-title w-100">Some of the most recent articles on trending topics that help you maintain a
                    healthy Ayurvedic lifestyle all year round.</p>
            </div>
            <!-- /Section Header -->

            <div class="row justify-content-center">
                <div class="col-md-12">

                    <div class="doctor-slider slider">


                        <div class="">
                            <!-- Blog Post -->
                            <div class="blog grid-blog">
                                <div class="blog-image">
                                    <a href="http://localhost/ayurwayda/blog/How_to_improve_immunity_with_Ayurveda"><img
                                            class="img-fluid" src="uploads/post_image/308x206_1615571728.png"
                                            alt="Post Image"></a>
                                </div>
                                <div class="blog-content">
                                    <span class="blog-tag"></span>

                                    <h3 class="blog-title"><a
                                            href="http://localhost/ayurwayda/blog/How-to-improve-immunity-with-Ayurveda">How
                                            to improve immunity with Ayurveda</a></h3>


                                    <ul class="entry-meta meta-item">
                                        <li>
                                            <div class="post-author">
                                                <a href="javascript:void(0);"><img
                                                        src="http://localhost/ayurwayda/uploads/profileimage/1614927804.png"
                                                        alt="Post Author"> <span>Admin</span></a>
                                            </div>
                                        </li>


                                        <li><i class="far fa-clock"></i> 12 Mar 2021</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /BLog Post -->

                        </div>


                        <div class="">
                            <!-- Blog Post -->
                            <div class="blog grid-blog">
                                <div class="blog-image">
                                    <a href="http://localhost/ayurwayda/blog/Introduction_to_Pranayama"><img
                                            class="img-fluid" src="uploads/post_image/308x206_1615607791.png"
                                            alt="Post Image"></a>
                                </div>
                                <div class="blog-content">
                                    <span class="blog-tag"></span>

                                    <h3 class="blog-title"><a
                                            href="http://localhost/ayurwayda/blog/Introduction-to-Pranayama">Introduction
                                            to Pranayama</a></h3>


                                    <ul class="entry-meta meta-item">
                                        <li>
                                            <div class="post-author">
                                                <a href="javascript:void(0);"><img
                                                        src="http://localhost/ayurwayda/uploads/profileimage/1614927804.png"
                                                        alt="Post Author"> <span>Admin</span></a>
                                            </div>
                                        </li>


                                        <li><i class="far fa-clock"></i> 13 Mar 2021</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /BLog Post -->

                        </div>


                    </div>


                </div>
                <div class="view-all text-center col-md-12">
                    <a href="http://localhost/ayurwayda/blog" class="btn btn-primary">View All</a>
                </div>
            </div>
        </div>
    </section>
    <!-- /Blog Section -->


    <!--testimonial section-->

    <section class="testimonial text-center">
        <div class="container">

            <div class="heading">
                <h1>What They Believe</h1>
                <p>What our users have to say about their experience.</p>
            </div>
            <div id="testimonial4"
                 class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x"
                 data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">

                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="testimonial4_slide">

                            <p class="pb-3">Very good site. Well thought out about booking/rescheduling/canceling an
                                appointment. Also, Doctor’s feedback mechanism is good and describes all the basics in a
                                good way</p>
                            <div class="client_img">
                                <img src="https://i.ibb.co/8x9xK4H/team.jpg" class="img-circle img-responsive"/>
                                <h5 class="pt-3">Ankur Singh</h5>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">
                            <p class="pb-3">Very good site. Well thought out about booking/rescheduling/canceling an
                                appointment. Also, Doctor’s feedback mechanism is good and describes all the basics in a
                                good way</p>
                            <div class="client_img">
                                <img src="https://i.ibb.co/8x9xK4H/team.jpg" class="img-circle img-responsive"/>
                                <h5 class="pt-3">Ankur Singh</h5>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">

                            <p class="pb-3">Very good site. Well thought out about booking/rescheduling/canceling an
                                appointment. Also, Doctor’s feedback mechanism is good and describes all the basics in a
                                good way</p>
                            <div class="client_img">
                                <img src="https://i.ibb.co/8x9xK4H/team.jpg" class="img-circle img-responsive"/>
                                <h5 class="pt-3">Ankur Singh</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#testimonial4" data-slide="prev">
                    <span class="carousel-control-prev-icon">
                      <img src="assets/img/left-arrow.png" alt="image">
                    </span>
                </a>
                <a class="carousel-control-next" href="#testimonial4" data-slide="next">
                    <span class="carousel-control-next-icon">
                      <img src="assets/img/right-arrow.png" alt="image">
                    </span>
                </a>
            </div>
        </div>
    </section>


    <!--have doubts-->
    <section class="py-5 have_doubts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="assets/img/doubt.png" alt="image" class="img-fluid">
                </div>
                <div class="col-md-6">
                    <h1>Have Doubts? <br/>Here’s Your Answers</h1>

                    <div class="bs-example">
                        <div class="accordion" id="accordionExample">
                            @foreach($questions as $question)
                            <div class="card">
                                <div class="card-header" id="headingOne_{{$loop->index}}">
                                    <h2 class="mb-0">
                                        <button type="button" class="btn btn-link d-flex" data-toggle="collapse"
                                                data-target="#collapseOne_{{$loop->index}}">
                                            <span>{{ $loop->index + 1 }}</span>
                                            <i class="fa fa-plus"></i>
                                            <p class="text-left text-black">{{ $question->body }}</p>
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseOne_{{$loop->index}}" class="collapse" aria-labelledby="headingOne_{{$loop->index}}"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        @forelse($question->answers as $answer)
                                            @if(($answer->replier->role) == 'doctor')
                                            <p>{{$answer->body}} </p>
                                            @break
                                            @else
                                                <p> No Answer Found</p>
                                            @endif
                                        @empty
                                            <p> No Answer Found</p>
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="add_ques mt-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="view-all text-left">
                                    <a href="{{ route('user.get.login') }}">
                                        <button type="button" class="btn btn-primary mt-0">Login to Add your Question
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Main Wrapper -->

@endsection

@push('scripts')
    <script>
        $('#search_keywords')
    </script>
@endpush
