@extends('layout.mainlayout', ['profile' => 'patient'])
@section('content')
    <div id="account">
        <div class="row">
            <div class="col-lg-5 d-flex">
                <div class="card flex-fill">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <h3 class="card-title">Account</h3>
                            </div>
                            <div class="col-sm-6">
                                <div class="text-right">
                                    <a title="Edit Profile" class="btn btn-primary btn-sm"
                                       onclick="add_account_details()"><i class="fas fa-pencil"></i> Edit Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="profile-view-bottom">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="info-list">
                                        <div class="title">Bank Name</div>
                                        <div class="text" id="bank_name"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="info-list">
                                        <div class="title">Branch Name</div>
                                        <div class="text" id="branch_name"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="info-list">
                                        <div class="title">Account Number</div>
                                        <div class="text" id="account_no"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="info-list">
                                        <div class="title">Account Name</div>
                                        <div class="text" id="account_name"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 d-flex">
                <div class="card flex-fill">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="account-card bg-success-light">
                                    <span>₹0.00</span> Received
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="account-card bg-warning-light">
                                    <span>₹0.00</span> Requested
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="account-card bg-purple-light">
                                    <span>₹0.00</span> Balance
                                </div>
                            </div>

                            <div class="col-md-12 text-center">
                                <a href="javascript:void(0);" onclick="payment_request(2)"
                                   class="btn btn-primary request_btn">Refund Request</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="user-tabs">
            <ul class="nav nav-tabs nav-tabs-bottom nav-justified flex-wrap">
                <li class="nav-item">
                    <a class="nav-link active" onclick="paccount_table()" href="#account_tab"
                       data-toggle="tab">Accounts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="doctor_request()" href="#doctor_request_tab" data-toggle="tab"><span>Doctor Request</span></a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div id="account_tab" class="tab-pane fade show active">
                <div class="card card-table">
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="accounts_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="dataTables_length" id="accounts_table_length"><label>Show <select
                                                    name="accounts_table_length" aria-controls="accounts_table"
                                                    class="custom-select custom-select-sm form-control form-control-sm">
                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select> entries</label></div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div id="accounts_table_filter" class="dataTables_filter"><label>Search:<input
                                                    type="search" class="form-control form-control-sm" placeholder=""
                                                    aria-controls="accounts_table"></label></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="accounts_table"
                                               class="table table-hover table-center mb-0 dataTable no-footer"
                                               role="grid" aria-describedby="accounts_table_info"
                                               style="width: 1028px;">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_disabled" rowspan="1" colspan="1"
                                                    style="width: 132px;">Date
                                                </th>
                                                <th class="sorting_disabled" rowspan="1" colspan="1"
                                                    style="width: 259px;">Doctor Name
                                                </th>
                                                <th class="sorting_disabled" rowspan="1" colspan="1"
                                                    style="width: 169px;">Amount
                                                </th>
                                                <th class="sorting_disabled" rowspan="1" colspan="1"
                                                    style="width: 145px;">Status
                                                </th>
                                                <th class="sorting_disabled" rowspan="1" colspan="1"
                                                    style="width: 178px;">Actions
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <tr class="odd">
                                                <td valign="top" colspan="5" class="dataTables_empty">No data available
                                                    in table
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="accounts_table_processing" class="dataTables_processing card"
                                             style="display: none;">Processing...
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-5">
                                        <div class="dataTables_info" id="accounts_table_info" role="status"
                                             aria-live="polite">Showing 0 to 0 of 0 entries
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="dataTables_paginate paging_simple_numbers"
                                             id="accounts_table_paginate">
                                            <ul class="pagination">
                                                <li class="paginate_button page-item previous disabled"
                                                    id="accounts_table_previous"><a href="#"
                                                                                    aria-controls="accounts_table"
                                                                                    data-dt-idx="0" tabindex="0"
                                                                                    class="page-link">Previous</a></li>
                                                <li class="paginate_button page-item next disabled"
                                                    id="accounts_table_next"><a href="#" aria-controls="accounts_table"
                                                                                data-dt-idx="1" tabindex="0"
                                                                                class="page-link">Next</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="doctor_request_tab" class="tab-pane fade">
                <div class="card card-table">
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="doctor_request_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="dataTables_length" id="doctor_request_length"><label>Show <select
                                                    name="doctor_request_length" aria-controls="doctor_request"
                                                    class="custom-select custom-select-sm form-control form-control-sm">
                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select> entries</label></div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div id="doctor_request_filter" class="dataTables_filter"><label>Search:<input
                                                    type="search" class="form-control form-control-sm" placeholder=""
                                                    aria-controls="doctor_request"></label></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="doctor_request" style="width: 100%;"
                                               class="table table-hover table-center mb-0 dataTable no-footer"
                                               role="grid" aria-describedby="doctor_request_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_disabled" rowspan="1" colspan="1"
                                                    style="width: 0px;">Date
                                                </th>
                                                <th class="sorting_disabled" rowspan="1" colspan="1"
                                                    style="width: 0px;">Doctor Name
                                                </th>
                                                <th class="sorting_disabled" rowspan="1" colspan="1"
                                                    style="width: 0px;">Amount
                                                </th>
                                                <th class="sorting_disabled" rowspan="1" colspan="1"
                                                    style="width: 0px;">Status
                                                </th>
                                                <th class="sorting_disabled" rowspan="1" colspan="1"
                                                    style="width: 0px;">Actions
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <tr class="odd">
                                                <td valign="top" colspan="5" class="dataTables_empty">No data available
                                                    in table
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div id="doctor_request_processing" class="dataTables_processing card"
                                             style="display: none;">Processing...
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-5">
                                        <div class="dataTables_info" id="doctor_request_info" role="status"
                                             aria-live="polite">Showing 0 to 0 of 0 entries
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="dataTables_paginate paging_simple_numbers"
                                             id="doctor_request_paginate">
                                            <ul class="pagination">
                                                <li class="paginate_button page-item previous disabled"
                                                    id="doctor_request_previous"><a href="#"
                                                                                    aria-controls="doctor_request"
                                                                                    data-dt-idx="0" tabindex="0"
                                                                                    class="page-link">Previous</a></li>
                                                <li class="paginate_button page-item next disabled"
                                                    id="doctor_request_next"><a href="#" aria-controls="doctor_request"
                                                                                data-dt-idx="1" tabindex="0"
                                                                                class="page-link">Next</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
