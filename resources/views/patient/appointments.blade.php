@extends('layout.mainlayout', ['profile' => 'patient'])
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="appointments">
                <div class="row">
            @forelse($unique_doctors as $doctor)
                <!-- Appointment List -->
                <div class="col-md-4">
                    <div class="profile-widget">
                        <div class="card-body">
                            <a href="patient-profile" class="booking-doc-img">
                                <img src="{{ get_profile_image($doctor) }}"
                                     class="w-100"
                                     alt="User Image">
                            </a>
                            <div class="profile-det-info">
                                <h3>
                                    <a href="patient-profile">{{ user_fullname($doctor) }}</a>
                                </h3>
                                <div class="patient-details">
                                    <h5>
                                    <h5><i class="fas fa-map-marker-alt"></i> Newyork, United States</h5>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                @empty
                @endforelse
                </div>

            </div>
        </div>

    </div>
    <!-- /Page Content -->
    </div>
@endsection
