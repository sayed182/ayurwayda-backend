@extends('layout.mainlayout', ['profile' => 'patient'])
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card card-table mb-0">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-center mb-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Order Id</th>
                                <th>Doctor Name</th>
                                <th class="text-center">Amount</th>
                                <th>Payment Gateway</th>
                                <th>Status</th>
                                <th>Order date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($invoices as $invoice)
                            <tr>
                                <td>{{ $loop->index }}</td>
                                <td><a href="#">{{ $invoice->invoice_no }}</a></td>
                                <td> {{ user_fullname($invoice->appointment->doctor) }} </td>
                                <td class="text-center">{{ get_formatted_amount($invoice->total_amount) }}</td>
                                <td>{{ $invoice->payment_merchant }}</td>
                                <td><span class="badge {{$invoice->transaction_status == 'success'?"badge-primary":"badge-danger"}}">Order {{ ucfirst($invoice->transaction_status) }}</span></td>
                                <td>{{ get_formatted_date($invoice->created_at) }} <span class="d-block text-info">{{ get_formatted_time($invoice->created_at) }}</span></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
