@extends('layout.mainlayout', ['profile' => 'patient'])
@section('content')

    <div class="card card-table mb-0">
        <div class="card-header">

            <h3 class="p-3">Questions &amp; Answers</h3>



            <a href="javascript:void(0);" onclick="add_question()" class="btn btn-primary float-right mt-2">+ Add </a>
        </div>
        <div class="card-body">



            <div class="table-responsive">
                <div id="my_ques_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="my_ques_length"><label>Show <select name="my_ques_length" aria-controls="my_ques" class="custom-select custom-select-sm form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-12 col-md-6"><div id="my_ques_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="my_ques"></label></div></div></div><div class="row"><div class="col-sm-12"><table id="my_ques" class="table table-hover table-center dataTable mb-0 no-footer" role="grid" aria-describedby="my_ques_info">
                                <thead>
                                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="my_ques" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 49.5781px;">#</th><th class="sorting" tabindex="0" aria-controls="my_ques" rowspan="1" colspan="1" aria-label="Date: activate to sort column ascending" style="width: 193.906px;">Date</th><th class="sorting" tabindex="0" aria-controls="my_ques" rowspan="1" colspan="1" aria-label="Questions: activate to sort column ascending" style="width: 345.156px;">Questions</th><th class="sorting" tabindex="0" aria-controls="my_ques" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending" style="width: 316.859px;">Action</th></tr>
                                </thead>
                                <tbody>
                                <tr role="row" class="odd">
                                    <td class="sorting_1">1</td>
                                    <td>22 Jan 2021</td>
                                    <td><h4>How to lose weight?</h4></td>
                                    <td>
                                        <a href="http://localhost/ayurwayda/qa/view_answers/5" class="btn btn-sm bg-info-light"><i class="far fa-eye"></i> View </a>
                                        <a href="javascript:void(0);" onclick="delete_question('5')" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i> Delete </a>
                                    </td>
                                </tr><tr role="row" class="even">
                                    <td class="sorting_1">2</td>
                                    <td>14 Aug 2020</td>
                                    <td><h4>Test from api</h4></td>
                                    <td>
                                        <a href="http://localhost/ayurwayda/qa/view_answers/4" class="btn btn-sm bg-info-light"><i class="far fa-eye"></i> View </a>
                                        <a href="javascript:void(0);" onclick="delete_question('4')" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i> Delete </a>
                                    </td>
                                </tr><tr role="row" class="odd">
                                    <td class="sorting_1">3</td>
                                    <td>14 Aug 2020</td>
                                    <td><h4>Test from api</h4></td>
                                    <td>
                                        <a href="http://localhost/ayurwayda/qa/view_answers/3" class="btn btn-sm bg-info-light"><i class="far fa-eye"></i> View </a>
                                        <a href="javascript:void(0);" onclick="delete_question('3')" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i> Delete </a>
                                    </td>
                                </tr><tr role="row" class="even">
                                    <td class="sorting_1">4</td>
                                    <td>25 Jul 2020</td>
                                    <td><h4>Stomach pain</h4></td>
                                    <td>
                                        <a href="http://localhost/ayurwayda/qa/view_answers/1" class="btn btn-sm bg-info-light"><i class="far fa-eye"></i> View </a>
                                        <a href="javascript:void(0);" onclick="delete_question('1')" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i> Delete </a>
                                    </td>
                                </tr><tr role="row" class="odd">
                                    <td class="sorting_1">5</td>
                                    <td>25 Jul 2020</td>
                                    <td><h4>Teeth pain</h4></td>
                                    <td>
                                        <a href="http://localhost/ayurwayda/qa/view_answers/2" class="btn btn-sm bg-info-light"><i class="far fa-eye"></i> View </a>
                                        <a href="javascript:void(0);" onclick="delete_question('2')" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i> Delete </a>
                                    </td>
                                </tr></tbody>
                            </table></div></div><div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="my_ques_info" role="status" aria-live="polite">Showing 1 to 5 of 5 entries</div></div><div class="col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="my_ques_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="my_ques_previous"><a href="#" aria-controls="my_ques" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="my_ques" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item next disabled" id="my_ques_next"><a href="#" aria-controls="my_ques" data-dt-idx="2" tabindex="0" class="page-link">Next</a></li></ul></div></div></div></div>
            </div>
        </div>
    </div>
@endsection
@include('patient.qa.add_qa_modal')

@push('scripts')
    <script>
        function add_question()
        {
            $('[name="method"]').val('insert');
            $('#question_form')[0].reset(); // reset form on modals
            $('#question_modal_form').modal('show'); // show bootstrap modal
            $('.modal-title').text('Add new question'); // Set Title to Bootstrap modal title
        }
    </script>
@endpush()
