@extends('layout.mainlayout', ['profile' => 'patient'])
@section('content')
    <div class="card">
        <div class="card-body pt-0">

            <!-- Tab Menu -->
            <nav class="user-tabs mb-4">
                <ul class="nav nav-tabs nav-tabs-bottom nav-justified">
                    <li class="nav-item">
                        <a class="nav-link active" onclick="appoinments_table()" href="#pat_appointments"
                           data-toggle="tab">Appointments</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" onclick="prescriptions_table()" href="#pres" data-toggle="tab"><span>Prescription</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" onclick="medical_records_table()" href="#medical" data-toggle="tab"><span
                                class="med-records">Medical Records</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" onclick="billings_table()" href="#billing"
                           data-toggle="tab"><span>Billing</span></a>
                    </li>
                </ul>
            </nav>
            <!-- /Tab Menu -->

            <!-- Tab Content -->
            <div class="tab-content pt-0">

                <input type="hidden" id="patient_id" value="1">
                <!-- Appointment Tab -->
                <div id="pat_appointments" class="tab-pane fade show active">
                    <div class="card card-table mb-0">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div id="appoinment_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="dataTables_length" id="appoinment_table_length"><label>Show
                                                    <select name="appoinment_table_length"
                                                            aria-controls="appoinment_table"
                                                            class="custom-select custom-select-sm form-control form-control-sm">
                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select> entries</label></div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div id="appoinment_table_filter" class="dataTables_filter">
                                                <label>Search:<input type="search" class="form-control form-control-sm"
                                                                     placeholder=""
                                                                     aria-controls="appoinment_table"></label></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table id="appoinment_table"
                                                   class="table table-hover table-center mb-0 dataTable no-footer"
                                                   role="grid" aria-describedby="appoinment_table_info"
                                                   style="width: 774px;">
                                                <thead>
                                                <tr role="row">
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 246px;">Doctor
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 142px;">Appt Date
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 172px;">Booking Date
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 94px;">Type
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @forelse($appointments as $appointment)
                                                <tr role="row" class="odd">
                                                    <td><h2 class="table-avatar">
                                                            <a href="{{ get_profile_image($appointment->doctor) }}"
                                                               class="avatar avatar-sm mr-2">
                                                                <img class="avatar-img rounded-circle"
                                                                     src="{{ get_profile_image($appointment->doctor) }}"
                                                                     alt="User Image">
                                                            </a>
                                                            <a href="http://localhost/ayurwayda/doctor-preview/doctord180">{{ user_fullname($appointment->doctor) }} <span>{{ $appointment->specialization->specialization_name??'No Specialization' }}</span></a>
                                                        </h2>
                                                    </td>
                                                    <td>{{ get_formatted_date($appointment->appointment_date) }} <span class="d-block text-info">{{ get_formatted_time($appointment->time_slot->start_time) }}</span></td>
                                                    <td>{{ get_formatted_date($appointment->created_at) }}</td>
                                                    <td style="text-transform:capitalize;">{{ $appointment->type }}</td>
                                                </tr>
                                                @empty
                                                    Np appointments
                                                @endforelse

                                                </tbody>
                                            </table>
                                            <div id="appoinment_table_processing" class="dataTables_processing card"
                                                 style="display: none;">Processing...
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info" id="appoinment_table_info" role="status"
                                                 aria-live="polite">Showing 1 to 10 of 20 entries (filtered from 25
                                                total entries)
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers"
                                                 id="appoinment_table_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous disabled"
                                                        id="appoinment_table_previous"><a href="#"
                                                                                          aria-controls="appoinment_table"
                                                                                          data-dt-idx="0" tabindex="0"
                                                                                          class="page-link">Previous</a>
                                                    </li>
                                                    <li class="paginate_button page-item active"><a href="#"
                                                                                                    aria-controls="appoinment_table"
                                                                                                    data-dt-idx="1"
                                                                                                    tabindex="0"
                                                                                                    class="page-link">1</a>
                                                    </li>
                                                    <li class="paginate_button page-item "><a href="#"
                                                                                              aria-controls="appoinment_table"
                                                                                              data-dt-idx="2"
                                                                                              tabindex="0"
                                                                                              class="page-link">2</a>
                                                    </li>
                                                    <li class="paginate_button page-item next"
                                                        id="appoinment_table_next"><a href="#"
                                                                                      aria-controls="appoinment_table"
                                                                                      data-dt-idx="3" tabindex="0"
                                                                                      class="page-link">Next</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Appointment Tab -->

                <!-- Prescription Tab -->
                <div class="tab-pane fade" id="pres">
                    <div class="card card-table mb-0">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div id="prescription_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="dataTables_length" id="prescription_table_length"><label>Show
                                                    <select name="prescription_table_length"
                                                            aria-controls="prescription_table"
                                                            class="custom-select custom-select-sm form-control form-control-sm">
                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select> entries</label></div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div id="prescription_table_filter" class="dataTables_filter"><label>Search:<input
                                                        type="search" class="form-control form-control-sm"
                                                        placeholder="" aria-controls="prescription_table"></label></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table id="prescription_table" style="width: 100%;"
                                                   class="table table-hover table-center mb-0 dataTable no-footer"
                                                   role="grid" aria-describedby="prescription_table_info">
                                                <thead>
                                                <tr role="row">
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">#
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">Date
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">Name
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">Doctor
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">View
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <tr class="odd">
                                                    <td valign="top" colspan="5" class="dataTables_empty">No data
                                                        available in table
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div id="prescription_table_processing" class="dataTables_processing card"
                                                 style="display: none;">Processing...
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info" id="prescription_table_info" role="status"
                                                 aria-live="polite">Showing 0 to 0 of 0 entries
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers"
                                                 id="prescription_table_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous disabled"
                                                        id="prescription_table_previous"><a href="#"
                                                                                            aria-controls="prescription_table"
                                                                                            data-dt-idx="0" tabindex="0"
                                                                                            class="page-link">Previous</a>
                                                    </li>
                                                    <li class="paginate_button page-item next disabled"
                                                        id="prescription_table_next"><a href="#"
                                                                                        aria-controls="prescription_table"
                                                                                        data-dt-idx="1" tabindex="0"
                                                                                        class="page-link">Next</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Prescription Tab -->

                <!-- Medical Records Tab -->
                <div class="tab-pane fade" id="medical">
                    <div class="card card-table mb-0">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div id="medical_records_table_wrapper"
                                     class="dataTables_wrapper dt-bootstrap4 no-footer">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="dataTables_length" id="medical_records_table_length"><label>Show
                                                    <select name="medical_records_table_length"
                                                            aria-controls="medical_records_table"
                                                            class="custom-select custom-select-sm form-control form-control-sm">
                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select> entries</label></div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div id="medical_records_table_filter" class="dataTables_filter"><label>Search:<input
                                                        type="search" class="form-control form-control-sm"
                                                        placeholder="" aria-controls="medical_records_table"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table id="medical_records_table"
                                                   class="table table-hover table-center mb-0 dataTable no-footer"
                                                   style="width: 100%;" role="grid"
                                                   aria-describedby="medical_records_table_info">
                                                <thead>
                                                <tr role="row">
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">#
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">Date
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">Description
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">Attachment
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">Doctor
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr class="odd">
                                                    <td valign="top" colspan="5" class="dataTables_empty">No data
                                                        available in table
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div id="medical_records_table_processing"
                                                 class="dataTables_processing card" style="display: none;">Processing...
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info" id="medical_records_table_info" role="status"
                                                 aria-live="polite">Showing 0 to 0 of 0 entries
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers"
                                                 id="medical_records_table_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous disabled"
                                                        id="medical_records_table_previous"><a href="#"
                                                                                               aria-controls="medical_records_table"
                                                                                               data-dt-idx="0"
                                                                                               tabindex="0"
                                                                                               class="page-link">Previous</a>
                                                    </li>
                                                    <li class="paginate_button page-item next disabled"
                                                        id="medical_records_table_next"><a href="#"
                                                                                           aria-controls="medical_records_table"
                                                                                           data-dt-idx="1" tabindex="0"
                                                                                           class="page-link">Next</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Medical Records Tab -->

                <!-- Billing Tab -->
                <div class="tab-pane" id="billing">
                    <div class="card card-table mb-0">
                        <div class="card-body">
                            <div class="table-responsive">

                                <div id="billing_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="dataTables_length" id="billing_table_length"><label>Show <select
                                                        name="billing_table_length" aria-controls="billing_table"
                                                        class="custom-select custom-select-sm form-control form-control-sm">
                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select> entries</label></div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div id="billing_table_filter" class="dataTables_filter">
                                                <label>Search:<input type="search" class="form-control form-control-sm"
                                                                     placeholder=""
                                                                     aria-controls="billing_table"></label></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table id="billing_table"
                                                   class="table table-hover table-center mb-0 dataTable no-footer"
                                                   style="width: 100%;" role="grid"
                                                   aria-describedby="billing_table_info">
                                                <thead>
                                                <tr role="row">
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">#
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">Date
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">Description
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">Doctor
                                                    </th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1"
                                                        style="width: 0px;">View
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <tr class="odd">
                                                    <td valign="top" colspan="5" class="dataTables_empty">No data
                                                        available in table
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div id="billing_table_processing" class="dataTables_processing card"
                                                 style="display: none;">Processing...
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info" id="billing_table_info" role="status"
                                                 aria-live="polite">Showing 0 to 0 of 0 entries
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers"
                                                 id="billing_table_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous disabled"
                                                        id="billing_table_previous"><a href="#"
                                                                                       aria-controls="billing_table"
                                                                                       data-dt-idx="0" tabindex="0"
                                                                                       class="page-link">Previous</a>
                                                    </li>
                                                    <li class="paginate_button page-item next disabled"
                                                        id="billing_table_next"><a href="#"
                                                                                   aria-controls="billing_table"
                                                                                   data-dt-idx="1" tabindex="0"
                                                                                   class="page-link">Next</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Billing Tab -->

            </div>
            <!-- Tab Content -->
        </div>
    </div>
@endsection

