@extends('layout.mainlayout', ['profile' => 'patient'])
@section('content')
    <div class="card">
        <div class="card-body">
            <div id="calendar" class="fc fc-unthemed fc-ltr">
            </div>
        </div>
    </div>
@endsection
