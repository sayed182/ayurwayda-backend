<?php $page="register1";?>
@extends('layout.mainlayout_withoutsidebar')
@section('content')
    <div class="content" style="min-height: 461.312px;">
        <div class="container">

            <div class="row">
                <div class="col-md-12">

                    <!-- Login Tab Content -->
                    <div class="account-content pb-5">
                        <div class="row align-items-center justify-content-center">
                            <div class="col-md-7 col-lg-6 login-left">
                                <!-- <img src="http://localhost/ayurwayda/uploads/login_image/1589344288_1060_663.jpg" class="img-fluid" alt="Doccure Login">  -->
                                <img src="assets/img/log_img.png" alt="image" class="w-100">

                            </div>
                            <div class="col-md-12 col-lg-6 login-right">
                                <ul class="nav nav-tabs select_log" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link " href="{{ route('user.get.login') }}">Login</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#register">Register</a>
                                    </li>

                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content add_ques">
                                    <div id="login" class="container tab-pane "><br>
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="#" id="signin_form" method="post" autocomplete="off" novalidate="novalidate">
													@csrf
                                                    <input type="hidden" id="country_id" value="+91">
                                                    <div id="username_div">
                                                        <div class="form-group form-focus focused">
                                                            <label class="round-input-container w-100">
                                                                <div class="round-input-decorator">
                                                                    <div class="round-input-border-left"></div>
                                                                    <span class="round-input-label-text">Email or Mobile No</span>
                                                                    <div class="round-input-border-right"></div>
                                                                </div>

                                                                <input type="text" name="email" id="email" class="form-control floating">
                                                            </label>
                                                        </div>

                                                        <div class="form-group form-focus focused">
                                                            <label class="round-input-container w-100">
                                                                <div class="round-input-decorator">
                                                                    <div class="round-input-border-left"></div>
                                                                    <span class="round-input-label-text">Password</span>
                                                                    <div class="round-input-border-right"></div>
                                                                </div>
                                                                <input type="password" name="password" id="password" class="form-control floating">
                                                            </label>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="otp">
                                                                    <p>
                                                                        <input type="radio" id="remember" name="remember" value="1">
                                                                        <label for="remember">Remember me for 30 days</label>
                                                                    </p>
                                                                    <p>
                                                                        <input type="radio" id="login_otp" name="radio-group">
                                                                        <label for="login_otp">Login with OTP instead of password</label>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="text-right">
                                                                    <a class="forgot-link" style="color: #16bef0" href="http://localhost/ayurwayda/forgot-password">Forgot Password?</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="otp_div" style="display: none">
                                                        <div class="form-group form-focus">
                                                            <label class="round-input-container w-100 mt-0">
                                                                <div class="round-input-decorator">
                                                                    <div class="round-input-border-left"></div>
                                                                    <span class="round-input-label-text">Mobile Number</span>
                                                                    <div class="round-input-border-right"></div>
                                                                </div>
                                                                <input type="text" name="mobileno" id="mobileno" class="form-control floating">
                                                            </label>
                                                        </div>
                                                        <div class="text-right otp_load">
                                                            <a class="forgot-link" href="javascript:void(0);" id="sendotp_login">Send OTP</a>
                                                        </div>
                                                        <div class="form-group form-focus OTP" style="display: none;">
                                                            <label class="round-input-container w-100">
                                                                <div class="round-input-decorator">
                                                                    <div class="round-input-border-left"></div>
                                                                    <span class="round-input-label-text">OTP</span>
                                                                    <div class="round-input-border-right"></div>
                                                                </div>

                                                                <input type="text" name="otpno" id="otpno" class="form-control floating">
                                                            </label>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="otp">
                                                                    <p>
                                                                        <input type="radio" id="remember1" name="radio-group">
                                                                        <label for="remember1">Remember me for 30 days</label>
                                                                    </p>
                                                                    <p>
                                                                        <input type="radio" id="login_username" name="radio-group">
                                                                        <label for="login_username">Login with Password instead of OTP</label>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="text-right">
                                                                    <a class="forgot-link" style="color: #16bef0" href="http://localhost/ayurwayda/forgot-password">Forgot Password?</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="view-all text-left">
                                                        <button type="submit" class="btn btn-primary mt-0">Login</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="register" class="container tab-pane  active"><br>
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row pb-4">
                                                    <div class="col-md-5">
                                                        <h5 class="get_start" style="font-weight: 900">Get Started by Join <span>Ayurwayda</span></h5>
                                                    </div>
                                                    <div class="col-md-7 text-right"><b>Are you a <span id="role_types">Doctor</span>?<a onclick="change_role()" href="javascript:void(0);"><span style="color: #3dac7b"> Register Here</span></a></b><p></p>
                                                    </div>
                                                </div>


                                                <div class="login-header">
                                                    <h3 style="font-weight: 700"><span id="role_type">Patient</span> Register </h3>
                                                </div>

                                                <!-- Register Form -->
                                                <form method="post" id="register_form" autocomplete="off" action="#" enctype="multipart/form-data" novalidate="novalidate">
                                                    <input type="hidden" id="role" name="role" value="2">
                                                    <div class="form-group form-focus">
                                                        <label class="round-input-container w-100">
                                                            <div class="round-input-decorator">
                                                                <div class="round-input-border-left"></div>
                                                                <span class="round-input-label-text">Full Name</span>
                                                                <div class="round-input-border-right"></div>
                                                            </div>
                                                            <input type="text" name="first_name" id="first_name" class="form-control floating">
                                                        </label>
                                                    </div>
                                                    <div class="form-group form-focus focused">
                                                        <label class="round-input-container w-100">
                                                            <div class="round-input-decorator">
                                                                <div class="round-input-border-left"></div>
                                                                <span class="round-input-label-text">Email</span>
                                                                <div class="round-input-border-right"></div>
                                                            </div>
                                                            <input type="email" name="email" id="email_reg" class="form-control floating error" aria-invalid="true"><label id="email_reg-error" class="error" for="email_reg">Please enter valid email address</label>
                                                        </label>
                                                    </div>

                                                    <div class="row form-group form-focus">
                                                        <div class="col-md-6">
                                                            <select name="country_code" class="form-control mob-cd" id="country_code" style="margin-top: 15px;">
                                                                <option value="91">India(+91)</option></select>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group form-focus w-100">
                                                                <label class="round-input-container w-100 mt-0">
                                                                    <div class="round-input-decorator">
                                                                        <div class="round-input-border-left"></div>
                                                                        <span class="round-input-label-text">Mobile Number</span>
                                                                        <div class="round-input-border-right"></div>
                                                                    </div>
                                                                    <input type="text" name="mobileno" id="mobileno_reg" class="form-control floating">
                                                                </label>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="text-right otp_load">
                                                        <a class="btn btn-primary btn-sm forgot-link" style="color:white" href="javascript:void(0);" id="sendotp">Send OTP</a>
                                                    </div>
                                                    <div class="form-group form-focus OTP" style="display: none;">
                                                        <label class="round-input-container w-100">
                                                            <div class="round-input-decorator">
                                                                <div class="round-input-border-left"></div>
                                                                <span class="round-input-label-text">OTP</span>
                                                                <div class="round-input-border-right"></div>
                                                            </div>
                                                            <input type="text" name="otpno" id="otpno" class="form-control floating">
                                                        </label>
                                                    </div>
                                                    <div class="form-group form-focus focused">
                                                        <label class="round-input-container w-100">
                                                            <div class="round-input-decorator">
                                                                <div class="round-input-border-left"></div>
                                                                <span class="round-input-label-text">Create Password</span>
                                                                <div class="round-input-border-right"></div>
                                                            </div>
                                                            <input type="password" name="password" id="password" class="form-control floating valid" aria-invalid="false">
                                                        </label>
                                                    </div>

                                                    <div class="document-info" style="margin-bottom: 15px;display: none;">
                                                        <div class="row document-cont">
                                                            <div class="col-md-9">
                                                                <label>Document Name</label>
                                                                <input type="text" name="doc_name[]" class="form-control">
                                                            </div>
                                                            <div class="col-md-9">
                                                                <label>Document</label>
                                                                <input type="file" name="doc_files[]" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="add-more" style="margin-bottom: 15px;display: none;">
                                                        <a href="javascript:void(0);" class="add-document"><i class="fa fa-plus-circle"></i>Add More </a>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="otp">
                                                                <p>
                                                                    <input type="radio" id="test1" name="radio-group" checked="">
                                                                    <label for="test1" style="font-weight: 700">Receive relevant ofers and promotional communication from ayurwayda<br><br>By signing up, I agree to <a href="#" style="color: #3dac7b">terms</a></label>
                                                                </p>


                                                            </div>
                                                        </div>

                                                    </div>
                                                    <button class="btn btn-primary btn-block btn-lg login-btn" id="register_btn" type="submit">Signup </button>

                                                </form>
                                                <!-- /Register Form -->

                                            </div>
                                        </div>
                                    </div>

                                </div>




                            </div>
                        </div>
                    </div>
                    <!-- /Login Tab Content -->

                </div>
            </div>

        </div>
    </div>
@endsection
