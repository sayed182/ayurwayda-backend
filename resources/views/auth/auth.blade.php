<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>
<body>
<form action="{{Route('user.post.login')}}" method="post">

    @csrf
    <div class="form-group">
        <input class="form-control" type="text" placeholder="Email" name="email">
    </div>
    <div class="form-group">
        <input class="form-control" type="password" placeholder="Password" name="password">
    </div>
    <div class="form-group">
        <button class="btn btn-primary btn-block" type="submit">Login</button>
    </div>
</form>
</body>
</html>
