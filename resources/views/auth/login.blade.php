<?php $page = "login"; ?>
@extends('layout.mainlayout_withoutsidebar')
@section('content')
    <!-- Page Content -->
    <div class="content" style="min-height: 460.891px;">
        <div class="container">

            <div class="row">
                <div class="col-md-12">

                    <!-- Login Tab Content -->
                    <div class="account-content pb-5">
                        <div class="row align-items-center justify-content-center">
                            <div class="col-md-7 col-lg-6 login-left">
                                <!-- <img src="http://localhost/ayurwayda/uploads/login_image/1589344288_1060_663.jpg" class="img-fluid" alt="Doccure Login">  -->
                                <img src="assets/img/log_img.png" alt="image" class="w-100">

                            </div>
                            <div class="col-md-12 col-lg-6 login-right">


                                <!--  <div class="login-header">


                                   <h3>Login <span>AyurWayda | Book & Consult Ayurvedic  Doctors Online, Order Medicines</span></h3>
                                 </div> -->

                                <!-- <form action="#" id="signin_form" method="post" autocomplete="off">
                                  <div class="form-group form-focus">
                                    <input type="text" name="email" id="email" class="form-control floating">
                                    <label class="focus-label">Email or Mobile No</label>
                                  </div>
                                  <div class="form-group form-focus">
                                    <input type="password" name="password" id="password" class="form-control floating">
                                    <label class="focus-label">Password</label>
                                  </div>
                                  <div class="text-right">
                                    <a class="forgot-link" href="http://localhost/ayurwayda/forgot-password">Forgot Password?</a>
                                  </div>
                                  <button class="btn btn-primary btn-block btn-lg login-btn" id="signin_btn" type="submit">Signin</button>


                                  <div class="text-center dont-have">Don’t have an account? <a href="http://localhost/ayurwayda/register">Register</a></div>
                                </form> -->


                                <ul class="nav nav-tabs select_log" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#login">Login</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('user.get.register') }}">Register</a>
                                    </li>

                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content add_ques">
                                    <div id="login" class="container tab-pane active"><br>
                                        <div class="card">
                                            <div class="card-body">
                                                <form action="{{ route('user.post.login') }}" id="signin_form" method="post" autocomplete="off"
                                                      novalidate="novalidate">
                                                    @csrf
                                                    <input type="hidden" id="country_id" value="+91">
                                                    <div id="username_div">
                                                        <div class="form-group form-focus">
                                                            <label class="round-input-container w-100">
                                                                <div class="round-input-decorator">
                                                                    <div class="round-input-border-left"></div>
                                                                    <span class="round-input-label-text">Email or Mobile No</span>
                                                                    <div class="round-input-border-right"></div>
                                                                </div>

                                                                <input type="text" name="email" id="email"
                                                                       class="form-control floating">
                                                            </label>
                                                        </div>

                                                        <div class="form-group form-focus">
                                                            <label class="round-input-container w-100">
                                                                <div class="round-input-decorator">
                                                                    <div class="round-input-border-left"></div>
                                                                    <span class="round-input-label-text">Password</span>
                                                                    <div class="round-input-border-right"></div>
                                                                </div>
                                                                <input type="password" name="password" id="password"
                                                                       class="form-control floating">
                                                            </label>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="otp">
                                                                    <p>
                                                                        <input type="radio" id="remember"
                                                                               name="remember" value="1">
                                                                        <label for="remember">Remember me for 30
                                                                            days</label>
                                                                    </p>
                                                                    <p>
                                                                        <input type="radio" id="login_otp"
                                                                               name="radio-group">
                                                                        <label for="login_otp">Login with OTP instead of
                                                                            password</label>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="text-right">
                                                                    <a class="forgot-link" style="color: #16bef0"
                                                                       href="http://localhost/ayurwayda/forgot-password">Forgot
                                                                        Password?</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="otp_div" style="display: none">
                                                        <div class="form-group form-focus">
                                                            <label class="round-input-container w-100">
                                                                <div class="round-input-decorator">
                                                                    <div class="round-input-border-left"></div>
                                                                    <span
                                                                        class="round-input-label-text">Mobile Number</span>
                                                                    <div class="round-input-border-right"></div>
                                                                </div>
                                                                <input type="text" name="mobileno" id="mobileno"
                                                                       class="form-control floating">
                                                            </label>
                                                        </div>
                                                        <div class="text-right otp_load">
                                                            <a class="forgot-link" href="javascript:void(0);"
                                                               id="sendotp_login">Send OTP</a>
                                                        </div>
                                                        <div class="form-group form-focus OTP" style="display: none;">
                                                            <label class="round-input-container w-100">
                                                                <div class="round-input-decorator">
                                                                    <div class="round-input-border-left"></div>
                                                                    <span class="round-input-label-text">OTP</span>
                                                                    <div class="round-input-border-right"></div>
                                                                </div>

                                                                <input type="text" name="otpno" id="otpno"
                                                                       class="form-control floating">
                                                            </label>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="otp">
                                                                    <p>
                                                                        <input type="radio" id="remember1"
                                                                               name="radio-group">
                                                                        <label for="remember1">Remember me for 30
                                                                            days</label>
                                                                    </p>
                                                                    <p>
                                                                        <input type="radio" id="login_username"
                                                                               name="radio-group">
                                                                        <label for="login_username">Login with Password
                                                                            instead of OTP</label>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="text-right">
                                                                    <a class="forgot-link" style="color: #16bef0"
                                                                       href="http://localhost/ayurwayda/forgot-password">Forgot
                                                                        Password?</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="view-all text-left">
                                                        <button type="submit" class="btn btn-primary mt-0">Login
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                </div>


                            </div>
                        </div>
                    </div>
                    <!-- /Login Tab Content -->

                </div>
            </div>

        </div>

    </div>
@endsection
