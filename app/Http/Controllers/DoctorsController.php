<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Blog;
use App\Models\BusinessHour;
use App\Models\Country;
use App\Models\DoctorsDetail;
use App\Models\State;
use App\Models\User;
use App\Models\UserDetails;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use function PHPUnit\Framework\throwException;

class DoctorsController extends Controller
{
    public function index()
    {
        $total_appointments = Appointment::where('doctor_id', auth()->id())->count();
        $last_appointment_date = Appointment::where('doctor_id', auth()->id())->orderByDesc('created_at')->first()->appointment_date;
        $appointments = Appointment::with(['patient', 'payments'])->where('doctor_id', auth()->id())->get();
        return view('doctor.dashboard', [
            'total_appointment' => $total_appointments,
            'last_appointment_date' => Carbon::createFromDate($last_appointment_date)->format('d, M y'),
            'appointments' => $appointments,
        ]);
    }

    public function appointment()
    {
        $appointments = Appointment::with(['patient', 'payments', 'time_slot'])->where('doctor_id', auth()->id())->get();
        return view('doctor.appointments', compact('appointments'));
    }

    public function profileSettings(Request $request)
    {
        $countries = Country::all();
        $states = State::all();
        $doctorProfile = User::with(['doctorDetails', 'userDetails', 'businessHours'])->find(auth()->id());
        return view('doctor.profile-settings',
                    ['profile' => $doctorProfile, 'countries' => $countries, 'states' => $states]);
    }

    public function storeProfileSettings(Request $request)
    {
        \DB::transaction(function() use($request) {
            $gender = $request->input('gender') ?? null;
            $dob = $request->input('dob') ?? null;
            $biography = $request->input('biography');
            $clinic_name = $request->input('clinic_name');
            $primary_address = $request->input('primary_address');
            $secondary_address = $request->input('secondary_address');
            $country_id = $request->input('country_id');
            $state_id = $request->input('state_id');
            $city_name = $request->input('city_name');
            $postcode = $request->input('post_code');
            $education = $request->input('education') ?? null;
            $experience = $request->input('experience') ?? null;
            $awards = $request->input('award') ?? null;
            $memberships = $request->input('membership') ?? null;
            $documents = $request->input('documents') ?? null;
            $avaibility = $request->input('availability') ?? null;
            $user = auth()->user();
            $userUpdate = (new User)->update(['id' => $user->id], $request->all());
            $result = UserDetails::updateOrCreate(['user_id' => $user->id],[
                'user_id' => $user->id,
                'gender' => $gender,
                'dob' => $dob,
                'biography' => $biography,
                'primary_address' => $primary_address,
                'secondary_address' => $secondary_address,
                'country_id' => $country_id,
                'state_id' => $state_id,
                'city_name' => $city_name,
                'post_code' => $postcode
            ]);
            if($avaibility){
                $user->businessHours()->delete();
                $businessHours = [];
                foreach ($avaibility as $av){
                    if(isset($av["status"])){
                        if($av["from_time"] == null || $av["to_time"] == null){
                            toastr()->error('Time slot cannot be empty', 'Incomplete Data');
                            throw ValidationException::withMessages(["Time slot cannot be empty"]);
                        }
                        $bh = BusinessHour::make($av);
                        array_push($businessHours, $bh);
                    }
                }
                $user->businessHours()->saveMany($businessHours);
            }
            DoctorsDetail::updateOrCreate(['user_id' => 2],[
                'education' => $education,
                'experience' => $experience,
                'awards' => $awards,
                'memberships' => $memberships,
                'documents' => $documents
            ]);
        }, 1);

        toastr()->success('Profile successfully updated', 'Success');
        return back();
    }

    public function showPatients(Request $request)
    {
        $patientsIds = Appointment::where('doctor_id', auth()->id())->get()->pluck('patient_id')->unique();
        $patient = [];
        $users = User::with(['userDetails', 'userDetails.country', 'userDetails.state', 'userDetails.city'])->whereIn('id', $patientsIds)->get();
        return view('doctor.patients', compact('users'));
    }

    public function showProfile($slug, Request $request)
    {
        $user = User::where('slug', $slug)->where('role', User::DOCTOR_ROLE)->firstOrFail();
        $userDetails = $user->userDetails()->first();
        $doctorDetails = $user->doctorDetails()->first();
        $specializations = implode(', ', $user->specialization()->get()->pluck('specialization_name')->toArray()??[]);
        return view('doctor.profile-page',
            ['user' => $user,
                'userDetails' => $userDetails,
                'doctorDetails' => $doctorDetails,
                'specializations' => $specializations
            ]);
    }

    public function doctorProfile()
    {

    }

    public function blog()
    {
        $blogs = Blog::all();
        return view('doctor.blog.list', ['blogs' => $blogs]);
    }

    public function loadQA()
    {
        return view('doctor.qa.index');
    }
}
