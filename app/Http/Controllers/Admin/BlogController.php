<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCategories;
use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    //
    public function index()
    {
        $blogs = Blog::all();

        return view('admin/blog-list', compact('blogs'));
    }

    public function add()
    {
        $categories = Categories::all();
        return view('admin/blog-add', compact("categories"));
    }

    public function save(Request $request){
        $blogTitle = $request->input('title');
        $blogContent = $request->input('blog-body');
        $category_id = $request->input('category_id');
        $user_id = auth()->id();
        $is_published = true;

        $blog = Blog::create(['body' => $blogContent, 'title' => $blogTitle, 'user_id' => $user_id, 'is_published' => $is_published]);
        if($blog){
            $blog_id = $blog->id;
            $blogCategoryModel = null;
            foreach($category_id as $id){
                $blogCategoryModel = BlogCategories::create(['id' => $id, 'blog_id' => $blog_id, 'user_id' => $user_id]);
                if(!$blogCategoryModel){
                    return back()->withErrors($blogCategoryModel);
                }
            }
            return back()->with('success', 'Blog added successfully');
        }else{
            return back()->withErrors($blog);
        }
    }


    public function edit(Request $request, $id)
    {
        $blog = Blog::findOrFail($id);
        $categories = Categories::all();
        return view('admin/blog-edit', compact("blog", "categories"));
    }

    public function update(Request $request){
        $id = $request->input('id');
        $category_id = $request->input('category_id');
        Blog::where('id', $id)
            ->update(['body' => $request->input('body'),'title' => $request->input('title') ]);
        $deletedRows = BlogCategories::where('blog_id', $id)->delete();
        foreach($category_id as $cid){
            $blogCategoryModel = BlogCategories::create(['id' => $cid, 'blog_id' => $id, 'user_id' => auth()->id()]);
            if(!$blogCategoryModel){
                return back()->withErrors($blogCategoryModel);
            }
        }

        return back()->withSuccess("Successfully updated blog");

    }

    //----------- Categories ---------------//
    public function categories()
    {
        $categories = Categories::all();
        return view('admin/blog-categories', compact("categories"));
    }

    public function categories_add(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'image' => 'nullable|mimes:jpeg,png,svg,webp|max:1024'
        ]);
        if($request->hasFile('image')){
            $imageName = time().'.'.$request->image->extension();

            $path = Storage::disk('s3')->put('public', $request->image);
            $path = Storage::disk('s3')->url($path);
            $data['image'] = $path;
        }
        $model = Categories::create($data);
        if(!$model){
            return back()->withErrors($model);
        }
        return back()->with('success', 'Category added successfully');
    }

    public function categories_edit(Request $request)
    {
        $data = $request->validate([
            'id' => 'required',
            'name' => 'required',
            'image' => 'optional|mimes:jpeg,png,svg,webp|max:1024'
        ]);
        if($request->hasFile('imageimage')){
            $path = $request->file('imageimage')->store( 'public');
            $data['image'] = $path;
        }
        $model = Categories::findOrFail($request->input('id'))->update($data);
        if(!$model){
            return back()->with('error', 'Cannot Update model');
        }
        return back()->with('success', 'Category added successfully');
    }

    public function categories_delete(Request $request)
    {
        $id = $request->input('id');
        $result = Categories::findOrFail($id)->deleteOrFail();
        if($result){
            return back()->with('success', 'Category deleted successfully.');
        }else{
            return back()->with('error', 'Cannot delete the category.');
        }
    }
}
