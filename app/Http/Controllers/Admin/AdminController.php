<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;

class AdminController extends Controller
{
    //
    public function index()
    {
        $user = new User();
        $doctors = $user->doctors();
        $patients = $user->getPatients();

        $data = [
            'doctors' => $doctors,
            'patients' => $patients,
        ];
        return view('admin.index_admin', $data);
    }
}
