<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Specialization;
use Illuminate\Http\Request;

class SpecializationController extends Controller
{
    //
    public function index()
    {
        $specializations = Specialization::all();
        return view('admin.specialities', compact('specializations'));
    }

    public function add(Request $request)
    {

        $specialization = $request->validate([
            'specialization_name' => 'required',
            'specialization_image' => 'required|mimes:jpeg,png,svg'
        ]);
        $path = $request->file('specialization_image')->store('specialization', 'public');
        $specialization['specialization_image']= $request->file();
        $specialization['status']= 1;
        $model = Specialization::create($specialization);
        if($model){
            return redirect()->back()->with('success', 'Added successfully');
        }

        return redirect()->back()->withErrors('');
    }

    public function edit(Request $request)
    {
        $specialization = $request->validate([
            'id'=>'required',
            'specialization_name' => 'required',
            'specialization_image' => 'nullable|mimes:jpeg,png,svg,webp|max:1024',
            'status' => 'nullable',
        ]);
        if($request->hasFile('specialization_image')){
            $path = $request->file('specialization_image')->store('specialization', 'public');
            $specialization['specialization_image'] = $path;
        }
        $model = Specialization::find($specialization['id'])->update($specialization);
        if($model){
            return redirect()->back()->with('success', 'Added successfully');
        }
    }

    public function delete(Request $request)
    {
        $specialization = $request->validate([
            'id'=>'required',
        ]);
        if(Specialization::find($specialization['id'])->delete()){
            return redirect()->back()->with('success', 'Deleted successfully');
        }
        return edirect()->back()->withError('Could not delete.');
    }
}
