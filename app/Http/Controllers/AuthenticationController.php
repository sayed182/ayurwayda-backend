<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthenticationController extends Controller
{
    public function index(Request $request)
    {
        $view = 'login';
        if($request->getRequestUri() == '/register'){
            return view('auth.register', ['view'=> $view]);
        }
        if($request->getRequestUri() == '/doctor-register'){
            return view('auth.doctor-register');
        }
        return view('auth.login', ['view'=> $view]);
    }

    public function login(Request $request){
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if(Auth::guard('web')->attempt($credentials)){
            $role = Auth::user()->role;
            $request->session()->regenerate();
            if($role == 'doctor')
                return redirect()->intended(route('doctor.dashboard'));
            else
                return redirect()->intended(route('patient.profile.index'));
        }
        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }
    public function logout(Request $request)
    {
        Auth::logout();

        Session::flush();
        return redirect()->route('home');
    }
}
