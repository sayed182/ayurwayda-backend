<?php

namespace App\Http\Controllers;

use App\Helpers\AgoraHelper;
use App\Models\Appointment;
use App\Models\BusinessHour;
use App\Models\DoctorTimeSlots;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Ramsey\Uuid\Rfc4122\UuidV4;
use Willywes\AgoraSDK\AgoraSDK;

class BookAppointments extends Controller
{
    public function index($slug, Request $request)
    {
        $doctor = User::where('slug', $slug)->firstOrFail();
        $userDetails = $doctor->userDetails()->first();
        $avaliableTime = BusinessHour::where('user_id', $doctor->id)->get();
        return view('appointments/book', ['doctor' => $doctor, 'slots' => $avaliableTime, 'userDetails' => $userDetails]);
    }

    public function checkout(Request $request){
        $request->validate([
            "doctor_id" => "required",
            "date_selected" => "required",
            "time_selected" => "required",
            "type" => 'required',
            "hourly_rate" => 'required'
        ]);
        $doctor = User::with(['userDetails', 'doctorDetails'])->findOrFail($request->doctor_id);
//        dd($doctor->userDetails);
        $date = $request->date_selected;
        $time = generate_slot(DoctorTimeSlots::find($request->time_selected));
        $time_id = $request->time_selected;
        return view('appointments.checkout', compact('date', 'time', 'time_id',  'doctor'));
    }

    public function store(Request $request)
    {
        $request->validate([
            "doctor_id" => "required",
            "date_selected" => "required",
            "time_selected" => "required",
            ]);
        $doctor_id = $request->input('doctor_id');
        $user_name = user_fullname(User::findOrFail($doctor_id));
        $patient_id = auth()->user()->id;
        $appointment_date = $request->input('date_selected');
        $status = 'new';
        $doctor_time_slot = $request->input('time_selected');
        $type = 'online';
        $payment_id = UuidV4::uuid4()->toString();
        $time = generate_slot(DoctorTimeSlots::find($request->time_selected));
        $date = get_formatted_date($appointment_date);
        $result = Appointment::create(['payment_id' => $payment_id,
            'patient_id' => $patient_id,
            'doctor_id' => $doctor_id,
            'type' => $type,
            'appointment_date' => $appointment_date,
            'doctor_time_slot' => $doctor_time_slot,
            'approved' => false,
            'status' => $status]);
        if($result){
            return view('appointments.success', compact('user_name', 'time', 'date' ));
        }
    }

    public function textChat($userid)
    {
        $user = User::find($userid);
        $token = AgoraHelper::GetToken(auth()->user()->email, "my-channel");
        return view('doctor.chat', ['token' => $token, 'peer' => $user]);
    }

    public function getSlots(Request $request)
    {
        $request->validate([
            'doctor_id' => 'required',
            'day_name' => 'required'
        ]);
        $avaliableTime = BusinessHour::where('user_id', $request->doctor_id)->get();
        $slots = $avaliableTime->where('day_name','LIKE', $request->day_name)->first();
        $fromTime = Carbon::make($slots->from_time);
        $toTime = Carbon::make($slots->to_time);
        $slotsArray = [];
        while($fromTime <= $toTime){
            array_push($slotsArray, ['from' => get_formatted_time($fromTime), 'to' => get_formatted_time($fromTime->addMinutes(30))]);
            $fromTime = $fromTime->addMinutes(30);
        }
        return response()->json($slotsArray);
    }
}

