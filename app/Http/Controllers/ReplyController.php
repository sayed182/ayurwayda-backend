<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\Reply;
use Illuminate\Http\Request;

class ReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function index(Question $question)
    {
        return $question->replies;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function create(Question $question)
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Question $question)
    {
		/** @var \App\Models\User $user **/
		$user = auth()->user();

		if ($user->role === 'doctor') {
			$doctor_specializations = $user->specialization()->pluck('id');
			$question_specializations = $question->specializations()->pluck('id');
			$common_specializations = $doctor_specializations->intersect($question_specializations);
			
			if ($common_specializations->isEmpty()) {
				abort(403);
			}

			$type = 'answer';
		} else {
			$type = 'question';
		}

        $validated = $request->validate([
			'body' => ['required', 'string']
		]);

		$reply = Reply::create([
			'user_id' => $user->id,
			'question_id' => $question->id,
			'type' => $type,
			'body' => $validated['body']
		]);

		return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Question  $question
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question, Reply $reply)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Question  $question
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question, Reply $reply)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Question  $question
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question, Reply $reply)
    {
		if (auth()->id() !== $reply->user_id) {
			abort(403);
		}
		
        $validated = $request->validate([
			'body' => ['required', 'string']
		]);

		$reply->update($validated);

		return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Question  $question
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question, Reply $reply)
    {
		if (auth()->id() !== $reply->user_id) {
			abort(403);
		}
		
		$reply->delete();
		return back();
    }
}
