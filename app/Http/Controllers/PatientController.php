<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;

class PatientController extends Controller
{

    public function index()
    {
        $appointments = Appointment::with(['doctor', 'specialization', 'time_slot'])->where('patient_id',\Auth::id())->get();

//        dd($appointments);
        return view('patient.dashboard', compact('appointments'));
    }

    public function settings()
    {
        return view('patient.profile-settings.index');
    }

    public function appointments(){
        $appointments = Appointment::with(['doctor', 'specialization', 'time_slot'])->where('patient_id',\Auth::id())->get();
        $unique = [];
        $unique_doctors = [];
        foreach($appointments as $appointment){
            if(!in_array($appointment->doctor->id, $unique)){
                array_push( $unique, $appointment->doctor->id);
                array_push($unique_doctors, $appointment->doctor);
            }
        }
        return view('patient.appointments', compact('unique_doctors'));
    }

    public function calender(){
        return view('patient.calender');
    }

    public function invoice(){
        $invoices = Payment::with('appointment')->whereHas('appointment', function ($query) {
            $query->where('patient_id', \Auth::id());
        })->get();
        return view('patient.invoice', compact('invoices'));
    }

    public function accounts(){
        return view('patient.accounts');
    }

    public function showPassword(){
        return view('patient.change-password');
    }

    public function loadQA(){
        return view('patient.qa.index');
    }
}
