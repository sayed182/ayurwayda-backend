<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\Specialization;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class QAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$questions = Question::query();
		if ($request->input('specialization')) {
			$specialization = $request->input('specialization');
			
			$questions = $questions->whereRelation('specializations', function($query) use ($specialization) {
				$query->where('id', $specialization);
			});
		}

		if ($request->input('search')) {
			$questions = $questions->where('body', 'LIKE', '%' . $request->input('search') . '%');
		}

		$questions = $questions->simplePaginate(10)->withQueryString();
		$specializations = Specialization::where('status', true)->get();
        $doctors = User::where('role', 'doctor')->get();
		$latestQuestions = Question::latest()->take(10)->get();
		
        return view('home.qa.index', [
			'questions' => $questions,
			'doctors' => $doctors,
			'specializations' => $specializations,
			'latestQuestions' => $latestQuestions
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$specializations = Specialization::where('status', true)->get();
        return view('home.qa.create', compact('specializations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$validated = $request->validate([
			'query' => ['required', 'string'],
			'specializations' => ['required', 'array', Rule::exists('specializations', 'id')->where('status', true)],
			'images' => ['sometimes', 'array'],
			'images.*' => ['image']
		], [
			'image' => 'Uploaded file must be a valid image.'
		]);

		$question = Question::create([
			'body' => $validated['query'],
			'user_id' => User::where('role', 'patient')->first()->id
		]);

		$question->specializations()->attach($validated['specializations']);

		foreach ($validated['images'] as $image) {
			$imagePath = $image->store('questions', 'public');

			$question->images()->create([
				'image' => $imagePath
			]);
		}

		return redirect()->route('qa')->with('success', 'Question Added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
		$specializations = Specialization::where('status', true)->get();
		$firstAnswer = $question->answers()->first();
        return view('home.qa.details', compact('question', 'specializations', 'firstAnswer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
