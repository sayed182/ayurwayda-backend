<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Blog;
use App\Models\City;
use App\Models\Question;
use App\Models\Reviews;
use App\Models\Specialization;
use App\Models\State;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Twilio\Rest\Client;

class HomeController extends Controller
{
    public function index()
    {
        $specializations = Specialization::all();
        $doctors = User::with(['userDetails', 'doctorDetails'])->where('role', 'doctor')->get();
        $blogs = Blog::with(['user', 'categories'])->orderByDesc('created_at')->limit(6)->get();
        $questions = Question::with(['asker', 'answers'])->orderByDesc('created_at')->limit(6)->get();
        return view('home.index', [
            'specializations' => $specializations,
            'doctors' => $doctors,
            'blogs' => $blogs,
            'questions' => $questions
        ]);
    }

    public function about()
    {
        return view('home.about');
    }

    public function blog()
    {
        $blogs = Blog::all();
        return view('home.blog', ['blogs' => $blogs]);
    }

    public function qa()
    {
        $questions = Question::all();
		$specializations = Specialization::where('status', true)->take(5)->get();
        $doctors = User::where('role', 'doctor')->get();

        return view('home.qa.index', [
			'questions' => $questions,
			'doctors' => $doctors,
			'specializations' => $specializations
		]);
    }

    public function getAllHomeResources(Request $request){
        return response()->json([
            'status' => 'success',
            'data' => [
                'specialization' => Specialization::all(),
                'doctors' => '',
                'blogs' => Blog::all(),
                'reviews' => Reviews::all(),
                'faq' => ''
            ],
            'message' => 'All homepage related data',
        ]);
    }

    public function contact()
    {
        return view('home.contact');
    }

}
