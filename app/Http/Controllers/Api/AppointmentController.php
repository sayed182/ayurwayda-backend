<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DoctorsAppointmentsListResource;
use App\Models\Appointment;
use App\Models\DoctorTimeSlots;
use App\Models\User;
use DateTime;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createSlots(Request $request)
    {
        $user_id = $request->user()->id;
        $isValidated = $request->validate([
            'day_name' => 'required|in:monday,tuesday,wednesday,thursday,friday',
            'start_time' => 'required|date_format:H:i',
            'duration_in_min' => 'required',
            'type' => 'required|in:online,clinic'
        ]);
        if(!$isValidated){
            return response()->json(['message'=>"Cannot validate the input"]);
        }
        $data = $request->input();
        $date = strtotime($data['start_time']);
        $dt = new DateTime();
        $dt->setTimestamp($date);
        $data['start_time'] = $dt->format('H:i:s');
        $data['user_id'] = $user_id;
        $result = DoctorTimeSlots::create($data);
        if(!$result){
            return response()->json(['message'=>"failed to insert data"]);
        }
        return \response()->json(['message' => 'Successfully inserted record']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $isValidated = $request->validate([
            'doctor_id' => 'required|int',
            'type' => 'required|in:online,clinic',
            'appointment_date' => 'required',
            'doctor_time_slot' => 'required|int']);
        $data = $request->input();
        $data['patient_id'] = $request->user()->id;
        $data['status'] = 'new';
        $data['approved'] = false;
        try{
            Appointment::create($data);
            return response()->json(['message' => 'Successfully created the appointment']);
        }catch (\Exception $e){
            return response()->json(['message'=>'Cannot create the appointment', 'error' => $e->getMessage()]);
        }

    }




    public function getAppointments(Request $request)
    {
        $doctor_id = $request->user()->id;
        return new DoctorsAppointmentsListResource($request);
        return response()->json(['data'=> $appointments]);
    }

    public function patientList(Request $request )
    {
        $used_id = $request->user()->id;
        $appointments = User::with('doctor_details')->where('id', $used_id)->first();
        $appointments = $appointments['doctor_details'];
        return response()->json(['data'=> $appointments]);
    }

    public function patientCancelAppointment(Request $request): \Illuminate\Http\JsonResponse
    {
        $appointment = new Appointment();
        $appointment_id = $request->input('appointment_id');
        $user_id = $request->user()->id;
        $appointment->where('id', '=', $appointment_id)->update(['status' => 'canceled']);
        DB::table('canceled_appointments')->insert(['appointment_id' => $appointment_id, 'user_id' => $user_id, 'created_at'=> date('Y:m:d H:i:s'), 'updated_at' => date('Y:m:d H:i:s')]);
        return response()->json(['messagae' => 'Succesfully cancelled appointment']);
    }
}
