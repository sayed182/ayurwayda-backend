<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BusinessHour;
use Illuminate\Http\Request;

class BusinessHourController extends Controller
{
    //
    public function save(Request $request)
    {
        $usr_id = $request->user()->id;
        BusinessHour::unguard();
        $data = $request->validate([
            'day' => 'required',
            'from_time' => 'required',
            'to_time' => 'required',
        ]);
        $day = $request->input('day');
        $from_time = $request->input('from_time');
        $to_time = $request->input('to_time');
        if( (count($day) != count($from_time)) ||  count($day) != count($to_time)){
            return response()->json(['message' => 'Proper time is missing'], 400);
        }
        $data = array_merge_recursive(array_combine($day, $from_time), array_combine($day, $to_time));
        foreach ($data as $day => $timeArr){
            $res = BusinessHour::create(['user_id'=> $usr_id, 'day_name' => $day, 'from_time' => date( 'H:i:s',strtotime($timeArr[0])) , 'to_time' =>  date( 'H:i:s',strtotime($timeArr[1]))]);
            BusinessHour::reguard();
            if($res){

                return response()->json(['message' => "success updating business hour for userNo: ". $usr_id]);
            }
        }
        BusinessHour::reguard();
        return response()->json($data);
    }
}
