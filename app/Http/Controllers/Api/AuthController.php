<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;


class AuthController extends Controller
{
    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if (Auth::attempt($request->only('email', 'password'))) {
            $request->session()->regenerate();
            $user = Auth::user();
            $token = $user->createToken('ayurwayda_token')->plainTextToken;
            return response()->json([
                    'role' => $user->role,
                'token' => $token,
            ], Response::HTTP_OK);
        }

        return response()->json(['error' => 'Invalid credentials']);
    }

    public function logOut(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return response()->json([], 204);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'full_name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'mobile' => 'required|numeric',
            'password' => 'required',
            'role' => 'required|IN:doctor,patient'
        ]);
        if($validator->fails()){
            return response()->json(["error"=>"true", "data"=>$validator->messages()]);
        }
        $data = $request->input();
        $name_array = explode(" ",$data['full_name']);
        $data['first_name']=$name_array[0];
        $data['last_name'] = count($name_array)>1?end($name_array):"";
        $data['password'] = Hash::make($data['password']);
        $data['username'] = explode('@', $data['email'])[0];
        unset($data['full_name']);
        try{
            $user = User::create([
                'first_name'=>$data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password'=>$data['password'],
                'mobileno'=>$data['mobile'],
                'username' => $data['username'],
                'status'=>'inactive'
            ]);
            $token = $user->createToken('ayurwayda')->plainTextToken;
            $response = [
                'user' => $user,
                'token' => $token,
            ];
            return response()->json(["success"=>true, "data"=>$response], 200);
        }catch (\Illuminate\Database\QueryException $e){
            return response()->json(["error"=>"true", "data"=>"User Already Present"]);
        }

    }

}
