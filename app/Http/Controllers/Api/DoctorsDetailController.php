<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Http\Request;

class DoctorsDetailController extends Controller
{
    public function index(Request $request)
    {
        $user_id=$request->user()->id;
        return "hello";
    }

    public function updateAvatar(Request $request)
    {
        $data = $request->validate([
            'avatar' => 'required|mimes:jpeg,jpg,png,svg|max:2048'
        ]);
        $user_id=$request->user()->id;
        $user = User::findOrFail($user_id);
        if($request->hasFile('avatar')){
            $ext = $request->file('avatar')->extension();
            $filename = $user->username.'.'.$ext;
            $path = $request->file('avatar')->storeAs('uploads/users', $filename, 'public');
            $user->profile_image = $path;
            if($user->save()){
                return response()->json(['message'=>"Successfully updated profile pic"], 200);
            }
        }
        return response()->json(['message'=>'Cannot process request']);
    }

    public function save(Request $request)
    {
        $data = $request->validate([
            'gender' => 'required|in:male,female,other',
            'dob' => 'required|date',
            'country_id' => 'required|numeric',
            'state_id' => 'required|numeric',
            'city_id' => 'nullable|numeric',
            'post_code' => 'required',
            'currency_code' => 'nullable',
            'price' => 'required|numeric',
            'specialization_id' => 'required|numeric',
        ]);
        $data['user_id'] = $request->user()->id;
        if($request->has('id')){
            $user_details = UserDetails::findOrFail($request->id);
            $status = $user_details->update($request->input());
            if($status){
                return response()->json(['message'=>"successfully updated user detail"]);
            }
        }
        $user_details = UserDetails::create($request->input());
        if($user_details){
            return response()->json(['message'=>"successfully updated user detail"]);
        }
        return response()->json(['message'=>"cannot update user details"], 401);
    }

}
