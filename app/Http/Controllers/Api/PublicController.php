<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Country;
use App\Models\State;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function listAllDoctors(Request $request)
    {
        $user_model = new User();
        $doctor = $user_model->doctors();
        if($request->has('status') && in_array($request->get('status'), ['active', 'inactive'])){
            $doctor->where('status', $request->get('status'));
        }
        if($request->has('specialization')){
            $specialization_array = explode(',',$request->get('specialization'));
            $doctor->whereHas('specialization', function ($query) use ($specialization_array) {
                return $query->whereIn('id', $specialization_array);
            });
        }
        $doctor = $doctor->get();
        return response()->json([
            'data' => $doctor
        ]);
    }

    protected function create(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'phone_number' => ['required', 'numeric'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        /* Get credentials from .env */
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_ACCOUNT_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        $verification_check= $twilio->verify->v2->services($twilio_verify_sid)
            ->verifications
            ->create($data['phone_number'], "sms");
        print_r($verification_check->status);
        return redirect()->route('verify')->with(['phone_number' => $data['phone_number']]);
    }

    protected function verify(Request $request)
    {
        $data = $request->validate([
            'verification_code' => ['required', 'numeric'],
            'phone_number' => ['required', 'string'],
        ]);
        /* Get credentials from .env */
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_ACCOUNT_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        $verification = $twilio->verify->v2->services($twilio_verify_sid)
            ->verificationChecks
            ->create($data['verification_code'], array('to' => $data['phone_number']));
        if ($verification->valid) {
            $user = tap(User::where('mobileno', $data['phone_number']))->update(['account_verified_at' => date('now')]);
            /* Authenticate user */
            Auth::login($user->first());
            return redirect()->route('home')->with(['message' => 'Phone number verified']);
        }
        return back()->with(['phone_number' => $data['phone_number'], 'error' => 'Invalid verification code entered!']);
    }

    public function get_countries()
    {
        $countries = Country::all();
        return response()->json($countries);
    }
    public function get_states(Request $request)
    {
        if($request->input('country')){
            $states = State::where('country_id', '=', $request->all())->get();
            return response()->json($states);
        }
        $states = State::all();
        return response()->json($states);
    }
    public function get_cities(Request $request)
    {
        if($request->input('state')){
            $cities = City::where('state_id', '=', $request->all())->get();
            return response()->json($cities);
        }
        $cities = City::all();
        return response()->json($cities);
    }
}
