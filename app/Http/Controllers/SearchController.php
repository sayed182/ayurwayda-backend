<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Specialization;
use App\Models\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index()
    {
        $specializations = Specialization::all();
        $cities = City::all();
        $doctors = User::with(['userDetails', 'doctorDetails'])->where('role', User::DOCTOR_ROLE)->get();
        return view('home.doctors-search', compact('doctors', 'cities', 'specializations'));
    }

    public function placeSearch($location){
        $city = City::where('slug', $location)->firstOrFail();
        $cityId = $city->id;

        $specializations = Specialization::all();
        $cities = City::all();
        $doctors = User::with(['doctorDetails'])
            ->whereHas('userDetails', function ($query) use ($cityId){
                return $query->where('city_id', $cityId);
            })
            ->where('role', User::DOCTOR_ROLE)->get();
        return view('home.doctors-search', compact('doctors', 'cities', 'specializations'));

    }

    public function placeDoctorSearch($location, $specialization){
        $city = City::where('slug', $location)->firstOrFail();
        $cityId = $city->id;

        $specializations = Specialization::all();
        $cities = City::all();
        $doctors = User::with(['doctorDetails', 'specialization'])
            ->whereHas('userDetails', function ($query) use ($cityId){
                return $query->where('city_id', $cityId);
            })
            ->whereHas('specialization', function($query) use ($specialization) {
                return $query->where('slug', $specialization);
            })
            ->where('role', User::DOCTOR_ROLE)->get();
        return view('home.doctors-search', compact('doctors', 'cities', 'specializations'));
    }

    public function search($param){
        $params_arr = explode('/', $param);
        foreach ($params_arr as $query){

        }
        return $param1;
    }
}
