<?php

namespace App\Http\Resources;

use App\Models\Appointment;
use Illuminate\Http\Resources\Json\JsonResource;

class DoctorsAppointmentsListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $user_id = $this->user()->id;
        $appointments = Appointment::where('doctor_id', $user_id)
            ->with(['patient_details', 'time_slot'])
            ->with('payments', function ($query) {
                $query->where('transaction_status','=','success');
            })
            ->get();
        $data = [];
        foreach ($appointments as $appointment){

            $temp['name'] = $appointment->patient_details->first_name . $appointment->patient_details->last_name;
            $temp['email'] = $appointment->patient_details->email;
            $temp['image'] = $appointment->patient_details->profile_image;
            $temp['mobile'] = $appointment->patient_details->mobileno;
            $temp['mobile'] = $appointment->patient_details->mobileno;
            $temp['day']=$appointment->time_slot->day_name;
            $temp['duration']=$appointment->time_slot->duration_in_min;
            $temp['start_time']=date('h:i a', strtotime($appointment->time_slot->start_time));
            $temp['type']=$appointment->type;
            $temp['status']=$appointment->status;
            $temp['confirmed']=$appointment->approved;
            $temp['date'] = date( 'd M, Y', strtotime($appointment->appointment_date));
           if(count($appointment->payments) > 0){
               $i = count($appointment->payments) - 1;
               $temp['amount'] = $appointment->payments[$i]->total_amount;
               $temp['invoice'] = $appointment->payments[$i]->invoice_no;
           }else{
               $temp['amount'] = null;
               $temp['invoice'] = null;
           }

           array_push($data, $temp);
        }
        return $data;
    }
}
