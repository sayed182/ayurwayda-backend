<?php

namespace App\View\Components;

use Illuminate\View\Component;

class InvoiceModal extends Component
{
    public $appointment;
    public function __construct($appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.invoice-modal');
    }
}
