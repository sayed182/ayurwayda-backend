<?php

if(!function_exists('user_fullname')){
    function user_fullname($user){
        return $user->first_name." ".$user->last_name;
    }
}

if(!function_exists('get_profile_image')){
    function get_profile_image($user){
        if(isset($user->profile_image) && $user->profile_image != ""){
            if(Storage::exists($user->profile_image)){
                return Storage::get($user->profile_image);
            }
        }
        return "https://randomuser.me/api/portraits/men/$user->id.jpg";
    }
}

if(!function_exists('user_city')){
    function user_city($user){
        if($user == null){
            return 'N/A';
        }
        return $user->city->city_name??"";
    }
}

if(!function_exists('user_state')){
    function user_state($user){
        if($user == null){
            return 'N/A';
        }
        return $user->state->state_name??'';
    }
}

if(!function_exists('user_address')){
    function user_address($user){
        if($user == null){
            return 'N/A';
        }
        $address = trim(user_city($user).', '.user_state($user), ',');
        return strlen($address) > 1? $address : 'No address given';
    }
}

if(!function_exists('generate_slot')){
    function generate_slot($slot){
        $time = \Carbon\Carbon::make($slot->start_time);
        return $time->format('h:i a').' - '.$time->addMinutes($slot->duration_in_min)->format('h:i a').' ('.$slot->type.')';
    }
}

if(!function_exists('get_formatted_date')){
    function get_formatted_date($date){
        $date = \Carbon\Carbon::make($date)->format('d M Y');
        return $date;
    }
}

if(!function_exists('get_formatted_time')){
    function get_formatted_time($time){
        $data = \Carbon\Carbon::make($time)->format('h:i a');
        return $data;
    }
}

if(!function_exists('get_money_sign')){
    function get_money_sign(){
        echo "&#8377;";
    }
}

if(!function_exists('get_formatted_amount')){
    function get_formatted_amount($amount){
        echo "&#8377; ".($amount/100);
    }
}

if(!function_exists('is_active_menu')){
    function is_active_menu($route){
        return Request::routeIs($route)?'active':'';
    }
}
