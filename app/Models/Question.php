<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
	use HasFactory;

	protected $fillable = ['user_id', 'body'];

	protected $with = ['images'];

	// public function scopeFilter(Builder $query, array $filters)
	// {
	// 	$query->when($filters['specializations'], function ($specialization) {
	// 		$query->
	// 	})
	// } 

	public function images()
	{
		return $this->hasMany(QuestionImage::class);
	}

	public function specializations()
	{
		return $this->belongsToMany(Specialization::class);
	}

	public function asker()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function replies()
	{
		return $this->hasMany(Reply::class);
	}

	public function answers()
	{
		return $this->hasMany(Reply::class)->where('type', 'answer');
	}
}
