<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecializationUsers extends Model
{
    use HasFactory;
    protected $table = "specialization_user";
}
