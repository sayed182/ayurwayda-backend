<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BusinessHour
 *
 * @property int $id
 * @property int $user_id
 * @property string $day_name
 * @property string $from_time
 * @property string $to_time
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\BusinessHourFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|BusinessHour newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BusinessHour newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BusinessHour query()
 * @method static \Illuminate\Database\Eloquent\Builder|BusinessHour whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BusinessHour whereDayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BusinessHour whereFromTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BusinessHour whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BusinessHour whereToTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BusinessHour whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BusinessHour whereUserId($value)
 * @mixin \Eloquent
 */
class BusinessHour extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'day_name', 'from_time', 'to_time'];
}
