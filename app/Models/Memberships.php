<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Memberships
 *
 * @method static \Database\Factories\MembershipsFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Memberships newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Memberships newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Memberships query()
 * @mixin \Eloquent
 */
class Memberships extends Model
{
    use HasFactory;
}
