<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Awards
 *
 * @method static \Database\Factories\AwardsFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Awards newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Awards newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Awards query()
 * @mixin \Eloquent
 */
class Awards extends Model
{
    use HasFactory;
}
