<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Payment
 *
 * @property int $id
 * @property int $appointment_id
 * @property string $invoice_no
 * @property int $per_hour_charge
 * @property int $total_amount
 * @property string $currency_code
 * @property string|null $price_is_usd
 * @property string $transaction_status
 * @property string $payment_merchant
 * @property float $tax_percentage
 * @property float $transaction_charge
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\PaymentFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereAppointmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereCurrencyCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereInvoiceNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment wherePaymentMerchant($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment wherePerHourCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment wherePriceIsUsd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereTaxPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereTotalAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereTransactionCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereTransactionStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Payment extends Model
{
    use HasFactory;

    public function appointment()
    {
        return $this->belongsTo(Appointment::class, 'appointment_id');
    }

}
