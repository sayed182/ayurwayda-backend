<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\otp
 *
 * @property int $id
 * @property string $mobile_number
 * @property string $otp
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\otpFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|otp newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|otp newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|otp query()
 * @method static \Illuminate\Database\Eloquent\Builder|otp whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|otp whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|otp whereMobileNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|otp whereOtp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|otp whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class otp extends Model
{
    use HasFactory;
}
