<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    protected $fillable = ['body','title', 'is_published', 'user_id'];

    public function categories()
    {
//        return $this->join('blog_category', 'blog_category.blog_id', "=", 'blogs.id')
//            ->join('categories', 'categories.id', '=', 'blog_category.id')
//            ->select(['categories.name','categories.id'])
//            ->get();
        return $this->belongsToMany(Categories::class,BlogCategories::class, 'blog_id', 'category_id', 'id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
