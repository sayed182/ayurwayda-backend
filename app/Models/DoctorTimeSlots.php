<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorTimeSlots extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','day_name', 'start_time', 'duration_in_min', 'type'];
}
