<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PaymentRequest
 *
 * @property int $id
 * @property int $patient_id
 * @property int $doctor_id
 * @property string $payment_type
 * @property int $amount
 * @property string $currency_code
 * @property string|null $description
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\PaymentRequestFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentRequest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentRequest query()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentRequest whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentRequest whereCurrencyCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentRequest whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentRequest whereDoctorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentRequest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentRequest wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentRequest wherePaymentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentRequest whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentRequest whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PaymentRequest extends Model
{
    use HasFactory;
}
