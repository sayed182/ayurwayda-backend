<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserDetails
 *
 * @property int $id
 * @property int $user_id
 * @property string $gender
 * @property string $dob
 * @property string $blood_group
 * @property string|null $biography
 * @property string|null $clinic_name
 * @property string|null $clinic_address
 * @property string|null $primary_address
 * @property string|null $secondary_address
 * @property int $country_id
 * @property int $state_id
 * @property int|null $city_id
 * @property string $post_code
 * @property string $currency_code
 * @property int $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $specialization_id
 * @method static \Database\Factories\UserDetailsFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereBiography($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereBloodGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereClinicAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereClinicName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereCurrencyCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereDob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails wherePostCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails wherePrimaryAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereSecondaryAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereSpecializationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserDetails whereUserId($value)
 * @mixin \Eloquent
 */
class UserDetails extends Model
{
    use HasFactory;

    protected $fillable =[
        'user_id', 'gender', 'dob', 'blood_group', 'biography', 'clinic_name', 'clinic_address', 'primary_address', 'secondary_address',
        'country_id', 'state_id', 'city_name', 'post_code', 'currency_code', 'price', 'specialization_id'
    ];
    protected $casts = [
        'dob'  => 'date:d-m-Y',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    public function state()
    {
        return $this->belongsTo(State::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
