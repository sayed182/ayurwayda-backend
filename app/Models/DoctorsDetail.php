<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DoctorsDetail
 *
 * @property int $id
 * @property int $user_id
 * @property mixed|null $education
 * @property mixed|null $experience
 * @property mixed|null $awards
 * @property mixed|null $memberships
 * @property mixed|null $documents
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\DoctorsDetailFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|DoctorsDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DoctorsDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DoctorsDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|DoctorsDetail whereAwards($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DoctorsDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DoctorsDetail whereDocuments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DoctorsDetail whereEducation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DoctorsDetail whereExperience($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DoctorsDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DoctorsDetail whereMemberships($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DoctorsDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DoctorsDetail whereUserId($value)
 * @mixin \Eloquent
 */
class DoctorsDetail extends Model
{
    use HasFactory;

    protected $guarded = [];
    public function specializations()
    {
        return $this->belongsToMany(Specialization::class, 'specialization_user', 'user_id', 'specialization_id', 'user_id');
    }
}
