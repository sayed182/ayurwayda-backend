<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Specialization
 *
 * @property int $id
 * @property string $specialization_name
 * @property string|null $specialization_image
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\SpecializationFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Specialization newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Specialization newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Specialization query()
 * @method static \Illuminate\Database\Eloquent\Builder|Specialization whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Specialization whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Specialization whereSpecializationImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Specialization whereSpecializationName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Specialization whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Specialization whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Specialization extends Model
{
    use HasFactory;
    protected $fillable = ['specialization_name', 'specialization_image', 'status'];

    public function doctors()
    {
        return $this->belongsToMany(User::class);
    }

	public function questions()
	{
		return $this->belongsToMany(Question::class);
	}
}
